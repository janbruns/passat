


procedure Tpp_solver.res_count_cls(cls : Pclause; var sat,taut : Tliteral);
var i,lit : Tliteral; alit : Tuliteral; lits : Pliteral;
begin
  sat := 0;
  taut := 0;
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin 
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    if trkvar_visited[alit] then inc(sat);
    if trkvar_visited[alit xor 1] then inc(taut);
  end;
end;



function Tpp_solver.check_cls_resolution(cls1 : Pclause; lst : Tliteral) : boolean;
var alit : Tuliteral; i,lit,sat,taut : Tliteral; cnt : int64; 
lits : Pliteral; cls2 : Pclause; ppc : PPclause; x : boolean;
begin
  x := false;
  lits := getLiterals(cls1);
  if (cls1=nil) then raise Texc_trk.create('pp-vele: chk nil cls?');

  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := true;
    if trkvar_visited[alit xor 1] 
    then raise Texc_trk.create('pp-vele: resolve a taultology');
  end;

  for cnt := lockOccurList(lst,ppc)-1 downto 0 do begin
    cls2 := ppc[cnt];
    if cls2^.len=1 then begin // don't eleminate forced vars
      raise Texc_trk.create('pp-vele: still chk forced var in var-elem chk');
    end;
    res_count_cls(cls2,sat,taut);
    if taut=1 then begin
      res_cl += 1;
      res_lit += cls1^.len +cls2^.len -sat -2;
      if (res_cl >ref_num_cls)
      or (res_lit>ref_num_lits) then begin
        x := true;
        break;
      end;
    end else if taut=0 then raise Texc_trk.create('pp-vele: appearlist outdated?');
  end;

  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;
  unlockOccurList(lst);
  check_cls_resolution := x or ((res_cl >ref_num_cls) or (res_lit>ref_num_lits));
end;

procedure Tpp_solver.do_cls_resolution(cls1 : Pclause; lst : Tliteral);
var alit : Tuliteral; i,lit,sat,taut : Tliteral; lits : Pliteral; cnt : int64; ppc : PPclause; cls2 : Pclause;
begin
  { mark lits of cls1, marks used for both, res_count_cls() and make_resolution_cls() }
  lits := getLiterals(cls1);
  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := true;
  end;

  for cnt := lockOccurList(lst,ppc)-1 downto 0 do begin
    cls2 := ppc[cnt];
    res_count_cls(cls2,sat,taut);
    if taut=1 then begin
      make_resolution_cls(cls1,cls2,lst,sat);
    end else if taut=0 then raise Texc_trk.create('pp-vele: cls_res appearlist outdated?');
  end;
  unlockOccurList(lst);

  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;
end;


procedure Tpp_solver.make_resolution_cls(cls1,cls2 : Pclause; lst : Tliteral; sat : Tliteral);
var alit : Tuliteral; i,j,lit : Tliteral; lits : Pliteral; cls3 : Pclause; ppc : array[0..1] of Pclause;
begin
  lst := abs(lst);
  j := 0;
  lits := getLiterals(cls1);
  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    if (alit<>lst) then begin
      trkvar_learncls[j] := lit;
      inc(j);
    end;
  end;
  if j<>cls1^.len-1 then begin
    raise Texc_trk.create('pp-vele: make_resolution_cls cls1len');
  end;
  lits := getLiterals(cls2);
  for i := cls2^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    if (alit<>lst) then begin
      alit := 2*alit +ord(lit<0);
      if not(trkvar_visited[alit] or trkvar_visited[alit xor 1]) 
      then begin
        trkvar_learncls[j] := lit;
        inc(j);
      end;
    end;
  end;
  if (j<>(cls1^.len+cls2^.len -sat -2)) then begin
    raise Texc_trk.create('pp-vele make_resolution_cls cls3len');;
  end;
  if (j=0) then begin
    raise Texc_trk.create('pp-vele: empty clause generated in var elemenination!');
  end;
  cls3 := createClause(j,2);
  cls3^.prv := latest_niver_clause;
  latest_niver_clause := cls3;
  cls3^.flags := 0;//clausetag_prob;
  move(trkvar_learncls^, getLiterals(cls3)^, j*sizeof(Tliteral));
  ppc[0] := cls1;
  ppc[1] := cls2;
  cl_spool.set_cls_extra(cls3,2,@ppc[0]);
end;


function Tpp_solver.velem_precheck(var lit : Tliteral) : boolean;
var v0a,v1a : int64;
begin
  v0a := lazylenOccurList(lit);
  lit := -lit;
  v1a := lazylenOccurList(lit);
  if (v0a-v1a)<0 
  then lit := -lit;
  velem_precheck := (v1a=0)and(v0a=0);
end;





procedure Tpp_solver.velem_remove_original_clauses_from_litappear(v : Tliteral);
var i : int64; cls1 : Pclause; ppc : PPclause;
begin
  { remove the original clauses on lit }
  i := lockOccurList(v,ppc);
  for i := i-1 downto 0 do begin
    cls1 := ppc[i];
    ppclsactions.push(v,cls1);
    remove_clause_from_appearlist(cls1);
  end;
  unlockOccurList(v);
  if lockOccurList(v,ppc)<>0 then raise Texc_trk.create('pp-vele: not all cls removed');
  unlockOccurList(v);
end;






procedure Tpp_solver.velem_hanlde_new_clauses();
var i,j : Tliteral; alit : Tuliteral; cls1 : Pclause; lits : Pliteral;
begin
  { add the fresh clauses, 
    and maybe make a list of their lits, to make self-subsumption tests about 
  }
  j := 0;
  while latest_niver_clause<>nil do begin
    cls1 := latest_niver_clause;
    latest_niver_clause := cls1^.prv;
    cls1^.prv := latestclause;
    latestclause := cls1;
    make_cls_signature(cls1);
    add_cls_to_processinglist(cls1);
    insert_clause_into_appearlist(cls1);
    ppactions.push(cls1^.len,cls1);
    lits := getLiterals(cls1);
    if ver_complete_resub<=0 then begin
      for i := cls1^.len-1 downto 0 do begin
        alit := abs(lits[i]);
        alit := 2*alit +ord(lits[i]<0);
        if not trkvar_visited[alit] then begin
          trkvar_visited[alit] := true;
          trkvar_conflcls[j] := lits[i];
          inc(j);
        end;
      end;
    end;
  end;
  if ver_complete_resub<=0 then begin
    { cleanup only required if list was made }
    for i := j-1 downto 0 do begin
      j := trkvar_conflcls[i];
      alit := abs(j);
      alit := 2*alit +ord(j<0);
      trkvar_visited[alit] := false;
    end;
    for i := j-1 downto 0 do begin
      add_literal_appearlist_to_processinglist(trkvar_conflcls[i]);
    end;
  end;
end;



function Tpp_solver.velem(v,limcl,limlit : Tliteral) : boolean;
var alit : Tuliteral; ncnt,cnt : int64; j : Tliteral; cls1 : Pclause; ppc : PPclause;
begin
  velem := false; 


  if velem_precheck(v) then exit;

  alit := abs(v);
  alit := 2*alit +ord(v<0);

  ref_num_cls := limcl;
  ref_num_cls += lazylenOccurList(v);
  ref_num_cls += lazylenOccurList(-v);

  
  { lit-limits are given relative to current literal consumption on lit, 
    so make stats about that first
  }
  ref_num_lits := limlit;
  j := 0;
  for cnt := lockOccurList(v,ppc)-1 downto 0 do begin
    j := ppc[cnt]^.len;
    ref_num_lits += j;
    if j=1 then break; // don't eleminate forced vars
  end;
  unlockOccurList(v);
  if j=1 then exit; // don't eleminate forced vars

  for cnt := lockOccurList(-v,ppc)-1 downto 0 do begin
    j := ppc[cnt]^.len;
    ref_num_lits += j;
    if j=1 then break; // don't eleminate forced vars
  end;
  unlockOccurList(-v);
  if j=1 then exit; // don't eleminate forced vars

  { check if resulting clauses would match limits }
  res_cl  := 0;
  res_lit := 0;
  ncnt := lockOccurList(v,ppc);
  for cnt := ncnt-1 downto 0 do begin
    cls1 := ppc[cnt];
    if check_cls_resolution(cls1,-v)
    then begin
      unlockOccurList(v);
      exit;
    end;    
  end;

  { make the resulting clauses }
  for cnt := ncnt-1 downto 0 do begin
    cls1 := ppc[cnt];
    do_cls_resolution(cls1,-v);    
  end;
  unlockOccurList(v);

  varchooseable[abs(v)] := varchooseable[abs(v)] or 2;

  ncnt := ppclsactions.len;
  velem_remove_original_clauses_from_litappear(v);
  velem_remove_original_clauses_from_litappear(-v);
  if ppclsactions.len<=ncnt then raise Texc_trk.create('pp-vele: no cls removed');

  velem_hanlde_new_clauses();

  velem := true;
end;







function Tpp_solver.do_var_elem(limcl,limlit : Tliteral) : boolean;
var i,n : Tliteral; lcl,llt : Tliteral;
begin
  lcl := limcl;
  llt := limlit;
  n := 0;
  repeat
    i := 1; 
    while (i<num_vars) do begin
      if velem(i,lcl,llt) then begin
        //check_marked_clauses_for_selfsubsumes();
        inc(n);
      end;
      inc(i);
    end;
    if (lcl<limcl) then inc(lcl)
    else if (llt<limlit) then inc(llt)
    else break; 
  until (n>0);
  if (n>0) then begin
    if notquiet then writeln('c eleminated ',n,' vars');
    if ver_complete_resub>0 then begin
      add_all_cls_to_processinglist();
      dec(ver_complete_resub);
    end;
  end;
  do_var_elem := (n>0);
end;









procedure Tpp_solver.maybe_single_sided_var(v : Tliteral);
var a,b : int64; ppc : PPclause; 
begin
  a := lockOccurList(v,ppc);
  unlockOccurList(v);
  b := lockOccurList(-v,ppc);
  unlockOccurList(-v);
  if (a>0)and(b>0) then exit;
  if (a=0)and(b=0) then exit;
  if (a=0) then begin
    v := -v;
  end;


  varchooseable[abs(v)] := varchooseable[abs(v)] or 1;

  { check if var is already forced }
  a := lockOccurList(v,ppc);
  b := 0;
  while (b<a) do begin
    if ppc[b]^.len=1 then begin
      if getliterals(ppc[b])[0]<>v then raise Texc_trk.create('pp-vele: len1-cls prob');
      unlockOccurList(v);
      exit;
    end;
    inc(b);
  end;

  unlockOccurList(v);

  { vitually delete the clauses
  }
  velem_remove_original_clauses_from_litappear(v);
end;




procedure Tpp_solver.remove_single_sided_vars();
var v : Tliteral;
begin
  check_marked_clauses_for_selfsubsumes();
  if notquiet then writeln('c remove_single_sided_vars...');
  repeat
    for v := num_vars downto 1 do maybe_single_sided_var(v);
    velem_hanlde_new_clauses();
    check_marked_clauses_for_selfsubsumes();
  until (rclauselist = nil) or not(keepsolving);
end;























function Tpp_solver.check_cls_blocked(cls1 : Pclause; lst : Tliteral) : boolean;
var alit : Tuliteral; i,lit,sat,taut : Tliteral; lits : Pliteral; cnt : int64; ppc : PPclause; cls2 : Pclause; x : boolean;
begin
  x := true;
  lits := getLiterals(cls1);
  if (cls1=nil) then raise Texc_trk.create('pp-bce: chk nil cls?');
  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := true;
    if trkvar_visited[alit xor 1] 
    then raise Texc_trk.create('pp-bce: blockedcheck on tautology');
  end;


  cnt := lockOccurList(lst,ppc);
  if (cnt=0) then x := false; 
  for cnt := cnt-1 downto 0 do begin
    cls2 := ppc[cnt];
    if cls2^.len=1 then begin 
      raise Texc_trk.create('pp-bce: still chk forced var in var-elem chk');
    end;
    res_count_cls(cls2,sat,taut);
    if taut=1 then begin
      x := false;
      break;
    end else if taut=0 then raise Texc_trk.create('pp-bce: appearlist outdated?');
  end;
  unlockOccurList(lst);

  for i := cls1^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;
  check_cls_blocked := x;
end;




function Tpp_solver.check_bce(v : Tliteral) : boolean;
var alit : Tuliteral; j : Tliteral; cnt : int64; ppc : PPclause; cls1 : Pclause; 
begin
  varvisit[v] := false;

  alit := abs(v);
  alit := 2*alit;
  check_bce := false;
  //if velem_precheck(blit) then exit;

  j := 0;
  for cnt := lockOccurList(v,ppc)-1 downto 0 do begin
    j := ppc[cnt]^.len;
    ref_num_lits += j;
    if j=1 then break; // don't eleminate forced vars
  end;
  unlockOccurList(v);
  if j=1 then exit; // don't eleminate forced vars

  for cnt := lockOccurList(-v,ppc)-1 downto 0 do begin
    j := ppc[cnt]^.len;
    ref_num_lits += j;
    if j=1 then break; // don't eleminate forced vars
  end;
  unlockOccurList(-v);
  if j=1 then exit; // don't eleminate forced vars


  ppstrengthen.clear();

  for cnt := lockOccurList(v,ppc)-1 downto 0 do begin
    cls1 := ppc[cnt];
    if check_cls_blocked(cls1,-v)
    then begin
      ppstrengthen.push(v,cls1);
    end; 
  end;
  unlockOccurList(v);
  while ppstrengthen.pop(v,cls1) do begin
    remove_clause_from_appearlist(cls1);
    ppclsactions.push(v,cls1);
    cls1^.flags := cls1^.flags or clausetag_pp_bce;
    inc(cl_bce);
    add_to_bce_revisitlist(cls1);
    check_bce := true;
  end;

  v := -v;
  for cnt := lockOccurList(v,ppc)-1 downto 0 do begin
    cls1 := ppc[cnt];
    if check_cls_blocked(cls1,-v)
    then begin
      ppstrengthen.push(v,cls1);
    end; 
  end;
  unlockOccurList(v);
  while ppstrengthen.pop(v,cls1) do begin
    remove_clause_from_appearlist(cls1);
    ppclsactions.push(v,cls1);
    cls1^.flags := cls1^.flags or clausetag_pp_bce;
    inc(cl_bce);
    add_to_bce_revisitlist(cls1);
    check_bce := true;
  end;


end;




procedure Tpp_solver.add_to_bce_revisitlist(cls : Pclause);
var lits : Pliteral; i : Tliteral;
begin
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin
    varvisit[abs(lits[i])] := true;
  end; 
end;


procedure Tpp_solver.bce();
var i : Tliteral; x : boolean;
begin
  cl_bce := 0;
  for i := num_vars downto 1 do varvisit[i]:=true;
  repeat
    x := true;
    for i := num_vars downto 1 do begin
      if not(keepsolving) then break;
      if (varvisit[i]) then begin
        check_bce(i);
        x := false;
      end;
    end;
  until x or not(keepsolving);
  if notquiet then writeln('c bce removed ',cl_bce,' clauses.');
end;




