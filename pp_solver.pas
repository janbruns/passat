{$mode objfpc}
unit pp_solver;

{$include config.pas}


interface
uses occur_solver,clause;

type

Ppp_equalities = ^Tpp_equalities;
Tpp_equalities = record
  next : Ppp_equalities;
  lit1,lit2 : Tliteral;
end;

Tpp_solver = class(Toccur_solver)
  public
  constructor create();
  destructor destroy(); override;
  function get_setting_descr(const n : ansistring) : ansistring; override;
  function handle_setting(const n,v : ansistring) : ansistring;override;


  procedure make_unsat_cert(var f : text);

  procedure unlink_all_cls();
  procedure link_all_cls(rnd : boolean);

  procedure remove_litter_literals();
  function remove_litter_literals_from_cls(cls : Pclause) : Tliteral;

  procedure do_freshknownvars_cleanups(rnd : boolean);

  procedure make_lits_signature(lits : Pliteral; len : Tliteral; var s1,s2 : qword);
  procedure make_cls_signature(cls : Pclause);
  procedure make_allcls_signature();
  function cls_is_subsumed(cls : Pclause; len,s1,s2 : qword) : boolean;

  function check_for_subsumed(cls0 : Pclause) : qword;
  function check_for_selfsubsumed(cls0 : Pclause) : qword;
  function check_for_selfsubsumed_v2(cls0 : Pclause) : qword;

  procedure check_marked_clauses_for_selfsubsumes();

  procedure strengthen(cls : Pclause; lit : Tliteral; rec : boolean);

  function do_binreplace() : boolean;
  procedure validate_clause(cls : Pclause);

  procedure validate_all_clauses();

  procedure do_pp();

  procedure add_cls_to_processinglist(cls : Pclause);
  procedure add_all_cls_to_processinglist();

  protected
  procedure clear_clause_flag(stoptag, tag : Tuliteral);
//  procedure mark_clause_refs(maxdepth : qword; tag : Tuliteral);
  procedure unlink_deleted_clauses();
  procedure mark_varreasons_as_clause_ref();
  procedure release_deleted_clauses();
  procedure calc_avg_clauselen(cnttag : Tuliteral);

  procedure check_for_satisfied_clauses();

  function get_cls_from_processinglist() : Pclause;





  function count_cls() : double;


  procedure remove_clause_from_appearlist(cls : Pclause);
  procedure insert_clause_into_appearlist(cls : Pclause);
  procedure register_new_binary_clause(lit1,lit2 : Tliteral);
  procedure unregister_new_binary_clause(lit1,lit2 : Tliteral);
  procedure make_big(p : Pint64);
  procedure _update_big(lit1,lit2 : Tliteral);
  procedure unupdate_big(lit1,lit2 : Tliteral);
  procedure _unupdate_big(lit1,lit2 : Tliteral);
  procedure update_big(lit1,lit2 : Tliteral);
  procedure destroy_big();
  procedure handle_appearlist_effects_of_literal_drop(cls : Pclause; lit : Tliteral);
  procedure init_appearlist();

  function get_big_lits(lit : Tliteral; var lits : Pliteral) : Tliteral; override;




  { hle, hidden literaal elemination  implemented in pp_imp_hl.pas}
  procedure pp_hle_add(elit : Tliteral);
  procedure pp_get_hl(lit0 : Tliteral); override;
  procedure pp_unget_hl(lit0 : Tliteral); override;
  procedure pp_hle_lit(lit : Tliteral);
  procedure pp_hle_lit_apply(cls : Pclause; lit0 : Tliteral);
  procedure pp_hle_v();
  procedure pp_hle_v2();

  { bce (blocked clause elemination, implemented in pp_imp_ver.pas}
  function check_cls_blocked(cls1 : Pclause; lst : Tliteral) : boolean;
  function check_bce(v : Tliteral) : boolean;
  procedure add_to_bce_revisitlist(cls : Pclause);
  procedure bce();

  { var elemination implemented in pp_imp_ver.pas}
  function do_var_elem(limcl,limlit : Tliteral) : boolean;
  procedure add_literal_appearlist_to_processinglist(lit : Tliteral);
  procedure velem_hanlde_new_clauses();
  procedure velem_remove_original_clauses_from_litappear(v : Tliteral);
  procedure res_count_cls(cls : Pclause; var sat,taut : Tliteral);
  function check_cls_resolution(cls1 : Pclause; lst : Tliteral) : boolean;
  procedure do_cls_resolution(cls1 : Pclause; lst : Tliteral);
  procedure make_resolution_cls(cls1,cls2 : Pclause; lst : Tliteral; sat : Tliteral);
  function velem_precheck(var lit : Tliteral) : boolean;
  function velem(v,limcl,limlit : Tliteral) : boolean;

  procedure maybe_single_sided_var(v : Tliteral);
  procedure remove_single_sided_vars();

  {  els, equivalent literal substitution }
  procedure find_lit_equalities(lit0 : Tliteral);
  function eq_block_cyc() : boolean;
  function enum_equalities() : boolean;


  protected
  function big_has_marked_var(lit : Tliteral) : Tliteral; override;




  public
  clscnt_deleted,old_clscnt_deleted : qword;
  avg_cls_len : double;
  unsatcert_with_del : boolean;
  ppclsactions : Tpropagatelist; // used for sat-proof with bce, hle, ve, els
  unsatcert_printemptyclause : boolean;
  hl_deactivated, allow_bce, allow_velem, allow_els, allow_pp : boolean;

  private
  binaliases : array of Tliteral;
  binreplace : array of Tliteral;
  have_fresh_bincls_about : array of boolean;

  rclauselist : Pclause;
  ppactions, // used to record clause mods like strengthened out lits, don't confuse with ppclsactions or ppstrengthen
  ppstrengthen : Tpropagatelist; // used to rrecord clauses that need strechngen-checks
  max_lits_per_var_subs,
  max_lits_per_var_selfsubs : double;
  obvious_tautologies_removed : qword;
  occurlist_valid,have_fresh_binary_cls : boolean;


  pp_hllen : Tliteral;
  pp_hle_v_rm : qword;
  pp_equalities : Ppp_equalities;
  latest_niver_clause : Pclause;


  ref_num_cls,ref_num_lits,res_lit,res_cl : Tliteral;

  bin_impl_graph : array of Pliteral; // per lit: offs0: memlen, offs1: len, offs2 ... literals

  cl_bce : qword;

  hl_work, sss_work : qword;
  hl_workscale : double;
  pp_reloop_deeper, pp_reloop_deepest, hl_autolimit : boolean;

  ver_complete_resub : longint;
  velem_cllimit,
  velem_litlimit : Tliteral;
  subs_workaround : qword;
  pp_used_hle : boolean;
end;




implementation
uses print,bare_solve_struct,sort;



{$include pp_imp_appearlist.pas}
{$include pp_imp_subsume.pas}
{$include pp_imp_ver.pas}
{$include pp_imp_hl.pas}
{$include pp_imp_els.pas}



constructor Tpp_solver.create();
begin
  inherited create();

  setlength(bin_impl_graph,0);

  unsatcert_with_del := false;
  clscnt_deleted := 0;
  old_clscnt_deleted := 0;
  ppactions := Tpropagatelist.create();
  ppstrengthen := Tpropagatelist.create();
  ppclsactions := Tpropagatelist.create();
  max_lits_per_var_subs := 100;
  max_lits_per_var_selfsubs := 100;
  obvious_tautologies_removed := 0;
  rclauselist := nil;
  occurlist_valid := false;

  latest_niver_clause := nil;
  hl_work := 0;
  sss_work := 0;
  hl_workscale := 0.1;
  allow_pp := true;
  hl_deactivated := false;
  allow_bce := true;
  allow_velem := true;
  allow_els := false;
  velem_cllimit := 0;
  velem_litlimit := 20;
  pp_reloop_deeper := false;
  pp_reloop_deepest := false;
  hl_autolimit := true;

  add_setting('allow_pp');
  add_setting('allow_velem');
  add_setting('allow_bce');
  add_setting('allow_els');
  add_setting('allow_hle');
  add_setting('pp_reloop_depth');
  add_setting('velem_cllimit');
  add_setting('velem_litlimit');
  add_setting('trk_litredchk_limit');
  add_setting('allow_big_based_learnt_simp');

//  add_setting('allow_trk_minimize_lbd');
  add_setting('allow_trk_hle');

  unsatcert_printemptyclause := true;
end;




destructor Tpp_solver.destroy();
begin
  destroy_big();
  ppactions.destroy();
  ppstrengthen.destroy();
  ppclsactions.destroy();
  inherited destroy();
end;




{ Methods used on clause db simplification during solve process.
}


procedure Tpp_solver.unlink_deleted_clauses();
var i : Tliteral;
begin
  for i := 0 to num_vars do begin
    remove_marked_clauses_from_chains(varwatches, i,clausetag_linkless);
  end;
end;


procedure Tpp_solver.unlink_all_cls();
var  i : Tuliteral; cls : Pclause;
begin
  for i := num_vars*4+3 downto 0 do varwatches[i] := nil;
  cls := latestclause;
  while (cls<>nil) do begin
    cls^.nxt[0] := nil;
    cls^.nxt[1] := nil;
    cls^.nxt[2] := nil;
    cls^.nxt[3] := nil;
    cls := cls^.prv;
  end;
end;

procedure Tpp_solver.link_all_cls(rnd : boolean);
var cls : Pclause; lits : Pliteral;
begin
  unlink_all_cls();
  cls := latestclause;
  while (cls<>nil) do begin
    if (cls^.flags and clausetag_linkless)=0 then begin
      if rnd then randomize_cls(cls);
      search_link_targets(cls);
      lits := getLiterals(cls);
      if (cls^.len>1) then begin
        relink_cls_to(cls, 0, 0);
        relink_cls_to(cls, 1, 1);
      end else if (cls^.len=1) then begin
        propagations.push(lits[0],cls);
      end;
    end;
    cls := cls^.prv;
  end;
end;




  
procedure Tpp_solver.mark_varreasons_as_clause_ref();
var i : Tliteral; cls : Pclause;
begin
  for i := 1 to num_vars do begin
    if (varstate[i]<>0) then begin
      cls := varreason[i];
      cls^.flags := cls^.flags or clausetag_hasrefs;
//      while (cls<>nil) do begin
//        cls^.flags := cls^.flags or clausetag_hasrefs;
//        cls := cls^.rnxt;
//      end;
    end;
  end;
end;

  

procedure Tpp_solver.release_deleted_clauses();
var cls,cls2 : Pclause;
begin
  while (latestclause<>nil)and((latestclause^.flags and clausetag_deleted)>0) do begin
    cls := latestclause^.prv;
    cl_spool.discard_clauseextra(latestclause);
    releaseClause(latestclause);
    latestclause := cls;
    inc(clscnt_deleted);
  end;
  cls := latestclause;
  while (cls<>nil) do begin
    cls2 := cls^.prv;
    if (cls2<>nil) then begin
      if (cls2^.flags and clausetag_deleted)>0 then begin
        cls^.prv := cls2^.prv;
        cl_spool.discard_clauseextra(cls2);
        releaseClause(cls2);
        inc(clscnt_deleted);
      end else cls := cls2;
    end else break;
  end;
end;

procedure Tpp_solver.calc_avg_clauselen(cnttag : Tuliteral);
var cls : Pclause; nc,nl : qword;
begin
  nc := 0;
  nl := 0;
  cls := latestclause;
  while (cls<>nil)and((cls^.flags and cnttag)>0) do begin
    inc(nc);
    nl += cls^.len;
    cls := cls^.prv;
  end;
  if (nc>0) then avg_cls_len := nl/nc else avg_cls_len := 0;
end;

procedure Tpp_solver.clear_clause_flag(stoptag, tag : Tuliteral);
var cls : Pclause;
begin
  cls := latestclause;
  while (cls<>nil)and((cls^.flags and stoptag)>0) do begin
    cls^.flags := cls^.flags and not(tag);
    cls := cls^.prv;
  end;
end;




function Tpp_solver.remove_litter_literals_from_cls(cls : Pclause) : Tliteral;
var lits : Pliteral; lit,alit,i,n : Tliteral;
begin
  remove_litter_literals_from_cls := 0;
  n := 0;
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    if varstate[alit]=-lit then begin
      ppactions.push(cls^.len-1,cls);
      lits[i] := lits[cls^.len-1];
      lits[cls^.len-1] := lit;
      cls^.len -= 1;
      cls^.rm += 1;
      n += 1;
    end;
  end;
//if (n>0) then printclause(lits,cls^.len+n,true,true,output);
  if (cls^.len=0) then begin
    conflict_clause := cls;
    raise Tppunsat.create('unsat during pp.');
  end;
  remove_litter_literals_from_cls := n;
end;


procedure Tpp_solver.remove_litter_literals();
var cls : Pclause; c,cll,clc : qword;
begin
  c := 0;
  clc := 0;
  cll := 0;
  cls := latestclause;
  while (cls<>nil) do begin
    if (cls^.flags and clausetag_linkless)=0 then begin
      c := remove_litter_literals_from_cls(cls);
      if (c>0) then inc(clc);
      cll += c;
    end;
    cls := cls^.prv;
  end;
  if notquiet then writeln('c removed ',cll,' litter literals from ',clc,' clauses.');
end;





procedure Tpp_solver.do_freshknownvars_cleanups(rnd : boolean);
begin
  if (new_known_vars>0) then begin
    check_for_satisfied_clauses();
    // remove_litter_literals();
    link_all_cls(rnd);
    new_known_vars := 0;
  end;
end;



procedure Tpp_solver.check_for_satisfied_clauses();
var cls : Pclause;
begin
  cls := latestclause;
  while (cls<>nil) do begin
    if (cls^.flags and clausetag_linkless)=0 then begin
      if cls_sat(cls) then cls^.flags := cls^.flags or clausetag_linkless;
    end;
    cls := cls^.prv;
  end;
end;









{ Method about bulding an unsat certificate
}


procedure Tpp_solver.make_unsat_cert(var f : text);
var cls,cls2,cls3,cls4 : Pclause; i,len,flg,cc,c,ccu : Tliteral; ppc : PPclause; ulen : Tuliteral;
begin
  pp_used_hle := pp_used_hle or used_big_based_learnt_simp;

  cls2 := nil;
  i := 0;
  if pp_used_hle then while i<ppactions.len do begin
    { unconditionally print most pp actions into cert, if hle was used.
      filter out multiple operations in a row on the same clause 
    }
    cls := ppactions.reasons[i];
    len := ppactions.lits[i];
    inc(i);
    if (i<ppactions.len) then cls3 := ppactions.reasons[i] else cls3 := nil;
    if (cls<>cls3) 
    then printclause(getLiterals(cls),len,false,true,f);
    cls2 := cls;
  end;
  ccu := 0;
  cc := 0;
  c := 0;

  cls := latestclause;
  while (cls<>nil) do begin
    cls^.nxt[0] := nil;
    cls^.nxt[1] := nil;
    cls^.rnxt := nil;
    cls := cls^.prv;
  end;

  cls := conflict_clause;
  if (cls<>nil) then cls^.flags := cls^.flags or clausetag_incert;

  { Mark all assigned var reasons as incert, even if such vars didn't contribute to unsat.
    This is done to ease up optimizations during solve (like "strengthen known lits out",
    without notice: without adding the varreason to any strengthend cls refs).
    The intention already isn't to keep the cert clean, but to keep it short.
  }
  for i := 1 to num_vars do begin
    if (varstate[i]<>0) then begin
      cls := varreason[i];
      if (cls<>nil) then begin
        cls^.flags := cls^.flags or clausetag_incert;
        cls := cls^.rnxt;
      end;
    end;
  end;

  cls := latestclause;
  while (cls<>nil) do begin
    { include all incert-clause references as incert, and prepare del-info,
      and also generate the backlink cls^.rnxt to match clause creation order
    }
    flg := cls^.flags;
    if (flg and clausetag_incert)>0 then begin
      ulen := 0;
      ppc := cl_spool.get_cls_extra(cls,ulen);
      len := ulen;
      for i := len-1 downto 0 do begin
        cls2 := ppc[i];
        if (cls2^.flags and clausetag_incert)=0 then begin
          { cls is the last necessary clause referencing cls2 }
          cls2^.flags := cls2^.flags or clausetag_incert;
          cls2^.nxt[0] := cls^.nxt[1];
          cls^.nxt[1] := cls2;
        end;
      end;
    end;
    cls2 := cls^.prv;
    if (cls2<>nil) then begin
      cls2^.rnxt := cls;
      cls := cls2;
    end else break;
  end;

  i := 0;
  cls3 := nil;
  if not pp_used_hle then while i<ppactions.len do begin
    {conditionally print all referenced pp actions into cert, if hle was not used}
    cls2 := ppactions.reasons[i];
    len := ppactions.lits[i];
    inc(i);
    if i<ppactions.len then cls4 := ppactions.reasons[i] else cls4 := nil;
    if ((cls2^.flags and clausetag_incert)>0)
    and ((cls2<>cls3)or(cls2<>cls4))
    then printclause(getLiterals(cls2),len,false,true,f);
    cls3 := cls2;
  end;

  while (cls<>nil) do begin
    { print all referenced clauses + delete info already prepared above}
    flg := cls^.flags;
    if (flg and clausetag_incert)>0 then begin
      if (flg and clausetag_prob)=0 then begin
        printclause(getLiterals(cls),cls^.len,false,true,f);
        cc += 1;
      end;
      c += 1;
      if cls=latestclause then begin
        { add the empty clause. But only this unsat-cert really is about unsat situation. }
        if unsatcert_printemptyclause then writeln(f,'0')
      end else begin
        cls2 := cls^.nxt[1];
        if unsatcert_with_del then while (cls2<>nil) do begin
          write(f,'d ');
          printclause(getLiterals(cls2),cls2^.len,false,true,f);
          cls2 := cls2^.nxt[0];
          ccu += 1;
        end;
      end;
    end;
    cls := cls^.rnxt;
  end;
  if notquiet then writeln('c ',cc,' out of ',c,' clauses + ',ccu,' deletions in unsat-cert');
end;









{ ============PREPROCESSING=========== 
}




{ Clause Validation 

  validate_clause():

  a) remove duplicate literals
  b) sort out obvios tautologies.
  
}

procedure Tpp_solver.validate_clause(cls : Pclause);
var lits : Pliteral; i,lit : Tliteral; alit : Tuliteral; d : boolean;
begin
  if (cls=nil) 
  or ((cls^.flags and clausetag_pp_del)>0) 
  then exit;

  d := false;

  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    if trkvar_visited[alit xor 1] then begin
      cls^.flags := cls^.flags or clausetag_pp_del;
      obvious_tautologies_removed += 1;
    end else if trkvar_visited[alit] then begin
      lits[i] := lits[cls^.len-1];
      lits[cls^.len-1] := lit;
      cls^.len -=  1;
      cls^.rm += 1;
      d := true;
    end;
    trkvar_visited[alit] := true;
  end;
  for i := cls^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;
  if d and ((cls^.flags and clausetag_pp_del)>0) then begin
    ppactions.push(cls^.len,cls);
  end;
end;

procedure Tpp_solver.validate_all_clauses();
var cls : Pclause; un : qword;
begin
  un := 0;
  cls := latestclause;
  while (cls<>nil) do begin
    validate_clause(cls);
    if cls^.len=1 then inc(un);
    cls := cls^.prv;
  end;
  if notquiet then writeln('c found ',obvious_tautologies_removed,' obvious tautologies');
  if notquiet then writeln('c found ',un,' unit clauses');
end;






{ remove literal lit from a clause 
}

procedure Tpp_solver.strengthen(cls : Pclause; lit : Tliteral; rec : boolean);
var i : Tliteral; lits : Pliteral;
begin
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin
    if (lits[i]=lit) then begin
      if rec then ppactions.push(cls^.len-1,cls);
      //remove_clause_from_appearlist(cls);
      lits[i] := lits[cls^.len-1];
      lits[cls^.len-1] := lit;
      cls^.len -= 1;
      cls^.rm += 1;
      make_cls_signature(cls);
      add_cls_to_processinglist(cls);
      if (cls^.len=0) then begin
        conflict_clause := cls;
        raise Tppunsat.create('unsat during pp.');
      end else handle_appearlist_effects_of_literal_drop(cls,lit);//insert_clause_into_appearlist(cls);
      exit;
    end else if (lits[i]=-lit) then begin
      raise Texc_trk.create('pp: strengthening found tautology');
    end;
  end;
  raise Texc_trk.create('pp: strengthening didn''t find literal to remove');
end;




procedure Tpp_solver.add_literal_appearlist_to_processinglist(lit : Tliteral);
var i : int64; ppx : ppclause;
begin
  for i := lockOccurList(lit,ppx)-1 downto 0 do begin
    add_cls_to_processinglist(ppx[i]);
  end;
  unlockOccurList(lit);
end;



function Tpp_solver.count_cls() : double;
var cc,lc,cce : qword; a,b,c,d : int64; ppx : PPclause; cls : Pclause; i,nv2,nv1,nv0 : Tliteral; j : Tuliteral;
begin
  cce := 0;
  cc := 0;
  lc := 0;
  cls := latestclause;
  while (cls<>nil) do begin
    inc(cc);
    if ((cls^.flags and clausetag_pp_discard)=0) then begin
      inc(cce);
      lc += cls^.len;
    end;
    cls := cls^.prv;
  end;
  nv2 := 0;
  nv1 := 0; nv0 := 0;
  for i := num_vars downto 1 do begin
    j := i;
    j := j*2;
    c := lazylenOccurList( i);
    d := lazylenOccurList(-i);
    a := lockOccurList( i,ppx); unlockOccurList( i);
    b := lockOccurList(-i,ppx); unlockOccurList(-i);
    if (b>0)and(a>0) then inc(nv2)
    else if (b>0)or(a>0) then inc(nv1)
    else inc(nv0);
    if (a<>c) or (d<>b) then if notquiet then writeln('c mismatched lazylenOccurList: ',a,'/',c,' ',b,'/',d);
  end;  
  if notquiet then writeln('c stat: ',cce,' of ',cc,' clauses with a total of ',lc,' literals, ',nv2,'/',num_vars,' vars (',nv1,' single-sided, ',nv0,' empty)');
  count_cls := lc / (num_vars);
end;




procedure Tpp_solver.init_appearlist();
var cls : Pclause; i : int64; j,lit : Tliteral; alit : Tuliteral; lits : Pliteral; vcl,bvcl : array of int64;
begin
  setlength( vcl,int64(num_vars+1)*2);
  setlength(bvcl,int64(num_vars+1)*2);
  for i := high( vcl) downto 0 do vcl[i] := 0;
  for i := high(bvcl) downto 0 do vcl[i] := 0;
  cls := latestclause;
  while (cls<>nil) do begin
    if ((cls^.flags and clausetag_pp_discard)=0) then begin 
      lits := getLiterals(cls);
      for j := cls^.len-1 downto 0 do begin
        lit := lits[j];
        alit := abs(lit);
        alit := 2*alit +ord(lit<0);
        inc(vcl[alit]);
        if (cls^.len=2) then inc(bvcl[alit]);
      end;
    end;
    cls := cls^.prv;
  end;
  initOccurLists(@vcl[0]);
  make_big(@bvcl[0]);
  cls := latestclause;
  while (cls<>nil) do begin
    if ((cls^.flags and clausetag_pp_discard)=0) then begin 
      insert_clause_into_appearlist(cls);
    end;
    cls := cls^.prv;
  end;
end;



procedure Tpp_solver.do_pp();
var q : int64;
begin
  pp_used_hle := false;

  ver_complete_resub := 2;

  setlength(have_fresh_bincls_about,Tuliteral(num_vars+1)*2);
  setlength(binreplace,Tuliteral(num_vars+1)*2); 
  setlength(binaliases,Tuliteral(num_vars+1)*2); 
  for q := high(binreplace) downto 0 do binreplace[q] := q;
  for q := high(binaliases) downto 0 do binaliases[q] := q;


  have_fresh_binary_cls := true;
  for q := Tuliteral(num_vars+1)*2-1 downto 0 do begin
    have_fresh_bincls_about[q] := true;
  end;


  validate_all_clauses();
  make_allcls_signature();
  add_all_cls_to_processinglist();
  init_appearlist();

  if keepsolving then count_cls();
  if not allow_pp then exit;
  if not keepsolving then exit;

  repeat
    repeat
      repeat
        repeat

          check_marked_clauses_for_selfsubsumes();
          //if not hl_deactivated then pp_hle_v2();
        until (rclauselist = nil) or not(keepsolving);
        if allow_velem then do_var_elem(velem_cllimit,velem_litlimit);

      until (rclauselist = nil) or not(keepsolving);

      if allow_els and enum_equalities() then begin
        destroy_big();
        destroyOccurList();
        init_appearlist();
        add_all_cls_to_processinglist();
        for q := high(binreplace) downto 0 do binreplace[q] := q;
        for q := high(binaliases) downto 0 do binaliases[q] := q;
        have_fresh_binary_cls := true;
        for q := Tuliteral(num_vars+1)*2-1 downto 0 do begin
          have_fresh_bincls_about[q] := true;
        end;
      end;
      if not hl_deactivated then pp_hle_v2();

      if not pp_reloop_deeper then break;
    until ((rclauselist = nil) and (hl_deactivated or (pp_hle_v_rm=0))) or not(keepsolving); 
    cl_bce := 0;
    if allow_bce then bce();
//    break;
    if not hl_deactivated then pp_hle_v2();
    ver_complete_resub := 2;
    if allow_velem then do_var_elem(velem_cllimit,velem_litlimit);
    if not pp_reloop_deepest then begin
      check_marked_clauses_for_selfsubsumes();
      break;
    end;
//break;
  until (rclauselist = nil) or not(keepsolving);



  if keepsolving then remove_single_sided_vars();
  if keepsolving then count_cls();

  destroyOccurList();
//  destroy_big();
  setlength(binreplace,0); 
  setlength(binaliases,0); 
  setlength(have_fresh_bincls_about,0);
  rclauselist := nil;
  occurlist_valid := false;
end;




function Tpp_solver.get_setting_descr(const n : ansistring) : ansistring;
begin
  if (n='allow_pp') then begin
    get_setting_descr := 'Enable (1) or disable (0) preprocessing. When disabled, only TE (obvious tautology elemination) and duplicate clause elemination is applied by the loader. When turned on, preprocessing based on SE (subsumption elemination) and SSE (self subsumption) takes place, optionally combined with VE, BCE, ELS, HLE.'#10
  end else if (n='allow_velem') then begin
    get_setting_descr := 'Enable (1) or disable (0) VE (var elemination). The result of applying VE is a formula, that doesn''t contain the eleminated vars anymore, but all the constraints to keep everything logically equivalent (except for the assignment to the eleminated vars, that should internally get their correct assignment afterward -this part is currently unimplemented-).). Note: velem can have "2 speeds" in this implementation. This caused by 2 different ways of internally dealing with update-info (it is relatively expensive to track all clauses that might have subsumption-relevant changes).';
  end else if (n='allow_bce') then begin
    get_setting_descr := 'Enable (1) or disable (0) BCE (blocked clause elemination). The result of applying BCE isn''t logically equivalent to the input formula, but satisfiablity is preserved. The eleminated clauses aren''t required for prooving unstisfiability, and in case of a satisfiable formula, the solver works on an instance that allows for some more solutions (that should internally get corrected -this part is currently unimplemented-).';
  end else if (n='allow_els') then begin
    get_setting_descr := 'Enable (1) or disable (0) ELS (equivalent literal substitution). Eleminates vars that have an equivalent alias (identified by the existance of two specific clauses on that two vars). Note that after ELS sucessfully eleminated something, most data structures of the preoprocesor are drawn invalid and need some time for rebuild.';
  end else if (n='allow_hle') then begin
    get_setting_descr := 'Enable (2 or 1) or disable (0) HLE (hidden literal elemination). HLE tries to strengthen clauses by removing irrelevant literals according to binary implications. Can be slow, so the default (1) is to turn it off after some amount of computation rellative to work spent on other methods. Say (2) to force unlimited HLE processing. Note: using HLE can have the side-effect of heavily increasing the size of unsat certificates for more or less implementation-specific reasons.';
  end else if (n='pp_reloop_depth') then begin
    get_setting_descr := 'After applying some of the simplifications techniques to their respective fixpoint, other techniques might be applied again. Say (2) to get deepest recycling, (1) medium recycling, and (0) to merely apply most methods only once.';
  end else if (n='velem_cllimit') then begin
    get_setting_descr := 'An integer specifiying a clause limit in VE. When deciding wether or not to eleminate a var, velem_cllimit specifies the maximum formula growth in terms of number of clauses. Default is (0), specifying that VE never leaves with more than zero "additional" clauses per eleminated var.';
  end else if (n='velem_litlimit') then begin
    get_setting_descr := 'An integer specifiying a clause limit in VE. When deciding wether or not to eleminate a var, velem_litlimit specifies the maximum formula growth in terms of number of clause-literals. Default is (20), specifying that VE never leaves with more than 20 additional clause literals per eleminated var.';
  end else if (n='trk_litredchk_limit') then begin
    get_setting_descr := 'During conflict analysis, this integer value defines how hard to try to remove literals from the conflict clause by reason expansion. 0 turns it off, great values allow for complicated checks with low probability of successful literal removal, but there''s also some natural, problem instance/solve state inherent complexity limit. Default value 10.';
  end else if (n='allow_big_based_learnt_simp') then begin
    get_setting_descr := 'During conflict analysis, this option controls wether to apply further learnt-clause simplification by referencing the clause database. Because of implementation specific limitations, there''s a drawback related to unsat certificate complexity (the same effect as with --allow_hle=1).';
  end else if (n='allow_trk_hle') then begin
    get_setting_descr := 'During conflict analysis, this option controls wether (1) or not (0) to apply relatively expensive (partial) hle minimization on learnt. Probably doesn''t have very much overall effect.';
  end else if (n='allow_trk_minimize_lbd') then begin
    get_setting_descr := 'During conflict analysis, this option controls wether (1) or not (0) to attempt to minimize some internal value called lbd. Probably doesn''t have very much overall effect.';
  end else get_setting_descr := inherited get_setting_descr(n);
end;

function Tpp_solver.handle_setting(const n,v : ansistring) : ansistring;
var l : Tliteral; w : word;
begin
  if (n='allow_pp') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then allow_pp := false
    else if (w=0)and(l=1) then allow_pp := true
    else handle_setting := 'vnok'
  end else if (n='allow_velem') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then allow_velem := false
    else if (w=0)and(l=1) then allow_velem := true
    else handle_setting := 'vnok'
  end else if (n='allow_bce') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then allow_bce := false
    else if (w=0)and(l=1) then allow_bce := true
    else handle_setting := 'vnok'
  end else if (n='allow_els') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then allow_els := false
    else if (w=0)and(l=1) then allow_els := true
    else handle_setting := 'vnok'
  end else if (n='allow_hle') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then hl_deactivated := true
    else if (w=0)and(l=1) then begin hl_deactivated := false; hl_autolimit := true;  end
    else if (w=0)and(l=2) then begin hl_deactivated := false; hl_autolimit := false; end
    else handle_setting := 'vnok'
  end else if (n='pp_reloop_depth') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then begin pp_reloop_deeper := false; pp_reloop_deepest := false; end
    else if (w=0)and(l=1) then begin pp_reloop_deeper := true; pp_reloop_deepest := false; end
    else if (w=0)and(l=2) then begin pp_reloop_deeper := true; pp_reloop_deepest := true;  end
    else handle_setting := 'vnok'
  end else if (n='velem_cllimit') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0) then begin velem_cllimit := l; end
    else handle_setting := 'vnok'
  end else if (n='velem_litlimit') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0) then begin velem_litlimit := l; end
    else handle_setting := 'vnok';
  end else if (n='trk_litredchk_limit') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0) then begin trk_litredchk_limit := l; end
    else handle_setting := 'vnok';
  end else if (n='allow_trk_hle') then begin
    handle_setting := 'ok';
    if (v='1') then allow_trk_hle := true
    else if (v='0') then allow_trk_hle := false
    else handle_setting := 'vnok';
  end else if (n='allow_trk_minimize_lbd') then begin
    handle_setting := 'ok';
    if (v='1') then allow_trk_minimize_lbd := true
    else if (v='0') then allow_trk_minimize_lbd := false
    else handle_setting := 'vnok';
  end else if (n='allow_big_based_learnt_simp') then begin
    handle_setting := 'ok';
    if v='0' then allow_big_based_learnt_simp := false
    else if v='1' then allow_big_based_learnt_simp := true
    else handle_setting := 'vnok'
  end else handle_setting := inherited handle_setting(n,v);
end;



begin
end.