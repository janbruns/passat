{$mode objfpc}
unit choose_solver;

{$include config.pas}


interface
uses trkpst_solver,clause;

type


Tchoose_solver = class(Ttrkpst_solver)
  public
  constructor create();
  function handle_setting(const n,v : ansistring) : ansistring; override;
  function get_setting_descr(const n : ansistring) : ansistring; override;


  function choose() : Tliteral;

  procedure onAfterLoad(); override;



  protected
  procedure onAfterTackeBack(); override;

  function choose_sign(c : Tliteral) : Tliteral;
  function rechoose_sign(c : Tliteral) : Tliteral;

  function choose_forced() : Tliteral;
  function choose_by_name() : Tliteral;


  function choose_from_recent_cls() : Tliteral;
  function choose_from_all_cc() : Tliteral;
  function choose_from_ccvars() : Tliteral;

  function choose_berkmin_sign(c : Tliteral) : Tliteral;


  public
  procedure onStartSolveCycling(); virtual;


  private
  function choose_cmp_lits(lit,old : Tliteral) : boolean;
  function choose_by_revname() : Tliteral;
  function choose_by_fwdname() : Tliteral;


  public
  force_choose : array of Tliteral;
  backtracks : qword;

  protected
  choosing_recentcls : Pclause; // =latestclause after bt

  ch_recent_mintestcandidates : Tliteral;
  ch_probab_sign_use_equalize_ch : double;
  ch_probab_sign_flip : double;
  ch_probab_oldstate : double;
  ch_probab_true : double;
  choose_probab_cc : double;
  choose_ccnxt : Tliteral;
  ch_cbn_start,ch_cbn_revstart : Tliteral;
  ch_recent_satdiscard : boolean;
  choose_enterhint : boolean;
  choose_by_name_cutdl : Tliteral;
  ch_use_sat_sig : Tliteral;
  ch_probab_berksig, ch_probab_inv_berksig : double;

  rech_probab_berksig,
  rech_probab_oldstate,
  rech_probab_true,
  rech_probab_sign_flip,
  rechoose_probab_cc : double;
end;


implementation
uses bare_solve_struct;

constructor Tchoose_solver.create();
begin
  inherited create();

  add_setting('choose_probab_cc');
  add_setting('choose_by_name_cutdl');
  add_setting('ch_probab_berksig');
  add_setting('ch_probab_inv_berksig');


  add_setting('ch_probab_sign_use_equalize_ch');
  add_setting('ch_probab_oldstate');
  add_setting('ch_probab_true');


  add_setting('ch_probab_sign_flip');
  add_setting('ch_add_forced_choice');
  add_setting('ch_rcntclssig_sat');
  add_setting('ch_rcntcls_satdiscard');
  add_setting('ch_recent_mintestcandidates');

  add_setting('rech_probab_sign_flip');
  add_setting('rech_probab_oldstate');
  add_setting('rech_probab_true');
  add_setting('rech_probab_berksig');
  add_setting('rechoose_probab_cc');

  setlength(force_choose,0);


//var choosing
  choose_probab_cc := 1.0;
  rechoose_probab_cc := 1.0;
  choose_by_name_cutdl := 0;
  ch_recent_satdiscard := true;
  ch_recent_mintestcandidates := 1;

//sign choosing
  ch_probab_inv_berksig := 0.0;
  ch_use_sat_sig := 0;
  ch_probab_sign_use_equalize_ch := 0.0;


  ch_probab_berksig := 0.0;
  rech_probab_berksig := 0.0;

  ch_probab_oldstate := 0.0;
  rech_probab_oldstate := 0.0;

  ch_probab_true := 0.0;
  rech_probab_true := 0.0;

  ch_probab_sign_flip := 0.5;
  rech_probab_sign_flip := 0.5;


  backtracks := 0;
  ch_cbn_start := 1;
  ch_cbn_revstart := num_vars;
end;

procedure Tchoose_solver.onStartSolveCycling();
begin
  maybe_repair_ccnxt_order();
  choosing_recentcls := latestclause;
  choose_ccnxt := top_ccnxt;
  ch_cbn_start := 1;
  ch_cbn_revstart := num_vars;
end;

procedure Tchoose_solver.onAfterTackeBack();
begin
  inc(backtracks);
  maybe_repair_ccnxt_order();
  choosing_recentcls := latestclause;
  choose_ccnxt := top_ccnxt;
  ch_cbn_start := 1;
  ch_cbn_revstart := num_vars;
end;

procedure Tchoose_solver.onAfterLoad();
begin
  inherited onAfterLoad();
end;

function Tchoose_solver.choose_forced() : Tliteral;
var i,lit,alit,c : Tliteral;
begin
  c := 0;
  for i := decilevel to high(force_choose) do begin
    lit := force_choose[i];
    alit := abs(lit);
    if (varstate[alit]=0)and(varchooseable[alit]=0) then begin
      c := lit;
      break;
    end;
  end;
  choose_forced := c;
end;



function Tchoose_solver.choose_by_revname() : Tliteral;
var i,c : Tliteral;
begin
  c := 0;
  for i := ch_cbn_revstart downto 1 do begin
    if (varstate[i]=0)and(varchooseable[i]=0) then begin
      c := i;
      ch_cbn_revstart := i;
      break;
    end;
  end;
  choose_by_revname := c;
end;

function Tchoose_solver.choose_by_fwdname() : Tliteral;
var i,alit,c : Tliteral;
begin
  c := 0;
  alit := num_vars;
  for i := ch_cbn_start to alit do begin
    if (varstate[i]=0)and(varchooseable[i]=0) then begin
      c := i;
      ch_cbn_start := i;
      break;
    end;
  end;
  choose_by_fwdname := c;
end;

function Tchoose_solver.choose_by_name() : Tliteral;
var c : Tliteral;
begin
  if (choose_by_name_cutdl>0) then begin
    if (decilevel<choose_by_name_cutdl) 
    then c := choose_by_fwdname()
    else c := 0;
  end else if (choose_by_name_cutdl<0) then begin
    if (decilevel<-choose_by_name_cutdl) 
    then c := choose_by_revname()
    else c := 0;
  end else c := 0;
  if (c<>0) then c := choose_sign(c);
  choose_by_name := c;
end;


function Tchoose_solver.choose_cmp_lits(lit,old : Tliteral) : boolean;
begin
  lit := abs(lit);
  old := abs(old);
  if (lit=old) then choose_cmp_lits := false
  else choose_cmp_lits := varccnt[lit]>varccnt[old];
end;

function Tchoose_solver.choose_from_recent_cls() : Tliteral;
var i,lit,alit,c,cnt : Tliteral; cls : Pclause; lits : Pliteral; discard : boolean;
begin
  c := 0;
  cnt := 0;
  cls := choosing_recentcls;
  while (cls<>nil) do begin
    discard := false;
    if (ch_recent_satdiscard) then discard := cls_sat(cls);
    if not discard then begin
      lits := getLiterals(cls);
      for i := cls^.len-1 downto 0 do begin
        lit := lits[i];
        alit := abs(lit);
        if (varstate[alit]=0)and(varchooseable[alit]=0) then begin
          inc(cnt);
          if (c=0) or choose_cmp_lits(lit,c) then c := lit;
        end;
      end;
      if (c<>0)and(cnt>=ch_recent_mintestcandidates) then begin
        choose_from_recent_cls := c;
        exit;
      end;
    end;
    if (cnt=0) then choosing_recentcls := cls^.prv;
    cls := cls^.prv;
  end;
  if ch_use_sat_sig=0 then c := choose_berkmin_sign(c);
  choose_from_recent_cls := c;
end;




{ fallback-procedure to choose from all vars by cc in case 
  of destroyed order. Might sometimes be less expensive than
  to keep order, if
  num_conflv*log(num_conflv) plus some depence on num_vars
  is greater than
  decisions_per_conflict*num_vars 
}
function Tchoose_solver.choose_from_all_cc() : Tliteral;
var i,best : Tliteral; x,bestcc : double;
begin
  bestcc := 0;
  best := 0;
  for i := 1 to num_vars do begin
    if (varstate[i]=0)and(varchooseable[i]=0) then begin
      x := varccnt[i];
      if (x>bestcc) or (best=0) then begin
        best := i;
        bestcc := x;
      end;
    end;
  end;
  choose_from_all_cc := best;
end;

function Tchoose_solver.choose_from_ccvars() : Tliteral;
var c : Tliteral;
begin
  c := 0;
  if cccxt_order_clean then begin
    while (choose_ccnxt<>0) do begin
      if (varstate[choose_ccnxt]=0)and(varchooseable[choose_ccnxt]=0) then begin
        c := choose_ccnxt;
        break;
      end else choose_ccnxt := varccnxt[choose_ccnxt];
    end;
  end else begin
    c := choose_from_all_cc();
  end;
  if (random<0.5) then c := -c;
  choose_from_ccvars := c;
end;


function Tchoose_solver.choose_berkmin_sign(c : Tliteral) : Tliteral;
var d : Tuliteral; x : int64;
begin
  d := abs(c); 
  x := litccoccurcnt[2*d]-litccoccurcnt[2*d +1];
  if x>0 then c := d
  else if x<0 then c := -d
  else if (random()<0.5) then c := -d 
  else c := d;
  if (random()<ch_probab_inv_berksig) then c := -c;
  choose_berkmin_sign := c;
end;

function Tchoose_solver.choose_sign(c : Tliteral) : Tliteral;
begin
  if (random()<0.5) then begin
    c := -c;
  end;
  if random()<ch_probab_berksig then begin
    c := choose_berkmin_sign(c);
  end;
{$ifdef use_varsigch}
  if (random()<ch_probab_sign_use_equalize_ch) then begin
    c := abs(c);
    if (varsigch[c]>0) then c := -c;
  end; 
{$endif}
  if (varoldstate[abs(c)]<>0)and(random()<ch_probab_oldstate) then begin
    c := -varoldstate[abs(c)];
  end;
  if random()<ch_probab_true then begin
    c := abs(c);
  end;
  if (random()<ch_probab_sign_flip) then begin
    c := -c;
  end;
  choose_sign := c;
end;



function Tchoose_solver.rechoose_sign(c : Tliteral) : Tliteral;
begin
  if (random()<0.5) then begin
    c := -c;
  end;
  if random()<rech_probab_berksig then begin
    c := choose_berkmin_sign(c);
  end;
  if (varoldstate[abs(c)]<>0)and(random()<rech_probab_oldstate) then begin
    c := -varoldstate[abs(c)];
  end;
  if random()<rech_probab_true then begin
    c := abs(c);
  end;
  if (random()<rech_probab_sign_flip) then begin
    c := -c;
  end;
  rechoose_sign := c;
end;


function Tchoose_solver.choose() : Tliteral;
var c : Tliteral; var d : double;
begin
  c := choose_forced();
  if (c=0) then c := choose_by_name();
  if (c=0) then begin
    d := choose_probab_cc;
    if choose_enterhint then d := rechoose_probab_cc;
    if (random()<d) then begin
      c := choose_from_ccvars();
      if choose_enterhint then begin
        if varoldstate[abs(c)]<>0 
        then c := rechoose_sign(c)
        else c := choose_sign(c);
      end else begin
        c := choose_sign(c);
      end; 
    end else begin
      c := choose_from_recent_cls();
      if c=0 then begin
        c := choose_from_ccvars();
        c := choose_sign(c);
      end;
    end;   
  end;
  choose := c;
end;











function Tchoose_solver.handle_setting(const n,v : ansistring) : ansistring;
var q : double; l : Tliteral; w : word;
begin
  if (n='choose_probab_cc') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else choose_probab_cc := q;
  end else if (n='rechoose_probab_cc') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else rechoose_probab_cc := q;
  end else if (n='ch_probab_berksig') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else ch_probab_sign_use_equalize_ch := q;
  end else if (n='rech_probab_berksig') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else rech_probab_berksig := q;
  end else if (n='ch_probab_inv_berksig') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else ch_probab_inv_berksig := q;
  end else if (n='ch_probab_sign_use_equalize_ch') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else ch_probab_sign_use_equalize_ch := q;
  end else if (n='ch_probab_oldstate') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else ch_probab_oldstate := q;
  end else if (n='rech_probab_oldstate') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else rech_probab_oldstate := q;
  end else if (n='ch_probab_true') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else ch_probab_true := q;
  end else if (n='rech_probab_true') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else rech_probab_true := q;
  end else if (n='ch_probab_sign_flip') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else ch_probab_sign_flip := q;
  end else if (n='rech_probab_sign_flip') then begin
    handle_setting := 'ok';
    val(v,q,w);
    if (w>0) then handle_setting := 'vnok'
    else rech_probab_sign_flip := q;
  end else if (n='ch_add_forced_choice') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w>0)or(l=0) then handle_setting := 'vnok'
    else begin
      setlength(force_choose,high(force_choose)+2);
      force_choose[high(force_choose)] := l;
    end;
  end else if (n='ch_recent_mintestcandidates') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w>0)or(l<=0) then handle_setting := 'vnok'
    else begin
      ch_recent_mintestcandidates := l;
    end;
  end else if (n='ch_rcntclssig_sat') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then ch_use_sat_sig := 0
    else if (w=0)and(l=1) then ch_use_sat_sig := 1
    else handle_setting := 'vnok';
  end else if (n='ch_rcntcls_satdiscard') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l=0) then ch_recent_satdiscard := false
    else if (w=0)and(l=1) then ch_recent_satdiscard := true
    else handle_setting := 'vnok';
  end else if (n='choose_by_name_cutdl') then begin
    handle_setting := 'ok';
    val(v,choose_by_name_cutdl,w);
    if (w>0) then handle_setting := 'vnok';
  end else handle_setting := inherited handle_setting(n,v);
end;


function Tchoose_solver.get_setting_descr(const n : ansistring) : ansistring;
begin
  if (n='choose_probab_cc') then begin
    get_setting_descr := 'Currently, there are the following main methods for choosing decision variables:'#10
                        +'a.) choose from a forced-choice list (tried first, totally untested code!)'#10
                        +'b.) choose vars by name (index) from cnf file (tried as specified by choose_by_name_cutdl)'#10
                        +'c.) "VSIDS" choose from all vars the one with max conflict count'#10
                        +'d.) "BerkMin" choose from the set of vars in recent, (optionally not yet satisfied) clauses the one with max conflict count.'#10
                        +'choose_probab_cc (0.0 to 1.0) sets the probability of using method c) instead of method d., if methods a.) and b.) both fail.'
  end else if (n='ch_probab_sign_use_equalize_ch') then begin
{$ifndef use_varsigch}
    get_setting_descr := '[unavailable, requires recompile with "use_varsigch"] For choosing method b.) and c.) (see choose_probab_cc), the sign-selection can try to choose the sign so that it''s the opposite of the most frequent assignment for the var.';
{$else}
    get_setting_descr := 'For choosing method b.) and c.) (see choose_probab_cc), the sign-selection can try to choose the sign so that it''s the opposite of the most frequent assignment for the var.';
{$endif}
  end else if (n='ch_probab_berksig') then begin
    get_setting_descr := 'For choosing method b.) and c.) (see choose_probab_cc), the sign-selection can try to choose the sign so that it matches the the sign most frequently appearing in learnt clauses (BerkMin).';
  end else if (n='ch_probab_inv_berksig') then begin
    get_setting_descr := 'For all choosing methods except a.), set sign inversion probability within BerkMin-sign decision.';
  end else if (n='ch_probab_oldstate') then begin
    get_setting_descr := 'For choosing method b.) and c.) (see choose_probab_cc), the sign-selection can try to choose the sign so that it''s the opposite of the last assignment for the var.';
  end else if (n='ch_probab_true') then begin
    get_setting_descr := 'For choosing method b.) and c.) (see choose_probab_cc), the sign-selection can try to always choose a fixed sign.';
  end else if (n='ch_probab_sign_flip') then begin
    get_setting_descr := 'For choosing method b.) and c.) (see choose_probab_cc), the sign-selection can randomly be flipped.';
  end else if (n='ch_add_forced_choice') then begin
    get_setting_descr := 'Add a decision var to be a forced choice. A signed integer.';
  end else if (n='ch_rcntclssig_sat') then begin
    get_setting_descr := 'The recent clause based choosing method d.) can be configured to either choose in a way that satisfies the recent clause (1) or (0) to match BerkMin sign selection (see also: ch_probab_berksig).';
  end else if (n='ch_rcntcls_satdiscard') then begin
    get_setting_descr := 'The recent clause based choosing method d.) can be configured to take vars from already satisfied recent clause into account (0) or ignore satisfied clauses (1).';
  end else if (n='ch_recent_mintestcandidates') then begin
    get_setting_descr := 'The recent clause based choosing looks for most recently added clauses that aren''t yet already satisfied, and satisfies it by decision. From the literals of that clause, the one with highest cc is chosen. ch_recent_mintestcandidates defines a minimum set of literals to be taken into account (by looking at the next, less recent clause). Must be an integer>0, default:1.';
  end else if (n='choose_by_name_cutdl') then begin
    get_setting_descr := 'Configure name based choosing method b.): An integer specifying the maximum decision depth for using method b.), negative values specify reverse name order. default 0.';
  end else if (n='rech_probab_sign_flip') then begin
    get_setting_descr := 'ch_probab_sign_flip used during restart';
  end else if (n='rech_probab_oldstate') then begin
    get_setting_descr := 'ch_probab_oldstate used during restart';
  end else if (n='rech_probab_true') then begin
    get_setting_descr := 'ch_probab_true used during restart';
  end else if (n='rech_probab_berksig') then begin
    get_setting_descr := 'ch_probab_berksig used during restart';
  end else if (n='rechoose_probab_cc') then begin
    get_setting_descr := 'choose_probab_cc used during restart. Default: 1.0';
  end else get_setting_descr := inherited get_setting_descr(n);
end;






begin
end.


