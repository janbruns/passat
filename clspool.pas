{$mode objfpc}
unit clspool;

{$include config.pas}

{ Some extra-data of learnt clauses (the list of clauses referenced during conflict analysis)
  isn't accessed very frequently, so it gets swaped out to some separate memblock or even to an external file.

  To be more specific, this data is accessed when an unsat cert is built, 
  and when a clause gets deleted (to find out which clauses get a modified refcnt).

  To avoid unnecessary growth of the file, we'll reuse delete clause space 
  in case there are deleteds of exactly the rquested length.
 
}

interface
uses clause,seekstream,sysutils;

type
Tclsextralen = Tuliteral;
Tclspool = class
  { the clause-list returned is valid to next call, only }
  function get_cls_extra(cls : Pclause; var el : Tclsextralen) : PPclause;
  { called only once per clause: }
  procedure set_cls_extra(cls : Pclause; el : Tclsextralen; ed : PPclause);
  procedure discard_clauseextra(cls : Pclause);
  constructor create();
  destructor destroy(); override;
  procedure init_membased();
  function init_tmppath(fn : ansistring) : boolean;
  function getmemlen() : int64;


  procedure setlimit(n : int64);

  private
  function getFreeIdx(el : Tclsextralen) : int64;
  procedure resize_freedones(n : Tclsextralen);

  private
  spool : Tseekablestream;
  dat : array of Pclause;
  freedones : array of int64;
  toppos : int64;

  public
  times_del,times_reuse,bytessaved : qword;
end;

type Texc_clspool = class(Exception);




implementation

type
Tclspoolhead = packed record
  cls : Pclause;
  elemcnt : Tclsextralen;
end;


procedure Tclspool.setlimit(n : int64);
begin
  if spool<>nil then spool.setlimit(n) else raise Texc_clspool.create('Tclspool: setlimit before init.');
end;

function Tclspool.get_cls_extra(cls : Pclause; var el : Tclsextralen) : PPclause;
var h : Tclspoolhead; idx : int64;
begin
  get_cls_extra := nil;
  el := 0;
  if cls=nil then exit;
  idx := cls^.extraidx;
  h.cls := nil;
  h.elemcnt := 0;
  if idx>=0 then begin
    spool.posread(h,sizeof(h),idx);
    if (h.cls<>cls) then raise Texc_clspool.create('Tclspool: mismatched cls.');
    el := h.elemcnt;
    if (el>length(dat)) then setlength(dat,nexthalfpower2(el));
    if (el>0) then begin
      spool.posread(dat[0],el*sizeof(Pclause),sizeof(h)+idx);
      get_cls_extra := @dat[0];
    end;
  end;
end;

procedure Tclspool.set_cls_extra(cls : Pclause; el : Tclsextralen; ed : PPclause);
var h : Tclspoolhead; idx : int64;
begin
  if (el<=0) then exit;
  idx := getFreeIdx(el);
  h.cls := cls;
  h.elemcnt := el;
  spool.poswrite(h,sizeof(h),idx);
  spool.poswrite(ed[0],el*sizeof(Pclause),sizeof(h)+idx);
  cls^.extraidx := idx;
end;

procedure Tclspool.discard_clauseextra(cls : Pclause);
var h : Tclspoolhead; idx : int64;
begin
  idx := cls^.extraidx;
  if idx>=0 then begin
    inc(times_del);
    spool.posread(h,sizeof(h),idx);
    if (h.cls<>cls) then raise Texc_clspool.create('Tclspool: mismatched cls to discard.');
    if (h.elemcnt>high(freedones)) then resize_freedones(h.elemcnt);
    spool.poswrite(freedones[h.elemcnt],sizeof(int64),idx);
    freedones[h.elemcnt] := idx;
    cls^.extraidx := -1;
  end;
end;

function Tclspool.getFreeIdx(el : Tclsextralen) : int64;
var idx : int64;
begin
  idx := -1;
  if (el<=high(freedones)) then begin
    idx := freedones[el];
  end;
  if (idx>=0) then begin
    if idx>=toppos then raise Texc_clspool.create('Tclspool: inconsistent freelist:'+hexstr(idx,16)+'/'+hexstr(toppos,16)+' '+hexstr(el,8));

    {reuse a deleted clause space in case it had a matching extralen}
    spool.posread(freedones[el],sizeof(int64),idx);
    if idx>=toppos then raise Texc_clspool.create('Tclspool: inconsistent freelist.');
    bytessaved += el*sizeof(Pclause)+sizeof(Tclspoolhead);
    inc(times_reuse);
  end else begin
    {don't attempt to split/combine deleted clause space, just append}
    idx := toppos;
    toppos += sizeof(Tclspoolhead) +el*sizeof(Pclause);
  end;
  getFreeIdx := idx;
end;

procedure Tclspool.resize_freedones(n : Tclsextralen);
var i : Tclsextralen;
begin
  n := nexthalfpower2(n);
  i := length(freedones);
  if (i>=n) then exit;
//writeln('c resizing freedones from ',i,' to ',n);
  setlength(freedones,n);
  while (i<=high(freedones)) do begin freedones[i] := -1; inc(i); end;
end;


constructor Tclspool.create();
begin
  spool := nil;
  setlength(dat,1024);
  toppos := 0;
  setlength(freedones,0);
  resize_freedones(1024);
end;

destructor Tclspool.destroy();
begin
  if spool<>nil then begin
    if spool is Tmem_seekablestream then begin
      Tmem_seekablestream(spool).destroy();
    end else if spool is Tunixfile_seekablestream then begin
      Tunixfile_seekablestream(spool).destroy();
    end;
  end;
end;

procedure Tclspool.init_membased();
begin
  spool := Tmem_seekablestream.create();
end;

function Tclspool.init_tmppath(fn : ansistring) : boolean;
begin
  spool := Tunixfile_seekablestream.create();
  init_tmppath := Tunixfile_seekablestream(spool).setPath(fn,10);
end;

function Tclspool.getmemlen() : int64;
begin
  getmemlen := toppos;
end;


begin
end.