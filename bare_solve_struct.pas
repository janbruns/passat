{$mode objfpc}
unit bare_solve_struct;

{$include config.pas}

interface
uses clause,clspool,sysutils,rand;



type
Ptrk_expansion_state = ^Ttrk_expansion_state;
Ttrk_expansion_state = record
  old_conflcls_len,exp_litpos,num_alt,prod_alt : Tliteral;
  cls : Pclause;
end;
Tccnt = dword;
Pccnt = ^Tccnt;

PPrelinkjob=^Prelinkjob;
Prelinkjob=^Trelinkjob;
Trelinkjob = record
  next : Prelinkjob;
  cls : Pclause;
  chain,lit : Tliteral;
end;

Tbare_solve_struct = class
  public
  constructor create();
  destructor destroy(); override;
  procedure randomize();
  procedure set_randseed(q : qword);

  procedure add_setting(n : string);
  function get_setting_descr(const n : ansistring) : ansistring; virtual;
  function handle_setting(const n,v : ansistring) : ansistring; virtual;

  procedure reset_numVars(n : qword);
  procedure printcls(lits : Pliteral; len : Tliteral; withdl : boolean);
  procedure printclause(lits : Pliteral; len : Tliteral; withdl,with0 : boolean; var f : text);

  procedure init_cc();
  procedure randomize_cls(cls : Pclause);


  protected
  procedure apply_ccnxt_order_from_lrncls();
  procedure handle_cc_item_inc(lit : Tliteral);
  procedure retake_cc_item_ref(lit : Tliteral; after : Tliteral);
  procedure handle_sorted_learnt_cc_change(n : Tliteral);

  function dbg_trkvars_to_ccnxt() : Tliteral;
  function dbg_trkvars_to_ccnxt2() : Tliteral;


  private
  function cmp_cc_nxt(a,b : Tliteral) : boolean;
  procedure remove_ccnxt_item(alit : Tliteral);
  procedure insert_ccnext_item_after(alit,ref : Tliteral);



  private
  procedure reallocate(var p : pointer; oldlen,newlen : ptruint);


  public
  {memory consumption as per var}
  varhistory : Pliteral;
  varstate : Pliteral; // 0=unassigned, forced state expressed as +- varindex

  varstateb : Pboolean; // 2*(num_vars+1) bools, EXCEPTIONALLY indexed as [lit+num_varsb] instead of the usual [abs(lit)*2+ord(lit<0)]

  varoldstate : Pliteral; // same as varstate, but not reset to 0 on takeback
  vardecilevel : Pliteral; // number of decisions active at the time the var became forced
  varreason : PPclause;  // the reason clause that forced a var
  varwatches : PPclause; // 4 cls-pointers per var (2 signs, 2 chain-idx)
  varccnt : Pdouble;       // 2 conflict-counts per var
  varslowccnt : Psingle;       // 2 conflict-counts per var
  varchooseable : Pbyte;


  varexpcyc : Pdword;     // during conflict analysis, some expansion-distance from conflicting clause 


  litccoccurcnt : Pqword;  // used for berkmin-style sign choosing

  varccnxt : Pliteral;   // an int-based linked list kept sorted by ccnt using merge
  varccprv : Pliteral;   // an int-based dbl-linked list kept sorted by ccnt using merge
  vartmp : Pliteral;   
  varwcnt : Puliteral; 
  decivar : Pliteral;

{$ifdef dbg_use_decipt}
  vardecipt : Pqword;    // history-length at the time the var was set
{$endif}
  trkvar_visited, varvisit, hle_visit : Pboolean;   // tracker: var already in conflict clause?
  hle_lits : Pliteral;
  trkvar_dlstat : Pliteral;   

  trkvar_conflcls : Pliteral;  // tracker: current conflict clause / learn-candidate clause
  trkvar_expstore : Pliteral;  // tracker: expanded literals

  trkvar_learncls : Pliteral;  // tracker: final clause to be learnt
  trkvar_learnextra : PPclause;// tracker: unsat-cert info for trkvar_learncls

  varsigpref : Pshortint;

{$ifdef count_litassigns}
  varlitassigns : Pqword;      // per literal assignment count, EXCEPTIONALLY indexed as [lit+num_varsb]
{$endif}

{$ifdef use_varsigch}
  varsigch : Plongint;
{$endif}

  varelinkjobs : PPrelinkjob;

  varch : Pdword;
  {end of memory consumption as per var}


  conflict_clause : Pclause;     // initial conflict clause as reported by bcp() when all literals of a clause became false
  conflict_var_reason : Pclause; // initial conflict clause as reported by bcp() when a var already forced becomes forced to both states 
  latestclause : Pclause; // most recently added clause

  conflict_var : Tliteral; // initial conflict var as reported by bcp() when a var already forced becomes forced to both states 

  decilevel : Tliteral; // current number of active "decisions" / temporary assumptions
  historylen : Tliteral;
  num_initial_cls : qword;
  num_vars,num_varsb : qword;
  varmem : qword;
  top_ccnxt,top_ccprv : Tliteral;
  cccxt_order_clean : boolean;

  notquiet,keepsolving : boolean;

  prng : Txorshift128plusstate;

  setting : array of shortstring;
  settings : longint;

  cl_spool : Tclspool;
  starttime : double;
end;



function compare_cls(lits1,lits2 : Pliteral; len1,len2 : Tliteral) : boolean;


type Texc_trk = class(Exception);
type Tppunsat = class(Exception);







implementation
uses math;

constructor Tbare_solve_struct.create();
begin
  inherited create();
  keepsolving := true;
  setlength(setting,0);
  settings := 0;
  latestclause := nil;
  conflict_clause := nil;
  num_vars := 0;
  reset_numVars(0);
  historylen := 0;
  num_initial_cls := 0;
  cccxt_order_clean := false; 
  notquiet := false;
  prng[0] := random_qword() xor $7f32ab51179b3312;
  prng[1] := random_qword() xor $13f7135187424365;
  add_setting('quiet');
end;

destructor Tbare_solve_struct.destroy();
begin
  reset_numVars(0);
  releaseClauses(latestclause);
  //cl_spool.destroy();
  inherited destroy();
end;

procedure Tbare_solve_struct.set_randseed(q : qword);
begin
  prng[0] := not(prng[0]) xor random_qword() xor q;
  prng[1] := prng[1] xor random_qword(prng) xor not(q); 
end;

procedure Tbare_solve_struct.randomize();
var q : double; 
begin
  q := now();
  set_randseed(Pqword(pointer(@q))^);
end;




function Tbare_solve_struct.get_setting_descr(const n : ansistring) : ansistring;
begin
  if n='quiet' then get_setting_descr := 'Wether to print (0) or not (1) that much.'
  else get_setting_descr := 'nok';
end;

function Tbare_solve_struct.handle_setting(const n,v : ansistring) : ansistring;
begin
  if n='quiet' then begin
    handle_setting := 'ok';
    if (v='0') then notquiet := true
    else if (v='1') then notquiet := false
    else handle_setting := 'vnok';
  end else handle_setting := 'nok';
end;

procedure Tbare_solve_struct.add_setting(n : string);
begin
  if (settings>high(setting)) then setlength(setting,settings+4);
  setting[settings] := n;
  settings += 1;
end;



{ fpc's heaptrc often seen to have (had?) probs with reallocated blcks, 
  so use explicit fresh allocs here
}
procedure Tbare_solve_struct.reallocate(var p : pointer; oldlen,newlen : ptruint);
var p2 : pointer; cpylen : ptruint;
begin
  p2 := nil;
  cpylen := oldlen;
  if (newlen<cpylen) then cpylen := newlen;
  if (newlen>0) then begin
    getmem(p2,newlen);
    newlen -= cpylen;
    if (newlen>0) then fillbyte(pbyte(p2)[cpylen],newlen,0);
  end;
  if (cpylen>0)and(p<>nil) then move(p^,p2^,cpylen);
  if (oldlen>0)and(p<>nil) then freemem(p,oldlen);
  p := p2;
  varmem += newlen;
end;


procedure Tbare_solve_struct.reset_numVars(n : qword);
var q : qword;
begin
  if (n<>num_Vars) then begin
    varmem := 0;
    { var 0 will be ignored, so we'll need to alloc an extra-var 
      as soon as at least one var is requested }
    if (n>0) then n += 1;
    if (num_Vars>0) then num_Vars += 1;

    q := sizeof(Tliteral);              reallocate(varhistory,       num_Vars*q, n*q );
    q := sizeof(Tliteral);              reallocate(varstate,         num_Vars*q, n*q );
    q := sizeof(Tliteral);              reallocate(varoldstate,      num_Vars*q, n*q );
    q := sizeof(Tliteral);              reallocate(decivar,          num_Vars*q, n*q );


    q := sizeof(boolean)*2;             reallocate(varstateb,        num_Vars*q, n*q );


    q := sizeof(Tliteral);              reallocate(vardecilevel,     num_Vars*q, n*q );
    q := sizeof(Pclause);               reallocate(varreason,        num_Vars*q, n*q );
    q := sizeof(Pclause)*4;             reallocate(varwatches,       num_Vars*q, n*q );
    q := sizeof(double);                reallocate(varccnt,          num_Vars*q, n*q );
    q := sizeof(single);                reallocate(varslowccnt,      num_Vars*q, n*q );

    q := sizeof(qword)*2;               reallocate(litccoccurcnt,    num_Vars*q, n*q );


    q := sizeof(Tliteral);              reallocate(vartmp,           num_Vars*q, n*q );
    q := sizeof(Tuliteral)*2;           reallocate(varwcnt,          num_Vars*q, n*q );

    q := sizeof(Tliteral);              reallocate(varccnxt,         num_Vars*q, n*q );
    q := sizeof(Tliteral);              reallocate(varccprv,         num_Vars*q, n*q );


    q := sizeof(dword);                 reallocate(varexpcyc,        num_Vars*q, n*q );



{$ifdef dbg_use_decipt}
    q := sizeof(qword);                 reallocate(vardecipt,        num_Vars*q, n*q );
{$endif}
    q := sizeof(boolean)*2;             reallocate(trkvar_visited,   num_Vars*q, n*q );
    q := sizeof(boolean)*2;             reallocate(varvisit,         num_Vars*q, n*q );
    q := sizeof(boolean)*2;             reallocate(hle_visit,        num_Vars*q, n*q );

    q := sizeof(byte);                  reallocate(varchooseable,    num_Vars*q, n*q );

{$ifdef count_litassigns}
    q := sizeof(qword)*2;               reallocate(varlitassigns,    num_Vars*q, n*q );
{$endif}

    q := sizeof(Tliteral);              reallocate(trkvar_dlstat,    num_Vars*q, n*q );

    q := sizeof(Tliteral)*2;            reallocate(trkvar_conflcls,  num_Vars*q, n*q );
    q := sizeof(Tliteral)*2;            reallocate(hle_lits,         num_Vars*q, n*q );

    q := sizeof(Tliteral);              reallocate(trkvar_expstore,  num_Vars*q, n*q );

    q := sizeof(Tliteral)*2;            reallocate(trkvar_learncls,  num_Vars*q, n*q );
    q := sizeof(Pclause);               reallocate(trkvar_learnextra,num_Vars*q, n*q );



    q := sizeof(shortint);              reallocate(varsigpref,       num_Vars*q, n*q );
{$ifdef use_varsigch}
    q := sizeof(longint);               reallocate(varsigch,         num_Vars*q, n*q );
{$endif}


    q := sizeof(Prelinkjob);            reallocate(varelinkjobs,     num_Vars*q, n*q );

    q := sizeof(dword);                 reallocate(varch,            num_Vars*q, n*q );

    if (n>0) then n -= 1;
    num_Vars := n;
    num_varsb := n+1;
  end;
end;


procedure Tbare_solve_struct.randomize_cls(cls : Pclause);
var i,j,tmp : Tliteral; lits : Pliteral;
begin
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin
    j := random_qword(prng) mod Tuliteral(cls^.len);
    tmp := lits[i];
    lits[i] := lits[j];
    lits[j] := tmp;
  end;
end;




procedure Tbare_solve_struct.printclause(lits : Pliteral; len : Tliteral; withdl,with0 : boolean; var f : text);
var i,lit : Tliteral;
begin
  for i := 0 to len-1 do begin
    lit := lits[i];
    write(f,lit);
    if withdl then begin
      write(f,'@');
      if (varstate[abs(lit)]=0) then write(f,'_')
      else if (varstate[abs(lit)]=lit) then write(f,vardecilevel[abs(lit)])
      else write(f,'-',vardecilevel[abs(lit)]);
    end;
    write(f,' ');
  end;
  if with0 then writeln(f,'0') else writeln(f);
end;

procedure Tbare_solve_struct.printcls(lits : Pliteral; len : Tliteral; withdl : boolean);
begin
  printclause(lits,len,withdl,true,output);
end;



function compare_cls(lits1,lits2 : Pliteral; len1,len2 : Tliteral) : boolean;
var i : Tliteral; 
begin
  compare_cls := true;
  if (len1=len2) then begin
    for i := len1-1 downto 0 do begin
      if lits1[i]<>lits2[i] then begin
        compare_cls := false;
        exit;
      end;
    end;
  end else compare_cls := false;
end;





function Tbare_solve_struct.cmp_cc_nxt(a,b : Tliteral) : boolean;
begin
  cmp_cc_nxt := (varccnt[a]>varccnt[b]);
end;



procedure Tbare_solve_struct.remove_ccnxt_item(alit : Tliteral);
var nxt,prv : Tliteral;
begin
  prv := varccprv[alit];
  nxt := varccnxt[alit];
  if (nxt>0) then begin
    varccprv[nxt] := prv;
  end else begin
    top_ccprv := prv;
  end;
  if (prv>0) then begin
    varccnxt[prv] := nxt;
  end else begin
    top_ccnxt := nxt;
  end;
end;


procedure Tbare_solve_struct.insert_ccnext_item_after(alit,ref : Tliteral);
begin
  if (ref>0) then begin
    varccprv[alit] := ref;
    varccnxt[alit] := varccnxt[ref];
    varccnxt[ref] := alit;
    if (varccnxt[alit]=0) 
    then top_ccprv := alit
    else varccprv[varccnxt[alit]] := alit;
  end else begin
    varccprv[alit] := 0;
    varccnxt[alit] := top_ccnxt;
    top_ccnxt := alit;
    if (varccnxt[alit]=0) 
    then top_ccprv := alit
    else varccprv[varccnxt[alit]] := alit;
  end;
end;



procedure Tbare_solve_struct.handle_cc_item_inc(lit : Tliteral);
var prv : Tliteral; litcc : double;
begin
  lit := abs(lit);
  prv := varccprv[lit];
  litcc := varccnt[lit];
  while (prv>0) and (litcc>varccnt[prv]) do begin
    prv := varccprv[prv];
  end;
  if (prv<>varccprv[lit]) then begin
    remove_ccnxt_item(lit);
    insert_ccnext_item_after(lit,prv);
  end;
end;

procedure Tbare_solve_struct.retake_cc_item_ref(lit : Tliteral; after : Tliteral);
var prv : Tliteral; litcc : double;
begin
  lit := abs(lit);
  after := abs(after);
  prv := after;
  litcc := varccnt[lit];
  while (after>0) and (varccnt[after]>litcc) do begin
    prv := after;
    after := varccnxt[after];
  end;
  insert_ccnext_item_after(lit,prv);
end;

procedure Tbare_solve_struct.handle_sorted_learnt_cc_change(n : Tliteral);
var lit,old,i : Tliteral; 
begin
  n -= 1;
  for i := 0 to n do begin
    lit := abs(trkvar_learncls[i]);
    remove_ccnxt_item(lit);
  end;
  lit := abs(trkvar_learncls[0]);
  if top_ccnxt<>0 then begin
    if varccnt[lit]<varccnt[top_ccnxt] then begin
      retake_cc_item_ref(lit,top_ccnxt);
    end else begin
      insert_ccnext_item_after(lit,0);
    end;
  end else insert_ccnext_item_after(lit,0);
  for i := 1 to n do begin
    old := lit;
    lit := abs(trkvar_learncls[i]);
    retake_cc_item_ref(lit,old);
  end;
end;






procedure Tbare_solve_struct.apply_ccnxt_order_from_lrncls();
var i,a,b : Tliteral;
begin
  for i := num_vars-2 downto 0 do begin
    a := abs(trkvar_learncls[i]);
    b := abs(trkvar_learncls[i+1]);
    varccnxt[a] := b;
    varccprv[b] := a;
  end;
  a := abs(trkvar_learncls[0]);
  varccprv[a] := 0;
  top_ccnxt := a;
  a := abs(trkvar_learncls[num_vars-1]);
  varccnxt[a] := 0;
  top_ccprv := a;
end;


function Tbare_solve_struct.dbg_trkvars_to_ccnxt2() : Tliteral;
var i,j,k : Tliteral;
begin
  j := 0;
  k := 0;
  i := top_ccnxt;
  while (i<>0) do begin
    inc(j);
    i := varccnxt[i];
  end;
  i := top_ccprv;
  while (i<>0) do begin
    inc(k);
    i := varccprv[i];
  end;
  if (j<>k) then raise Texc_trk.create('dbg ccnxt prob: '+inttostr(j)+'<>'+inttostr(k));
  dbg_trkvars_to_ccnxt2 := j;
end;



function Tbare_solve_struct.dbg_trkvars_to_ccnxt() : Tliteral;
var i,j,k : Tliteral;
begin
  j := 0;
  k := 0;
  i := top_ccnxt;
  if notquiet then write('c dbg:');
  while (i<>0) and not trkvar_visited[i] do begin
    if notquiet then write(' ',varccnt[i]);//,'@',i);
    inc(j);
    trkvar_visited[i] := true;
    i := varccnxt[i];
  end;
  i := top_ccnxt;
  while (i<>0) and trkvar_visited[i] do begin
    inc(k);
    trkvar_visited[i] := false;
    i := varccnxt[i];
  end;
  if notquiet then writeln();
  if (j<>k) then raise Texc_trk.create('dbg ccnxt prob.');
  dbg_trkvars_to_ccnxt := j;
end;




procedure Tbare_solve_struct.init_cc();
var cls : Pclause; lits : Pliteral; i,lit,alit : Tliteral; tmp : array of double; t : double;
begin
  setlength(tmp,num_vars+1);
  for i := num_vars downto 0 do varccnt[i] := 0.1*random_float(prng);
  for i := num_vars downto 0 do tmp[i] := 0; //0.1*random();
  cls := latestclause;
  while (cls<>nil) do begin
    inc(num_initial_cls);
    lits := getLiterals(cls);
    i := cls^.len;
    if (i>500) then i := 500;
    t := 1;
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      varccnt[alit] += t;
    end;
    cls := cls^.prv;
  end;
  for i := num_vars downto 0 do varslowccnt[i] := varccnt[i];

  cccxt_order_clean := false;
end;


begin
end.

