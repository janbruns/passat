unit sort;
{$INCLUDE config.pas}


interface

type
Tcmpindex = longint;

{ Tplsortcmp : should be true, if a before b needs swap 
}
Tplsortcmp = function(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;

{ Tplsearchcmp : should be <0 if ref must in pl appear before i   
}
Tplsearchcmp = function(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;

Tplswap = procedure(pl : pointer; a,b : Tcmpindex; ud : pointer);



procedure qsort(pl : pointer; a,b : Tcmpindex; cmp : Tplsortcmp; swp : Tplswap; ud : pointer);

function binsearch(pl : pointer; a,b : Tcmpindex; cmp : Tplsearchcmp; ud,ref : pointer; var i : Tcmpindex) : boolean;
function refsearch(pl : pointer; a,b : Tcmpindex; cmp : Tplsearchcmp; ud,ref : pointer; var i : Tcmpindex) : boolean;


// swap/compares for array of longint
procedure swplongint(pl : pointer; a,b : Tcmpindex; ud :pointer);
function sortasccmplongint(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
function searchasccmplongint(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;
function sortdesccmplongint(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
function searchdesccmplongint(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;

// swap/compares for array of pointer
procedure swpptr(pl : pointer; a,b : Tcmpindex; ud :pointer);
function sortasccmpptr(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
function searchasccmpptr(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;
function sortdesccmpptr(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
function searchdesccmpptr(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;


implementation




{ quicksort just like implemented in the fpc sources.
  The while-loops don't seem to get out of bounds 
  because elements that stop them are put forward in
  the loop's runnig direction, so that they see it
  again, right before running out of bounds.
}


procedure qsort(pl : pointer; a,b : longint; cmp : Tplsortcmp; swp : Tplswap; ud : pointer);
var ref,c,d : longint;
begin
  repeat
    c := a;
    d := b;
    ref := (a+b) div 2;
    repeat
      while ( cmp(pl,ref,c,ud) ) do inc(c);
      while ( cmp(pl,d,ref,ud) ) do dec(d);
      if (c<=d) then begin
        swp(pl,c,d,ud);
        if (ref=c) then ref := d 
        else if (ref=d) then ref := c;
        inc(c);
        dec(d);
      end;
    until (c>d);
    if (a<d) then qsort(pl,a,d,cmp,swp,ud);
    a := c;
  until (c>=b);
end;

{ find the (first) index i of ref in pl.
  if ref isn't in pl, return false, and an "insert_before" 
  position i (a<=i<=b+1). 
}

function binsearch(pl : pointer; a,b : Tcmpindex; cmp : Tplsearchcmp; ud,ref : pointer; var i : Tcmpindex) : boolean;
var c,d : Tcmpindex; x : longint;
begin
  c := a;
  d := b;
  while (c<d) do begin
    i := (c+d) div 2;
    x := cmp(pl,i,ref,ud);
    if (x<0) then begin
      d := i-1; 
    end else if (x>0) then begin
      c := i+1;
    end else d := i;
  end;
  i := (c+d) div 2;
  x := cmp(pl,i,ref,ud);
  if (x=0) then begin
    binsearch := true;
  end else begin
    binsearch := false;
    if (x>0) then inc(i);
  end;
end;

function refsearch(pl : pointer; a,b : Tcmpindex; cmp : Tplsearchcmp; ud,ref : pointer; var i : Tcmpindex) : boolean;
var x : longint;
begin
  i := a;
  while (i<=b) do begin
    x := cmp(pl,i,ref,ud);
    if (x=0) then begin
      refsearch := true; exit;
    end else if (x>0) then begin
      inc(i);
    end else begin
      refsearch := false;
      exit;
    end;
  end;
  refsearch := false;
end;





procedure swplongint(pl : pointer; a,b : Tcmpindex; ud :pointer);
var tmp : longint;
begin
  tmp := plongint(pl)[a];
  plongint(pl)[a] := plongint(pl)[b];
  plongint(pl)[b] := tmp;
end;

function sortasccmplongint(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
begin
  sortasccmplongint := (Plongint(pl)[a]>Plongint(pl)[b]);
end;

function searchasccmplongint(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;
var p1,p2 : Plongint;
begin
  p1 := pl;
  p1 := @p1[i];
  p2 := ref;
  if (p1^<p2^) then searchasccmplongint := 1
  else if (p1^=p2^) then searchasccmplongint := 0
  else searchasccmplongint := -1;
end;

function sortdesccmplongint(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
begin
  sortdesccmplongint := (Plongint(pl)[a]<Plongint(pl)[b]);
end;

function searchdesccmplongint(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;
var p1,p2 : Plongint;
begin
  p1 := pl;
  p1 := @p1[i];
  p2 := ref;
  if (p1^<p2^) then searchdesccmplongint := -1
  else if (p1^=p2^) then searchdesccmplongint := 0
  else searchdesccmplongint := 1;
end;











procedure swpptr(pl : pointer; a,b : Tcmpindex; ud :pointer);
var tmp : pointer;
begin
  tmp := Ppointer(pl)[a];
  Ppointer(pl)[a] := Ppointer(pl)[b];
  Ppointer(pl)[b] := tmp;
end;

function sortasccmpptr(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
begin
  sortasccmpptr := (ptruint(Ppointer(pl)[a])>ptruint(Ppointer(pl)[b]));
end;

function searchasccmpptr(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;
var p1,p2 : Ppointer;
begin
  p1 := pl;
  p1 := @p1[i];
  p2 := ref;
  if (ptruint(p1^)<ptruint(p2^)) then searchasccmpptr := 1
  else if (ptruint(p1^)=ptruint(p2^)) then searchasccmpptr := 0
  else searchasccmpptr := -1;
end;

function sortdesccmpptr(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
begin
  sortdesccmpptr := (ptruint(Ppointer(pl)[a])<ptruint(Ppointer(pl)[b]));
end;

function searchdesccmpptr(pl : pointer; i : Tcmpindex; ref,ud : pointer) : longint;
var p1,p2 : Ppointer;
begin
  p1 := pl;
  p1 := @p1[i];
  p2 := ref;
  if (ptruint(p1^)<ptruint(p2^)) then searchdesccmpptr := -1
  else if (ptruint(p1^)=ptruint(p2^)) then searchdesccmpptr := 0
  else searchdesccmpptr := 1;
end;


begin
end.


