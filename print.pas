{$mode objfpc}
unit print;

{$include config.pas}


interface


function int4str(n : int64) : ansistring;
function _int4str(n : int64) : ansistring;
function flt5str(n : double) : ansistring;

implementation

function flt5str(n : double) : ansistring;
var s : string;
begin
  if (n>=1000) 
  then s := int4str(round(n))
  else begin
    if n>=100 then begin
      str(n:5:1,s);
    end else str(n:5:2,s);
  end;
  while length(s)<5 do s := ' '+s;
  flt5str := s;
end;

function int4str(n : int64) : ansistring;
var d,i : longint; c : char; s,s0 : string;
begin
  s0 := 'kMGTPXZY';
  if (abs(n)<=9999) then begin
    str(n:4,s);
  end else begin
    c := s0[1];
    n := n div 100;
    for i := 2 to 8 do begin
      if (abs(n)>=10000) then begin
        n := n div 1000;
        c := s0[i];
      end else break;
    end;
    d := abs(n) mod 10;
    n := n div 10;
    str(n,s);
    str(d,s0);
    if (abs(n)<10) then begin
      s += '.'+s0;
    end else if (abs(n)<100) then begin
      s += ' ';
    end;
    s += c;
  end;
  int4str := s;
end;

function _int4str(n : int64) : ansistring;
begin
  if (n>=-999) then _int4str := ' '+int4str(n) else _int4str := int4str(n);
end;

begin
end.
