{$mode objfpc}
 
uses clause,sysutils,loop_solver,bare_solve_struct, cnf_read,sort,print;

const
READ_ONLY = 0;
WRITE_ONLY = 1;
READ_WRITE = 2;


type
Tsolver = class(Tloop_solver)
  function checkclause(var len : Tliteral; lits : Pint64) : boolean;

  procedure takeclause(const cf : Tcnfload; idx : longint);
  procedure read_cnf();


  constructor create();
  function take_cmdlineparam(p : ansistring; islast : boolean) : boolean;
  procedure print_options();

  function handle_setting(const n,v : ansistring) : ansistring; override;
  function get_setting_descr(const n : ansistring) : ansistring; override;

  procedure do_unsat_cert();
  procedure do_sat_cert(vfmt : Tliteral; var f : text);
  procedure prepare_sat_cert();
  procedure print_sat_cert();

  private
  cnf_clscnt,cnf_clsskip : qword;
  probfn,certfn : ansistring;
  satcertfn : ansistring;
  satcert_fmt,screensatcert_fmt : Tliteral;
  screenprint_vlines : boolean;
  starttime : double;
end;

procedure Tsolver.print_options();
var i : longint;
begin
  writeln('Usage: passat [options] cnf-file');
  writeln('Available options, all used in the form name=val:');
  writeln();
  writeln;
  for i := 0 to settings-1 do begin
    writeln('--',setting[i]);
    writeln();
    writeln(get_setting_descr(setting[i]));
    writeln();
    writeln();
  end;
end;



constructor Tsolver.create();
begin
  inherited create();
  probfn := '';
  certfn := '';
  satcertfn := '';
  add_setting('unsat-cert');
  add_setting('unsat-cert-delinfo');
  add_setting('sat-cert');
  add_setting('sat-certfmt');
  add_setting('sat-vlines');
  satcert_fmt := 0;
  screensatcert_fmt := 10;
  screenprint_vlines := false;
end;




function Tsolver.checkclause(var len : Tliteral; lits : Pint64) : boolean;
var i,j,lit,alit : Tliteral; discard : boolean;
begin
  trk_learn_cls_len := 0;
  trk_learn_cls_extra := 0;
  i := 0;
  discard := false;
  while (i<len) do begin
    lit := lits[i];
    alit := abs(lit);
    if trkvar_visited[alit] then begin
      for j := i-1 downto 0 do begin
        if (lit=-lits[j]) then discard := true;
      end;
    end else begin
      trkvar_visited[alit] := true;
      trkvar_learncls[trk_learn_cls_len] := lit;
      inc(trk_learn_cls_len);
    end;
    inc(i);
  end;
  for i := trk_learn_cls_len-1 downto 0 do begin
    lit := trkvar_learncls[i];
    alit := abs(lit);
    trkvar_visited[alit] := false;
  end;
  len := trk_learn_cls_len;
  checkclause := discard;
end;


procedure Tsolver.takeclause(const cf : Tcnfload; idx : longint);
var clen : Tliteral; cls : Pclause;
begin
  inc(cnf_clsskip);
  clen := idx;
  while not(cf.ia[clen]=0) do begin
    inc(clen);
  end;
  clen := clen-idx;
  if (clen<1) then exit;
  if checkclause(clen,@cf.ia[idx]) then exit;
  trk_sort_learn_clause_by_lits();
  dec(cnf_clsskip);
  inc(cnf_clscnt);
  cls := trk_alloc_learn_clause(false);
  cls^.flags := cls^.flags or clausetag_prob;
  cls^.prv := latestclause;
  latestclause := cls;
end;



procedure Tsolver.read_cnf();
var cf : Tcnfload; i,j,cc : longint; x : qword;
begin
  filemode := READ_ONLY;
  if (copy(probfn,1,7)='file://') then delete(probfn,1,7);
  if probfn='' then begin
    print_options();
    halt(0);
  end;
  starttime := now();
  writeln('c loading file ',probfn);
  cnf_clscnt := 0;
  cnf_clsskip := 0;
  cc := 0;
  cf.force_maxsat := false;
  cf.quiet := not notquiet;
  readcnf(probfn,cf);
  if notquiet then writeln('c time after parse: ',(now()-starttime)*3600*24:0:2);

  reset_numVars(cf.mv);

  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then takeclause(cf,j);
    end;
    inc(cc);
  end;
  if notquiet then writeln('c time after add: ',(now()-starttime)*3600*24:0:2);

  if notquiet then writeln('c added ',num_vars,' vars, ',cnf_clscnt,' clauses, tautologies:',cnf_clsskip);
  init_cc();
  do_pp();
  link_all_cls(true);

  onAfterLoad();

  do_freshknownvars_cleanups(true);
  if notquiet then writeln('c propcnt after init: ',propcnt);

  j := 0; cc := 0; x := 0;
  for i := num_vars downto 1 do begin
    if varstate[i]=0 then begin
      inc(x);
      if (varchooseable[i] =1) then inc(cc); 
    end else begin
      inc(j);
    end;
  end;

  if (cc>0) then writeln('c unforced non-eleminated, single sided:',cc);

end;


function Tsolver.take_cmdlineparam(p : ansistring; islast : boolean) : boolean;
var mm,v : ansistring; i : int64;
begin
  mm := copy(p,1,2);
  if (mm='--') then begin
    delete(p,1,2);
    if (p='h')or(p='help') then begin
      print_options();
      halt(0);
    end;
    i := pos('=',p);
    if (i>0) then begin
      v := copy(p,i+1,length(p));
      p := copy(p,1,i-1);
      take_cmdlineparam := ( 'ok'=handle_setting(p,v) );
    end else begin
      take_cmdlineparam := false;
    end;
  end else begin
    if islast then begin
      probfn := p;
      take_cmdlineparam := true;
    end else take_cmdlineparam := false;
  end;
end;




function Tsolver.handle_setting(const n,v : ansistring) : ansistring;
var l : Tliteral; w : word;
begin
  if (n='unsat-cert') then begin
    certfn := v;
    handle_setting := 'ok';
  end else if (n='unsat-cert-delinfo') then begin
    handle_setting := 'ok';
    if (v='0') then unsatcert_with_del := false
    else if (v='1') then unsatcert_with_del := true
    else handle_setting := 'vnok';
  end else if (n='sat-cert') then begin
    satcertfn := v;
    handle_setting := 'ok';
  end else if (n='sat-certfmt') then begin
    handle_setting := 'ok';
    val(v,l,w);
    if (w=0)and(l>=0) then satcert_fmt := l
    else handle_setting := 'vnok';
  end else if (n='sat-vlines') then begin
    handle_setting := 'ok';
    if (v='0') then screenprint_vlines := false
    else if (v='1') then screenprint_vlines := true
    else handle_setting := 'vnok';
  end else handle_setting := inherited handle_setting(n,v);
end;


function Tsolver.get_setting_descr(const n : ansistring) : ansistring;
begin
  if (n='unsat-cert') then begin
    get_setting_descr := 'Set the filename to use for writing unsat certificate (proof) in drup-format. No cert is generated, if this options is missing, the problem turns out to be satisiable or on aborted search.';
  end else if (n='unsat-cert-delinfo') then begin
    get_setting_descr := 'Wether to include (1) or not (0) clause deletion information into unsat certificates.';
  end else if (n='sat-cert') then begin
    get_setting_descr := 'Set filename to use for writing sat sertificate.';
  end else if (n='sat-certfmt') then begin
    get_setting_descr := 'An integer >=0 specifying the format of sat-cert file (values per vline). The special value of 0 prints one value per line followed by 0 (like in cnf file fomat).';
  end else if (n='sat-vlines') then begin
    get_setting_descr := 'Wether to print(1) or not(0) vlines to console in case of sat.';
  end else get_setting_descr := inherited get_setting_descr(n);
end;

procedure Tsolver.do_unsat_cert();
var f : TEXT;
begin
  if certfn<>'' then begin
    filemode := WRITE_ONLY;
    assign(f,certfn);
    rewrite(f);
    make_unsat_cert(f);
    close(f);
  end;
end;



procedure Tsolver.do_sat_cert(vfmt : Tliteral; var f : text);
var i,n : Tliteral;
begin
  n := 0;
  for i := 1 to num_vars do begin
    if varstate[i]<>0 then begin
      if (vfmt>0)and((n mod vfmt)=0) then write(f,'v ');
      write(f,varstate[i],' ');
      if (vfmt=0) then writeln(f,0);
      inc(n);
      if (vfmt>0)and((n mod vfmt)=0) then writeln(f);
    end;
  end;
  if (vfmt>0)and((n mod vfmt)=0) then write(f,'v ');
  if (vfmt>0) then writeln(f,0);
end;

procedure Tsolver.prepare_sat_cert();
var lit,i,dl,last : Tliteral; cls : Pclause; lits : Pliteral;
begin
  last := 0;
  while ppclsactions.pop(lit,cls) do begin
    if (cls=nil) then begin
      { undo ELS }
      ppclsactions.pop(i,cls);
      if varstate[abs(lit)]<>0 then raise Texc_trk.create('undo-els: target already assigned');
      dl := varstate[abs(i)];
      if i<0 then dl := -dl;
      if dl<0 
      then varstate[abs(lit)] := -abs(lit)
      else varstate[abs(lit)] :=  abs(lit);
      if varstate[abs(lit)]=0 then raise Texc_trk.create('undo-els: incomplete assignment ref');
    end else begin
      if (cls^.flags and clausetag_pp_bce)=0 then begin
        if (varstate[abs(lit)]=0) then begin
          varstate[abs(lit)] := -lit;
          if cls_unsat(cls) then begin
            varstate[abs(lit)] := lit;
            if cls_unsat(cls) then begin
              raise Texc_trk.create('cls unsat after intial true choice.');
            end else begin
              last := lit;
            end;
          end;
        end else if cls_unsat(cls) then begin
          if (abs(lit)<>abs(last)) then begin
            varstate[abs(lit)] := lit;
            last := lit;
          end else begin
            raise Texc_trk.create('lit forced to both signs.');
          end;
        end;
      end;
      lits := getLiterals(cls);
      for i := cls^.len-1 downto 0 do begin
        if varstate[abs(lits[i])]=0 then raise Texc_trk.create('incomplete assignment');
      end;
      if (cls^.flags and clausetag_pp_bce)>0 then begin
        if cls_unsat(cls) then varstate[abs(lit)] := lit;
      end;
    end;
  end;
end;

procedure Tsolver.print_sat_cert();
var f : TEXT;
begin
  prepare_sat_cert();
  if screenprint_vlines then do_sat_cert(screensatcert_fmt,output);
  if satcertfn<>'' then begin
    filemode := WRITE_ONLY;
    assign(f,satcertfn);
    rewrite(f);
    do_sat_cert(satcert_fmt,f);
    close(f);
  end;
end;




var
i : longint;
tim : double;
slvr : Tsolver;
clmem,clcnt,cllit,clextra : qword;
cls : Pclause;
s : string;

begin
  writeln('c PASSAT (c)2020 by Jan Bruns, for help run passat --help.');
  randomize();
  tim := now();

  slvr := Tsolver.create();
  slvr.set_randseed(12312);
  slvr.randomize();
  for i := 1 to paramcount do begin
    if not slvr.take_cmdlineparam(paramstr(i),i=paramcount) then begin
      writeln('c problem with parameter: ',paramstr(i));
      writeln('s UNKNOWN');
      halt(0);
    end;
  end;



  try
    slvr.read_cnf();
    if slvr.notquiet then write('c ____ ');
    repeat
      i := slvr.do_solvecycles();
    until (i<>0);//or((now()-tim)>timeout);
  except 
    on Tppunsat do 
    begin  
      i := -1; 
      if slvr.notquiet then writeln('c pp_unsat!'); 
    end;
  end;
  
  if slvr.notquiet then writeln('c ');

  tim := (now()-tim)*3600*24;

  s := 'UNKNOWN';
  if (i<0) then s := 'UNSATISFIABLE' else if (i>0) then s := 'SATISFIABLE';
  writeln('s ',s);

  if (i<0) then slvr.do_unsat_cert();
  if (i>0) then slvr.print_sat_cert();


  clmem := 0;
  clcnt := 0;
  cllit := 0;
  clextra := 0;
  cls := slvr.latestclause;
  while (cls<>nil) do begin
    inc(clcnt);
    clmem += getMemLen(cls);
    cllit += cls^.len;
    clextra += cls^.extra;
    cls := cls^.prv;
  end;
  if (tim<1e-6) then tim := 1e-6;


  if slvr.notquiet then begin 
    writeln('c confl  =',slvr.trk_learnt_clauses);
    writeln('c clcnt  =',clcnt);
    writeln('c clmem  =',clmem);
    writeln('c cllit  =',cllit);
    writeln('c clextra=',clextra);
    writeln('c varmem =',slvr.varmem);
    writeln('c clcchecks: ',clcchecks   :12,' = ',int4str(round(clcchecks   /tim)),'/s');
    writeln('c clchecks : ',clchecks    :12,' = ',int4str(round(clchecks    /tim)),'/s');
    writeln('c clchksskp: ',clchecksskp :12,' = ',int4str(round(clchecksskp /tim)),'/s');
    writeln('c clsteps  : ',clsteps     :12,' = ',int4str(round(clsteps     /tim)),'/s');
    writeln('c clclinks : ',clclinks    :12,' = ',int4str(round(clclinks    /tim)),'/s');
    writeln('c cllinks  : ',cllinks     :12,' = ',int4str(round(cllinks     /tim)),'/s');
    writeln('c propagate: ',propcnt     :12,' = ',int4str(round(propcnt     /tim)),'/s');
    writeln('c var-confl: ',confl_varbased :12,' = ',int4str(round(confl_varbased /tim)),'/s');
    writeln('c cls-confl: ',confl_clsbased :12,' = ',int4str(round(confl_clsbased /tim)),'/s');
    writeln('c decisions: ',decicnt     :12,' = ',int4str(round(decicnt     /tim)),'/s');
    writeln('c propcyc  : ',propcyc     :12,' = ',int4str(round(propcyc     /tim)),'/s');
    if confl_clsbased>0 then begin
      writeln('c avg confl decilevel  : ',slvr.trk_stat_dl/confl_clsbased:0:2);
      writeln('c avg cclen            : ',slvr.trk_stat_cclen/confl_clsbased:0:2);
      writeln('c avg exp              : ',slvr.trk_stat_expcc/confl_clsbased:0:2);
      writeln('c avg cclen * log n    : ',slvr.trk_stat_cclen_nlogn/confl_clsbased:0:2);
      writeln('c avg both  * log both : ',slvr.trk_stat_ccexp_nlogn/confl_clsbased:0:2);
    end;
  end;
  writeln('c wallclock-time: ',tim:0:2);
  slvr.destroy();
end.

