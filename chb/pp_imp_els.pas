



function Tpp_solver.do_binreplace() : boolean;
var i,lit,alit,todo : Tliteral; cls,cls2 : Pclause; lits,lits2 : Pliteral;
begin
  todo := 0;
  for i := num_vars downto 1 do begin
    binreplace[i] := binaliases[i];
    if (binreplace[i]<>i) then begin
      todo += 1;
      ppclsactions.push(binreplace[i],nil);
      ppclsactions.push(i,nil);
      varchooseable[i] := varchooseable[i] or 4;
    end;
  end;
  if todo>0 then begin
    do_binreplace := true;
    if notquiet then writeln('c replacing ',todo,' literal names by existing equal ones');
    cls := latestclause;
    while (cls<>nil) do begin
      todo := 0;
      lits := getliterals(cls);
      for i := cls^.len-1 downto 0 do begin
        lit := lits[i];
        alit := abs(lit);
        if (binreplace[alit]<>alit) then inc(todo);
      end;
      if (todo>0)
      then begin
        cls2 := createClause(cls^.len,0);
        lits2 := getliterals(cls2);
        for i := cls^.len-1 downto 0 do begin
          lit := lits[i];
          if (lit<0) 
          then lits2[i] := -binreplace[-lit] 
          else lits2[i] :=  binreplace[ lit];
        end;
        cls2^.flags := cls^.flags;
        cls^.flags := cls^.flags or clausetag_pp_del;
        cls2^.prv := latestclause;
        latestclause := cls2;
        ppactions.push(cls2^.len,cls2);
        validate_clause(cls2);
        make_cls_signature(cls2);
      end;
      cls := cls^.prv;
    end;
  end else do_binreplace := false;
end;













procedure Tpp_solver.find_lit_equalities(lit0 : Tliteral);
var alit0,alit1,alit : Tuliteral; i,lit : Tliteral; lits : Pliteral; peq : Ppp_equalities;
begin
  alit0 := abs(lit0);
  alit0 := alit0*2 +ord(lit0<0);
  lits := bin_impl_graph[alit0];

  for i := (lits[1]+2)-1 downto 2 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit+ord(lit<0);
    trkvar_visited[alit] := true;
  end;

  alit1 := alit0 xor 1;
  lits := bin_impl_graph[alit1];

  for i := (lits[1]+2)-1 downto 2 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit+ord(lit<0);
    if trkvar_visited[alit xor 1] then begin
      if (abs(lit)<abs(lit0)) then begin
        new(peq);
        peq^.next := pp_equalities;
        pp_equalities := peq;
        peq^.lit1 := lit;
        peq^.lit2 := lit0;
      end;
    end; 
  end;

  lits := bin_impl_graph[alit0];
  for i := (lits[1]+2)-1 downto 2 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit+ord(lit<0);
    trkvar_visited[alit] := false;
  end;
end;


function Tpp_solver.eq_block_cyc() : boolean;
var l1,l2,m,m1,m2,s : Tliteral; peq : Ppp_equalities; did : boolean;
begin
  did := false;
  peq := pp_equalities;
  while (peq<>nil) do begin
    l1 := peq^.lit1;
    l2 := peq^.lit2;
    m1 := binaliases[abs(l1)];
    m2 := binaliases[abs(l2)];
    s := 1;
    if (l1<0) then s *= -1;
    if (l2<0) then s *= -1;
    if (abs(m1)<abs(m2)) then begin
      m := m1*s;
      binaliases[abs(l2)] := m;
      if (m2<>m) then did := true;
    end else begin
      m := m2*s;
      binaliases[abs(l1)] := m;
      if (m1<>m) then did := true;
    end;
    peq := peq^.next;
  end;
  eq_block_cyc := did;
end;

function Tpp_solver.enum_equalities() : boolean;
var i : Tliteral; 
begin
  if notquiet then write('c eq-test ');
  pp_equalities := nil;
  for i := 1 to num_vars do find_lit_equalities(i);

  if notquiet then write('+');

  while eq_block_cyc() do if notquiet then write('.');
  if notquiet then writeln;
  enum_equalities := do_binreplace();
end;



