{$mode objfpc}
unit occur_solver;

{$include config.pas}


{ Manage lists of clauses where literals occur (for preprocessing).

  The clause pointers are expected to remain allocated during the lifetime of all occurlists.

  The occurlists may be locked for reading contents, 
  and use some lazy deletion strategy(basic idea found in minisat):

  * deal with dropped literals by appending delete-info to the list, 
    as long as memory doesn't need to grow, and ideally apply deletion on lock, only

  * deal with completely dropped clauses by just marking the affected occurlists
    dirty, so that lock operations initiate deletion 

}

interface
uses choose_solver,clause;

type
Poccurlist = ^Toccurlist;
Toccurlist = packed record
  len, mem : int64;
  flags,rm : dword;
end;

Toccur_solver = class(Tchoose_solver)
  protected
  { get occur[lit] data}
  function lazylenOccurList(lit : Tliteral) : int64;
  function lockOccurList(lit : Tliteral; var cl : PPclause) : int64;
  procedure unlockOccurList(lit : Tliteral);

  { add cls to list occur[lit] }
  procedure addToOccurList(listlit : Tliteral; cls : Pclause);
  procedure addToOccurList(cls : Pclause);

  { lazy "add dropped-literal" style deletion of cls from occur[lit] }
  procedure removeFromOccurList(lit : Tliteral; cls : Pclause);

  { lazy "global"-style deletion from all occurlists that cls covers }
  procedure removeFromOccurList(cls : Pclause);

  procedure mark_occurlist_dirty(lit : Tliteral);

  { allocate/release 2*num_vars occurlists }
  procedure initOccurLists(p : Pint64);
  procedure destroyOccurList();

  private
  { clean out the deletions, both, from global cls deletion as well as from local rm list }
  procedure cleanOccurList(occl : Poccurlist);
  procedure growOccurList(var occl : Poccurlist);

  procedure cleanorgrow(var occl : Poccurlist);

  procedure initOccurList(var occl : Poccurlist; n : int64);
  procedure destroyOccurList(var occl : Poccurlist);


  protected
  occurlist : array of Poccurlist;
end;




implementation
uses bare_solve_struct;

const
{$ifdef dbg_use_occurlock}
occurflag_locked : dword = $80000000;
{$endif}
occurflag_dirty  : dword = $7fffffff;



function Toccur_solver.lazylenOccurList(lit : Tliteral) : int64;
var alit : Tuliteral; occl : Poccurlist;
begin
  alit := abs(lit);
  alit := 2*alit +ord(lit<0);
  occl := occurlist[alit];
  lazylenOccurList := occl^.len -(occl^.flags and occurflag_dirty);
end;

function Toccur_solver.lockOccurList(lit : Tliteral; var cl : PPclause) : int64;
var alit : Tuliteral; occl : Poccurlist;
begin
  alit := abs(lit);
  alit := 2*alit +ord(lit<0);
  occl := occurlist[alit];
{$ifdef dbg_use_occurlock}
  if (occl^.flags and occurflag_locked)>0 
  then raise Texc_trk.create('occur: already locked');
  occl^.flags := occl^.flags or occurflag_locked;
{$endif}
  cleanOccurList(occl);
  cl := PPclause(pointer(@occl[1]));
  lockOccurList := occl^.len;
end;

procedure Toccur_solver.unlockOccurList(lit : Tliteral);
{$ifdef dbg_use_occurlock}
var alit : Tuliteral; occl : Poccurlist;
{$endif}
begin
{$ifdef dbg_use_occurlock}
  alit := abs(lit);
  alit := 2*alit +ord(lit<0);
  occl := occurlist[alit];
  if (occl^.flags and occurflag_locked)=0 
  then raise Texc_trk.create('occur: not locked');
  occl^.flags := occl^.flags xor occurflag_locked;
{$endif}
end;


procedure Toccur_solver.removeFromOccurList(lit : Tliteral; cls : Pclause);
var alit : Tuliteral; len,m,rm : int64; occl : Poccurlist; cl : PPclause;
begin
  alit := abs(lit);
  alit := 2*alit +ord(lit<0);
  occl := occurlist[alit];
{$ifdef dbg_use_occurlock}
  if (occl^.flags and occurflag_locked)>0 
  then raise Texc_trk.create('occur: attempt to rm from locked');;
{$endif}
  len := occl^.len;
  m   := occl^.mem;
  rm  := occl^.rm;
  cl := PPclause(pointer(@occl[1]));
  cl[m -rm -1] := cls;
  inc(rm);
  occl^.rm := rm;
  mark_occurlist_dirty(lit);
  if (len+rm)=m then cleanorgrow(occurlist[alit]);
end;

procedure Toccur_solver.addToOccurList(listlit : Tliteral; cls : Pclause);
var alit : Tuliteral; len,m,rm : int64; occl : Poccurlist; cl : PPclause;
begin
  alit := abs(listlit);
  alit := 2*alit +ord(listlit<0);
  occl := occurlist[alit];
{$ifdef dbg_use_occurlock}
  if (occl^.flags and occurflag_locked)>0 
  then raise Texc_trk.create('occur: attempt to add to locked');
{$endif}
  len := occl^.len;
  m   := occl^.mem;
  rm  := occl^.rm;
  cl := PPclause(pointer(@occl[1]));
  cl[len] := cls;
  inc(len);
  occl^.len := len;
  if (len+rm)=m then cleanorgrow(occurlist[alit]);
end;

procedure Toccur_solver.cleanorgrow(var occl : Poccurlist);
begin
  cleanOccurList(occl);
  if (occl^.len+occl^.rm)=occl^.mem then growOccurList(occl);
end;

procedure Toccur_solver.cleanOccurList(occl : Poccurlist);
var len,m,rm,i,j : int64; cl : PPclause;
begin
  if (occl^.flags and occurflag_dirty)=0 then exit;
  len := occl^.len;
  m   := occl^.mem;
  rm  := occl^.rm;
  cl := PPclause(pointer(@occl[1]));
  { mark the removed clauses for remove operation }
  cl := @cl[m -rm];
  for i := rm-1 downto 0 do begin
    cl[i]^.flags := cl[i]^.flags or clausetag_pp_occur;
  end;
  { remove removed operation }
  cl := PPclause(pointer(@occl[1]));
  i := 0; j := 0;
  while (i<len) do begin
    if (cl[i]^.flags and clausetag_pp_occur_del)=0 then begin
      cl[j] := cl[i];
      inc(j);
    end;
    inc(i);
  end;
  occl^.len := j;
  { unmark the removed clauses for remove operation }
  cl := @cl[m -rm];
  for i := rm-1 downto 0 do begin
    cl[i]^.flags := cl[i]^.flags xor clausetag_pp_occur;
  end;
  occl^.rm := 0;
{$ifdef dbg_use_occurlock}
  occl^.flags := occl^.flags and occurflag_locked;
{$else}
  occl^.flags :=  0;
{$endif}
end;


procedure Toccur_solver.mark_occurlist_dirty(lit : Tliteral);
var alit : Tuliteral; occl : Poccurlist; x : dword;
begin
  alit := abs(lit);
  alit := 2*alit +ord(lit<0);
  occl := occurlist[alit];
  x := occl^.flags and occurflag_dirty;
  if x<occurflag_dirty then inc(x);
  occl^.flags := x {$ifdef dbg_use_occurlock} or (occl^.flags and occurflag_locked) {$endif} ;
end;


function calc_nxt_occur_size(m : int64) : int64;
var n : int64;
begin
  m := (m+1)*sizeof(Pclause) + sizeof(Toccurlist);
  n := 32;
  while (n<m) do n*=2;
  n -= sizeof(Toccurlist);
  calc_nxt_occur_size := n div sizeof(Pclause);
end;

procedure Toccur_solver.growOccurList(var occl : Poccurlist);
var len,m,rm,m2 : int64; occl2 : Poccurlist; cl,cl2 : PPclause;
begin
  len := occl^.len;
  m   := occl^.mem;
  rm  := occl^.rm;
  m2 := calc_nxt_occur_size(m);
  getmem(occl2,m2*sizeof(Pclause)+sizeof(Toccurlist));
  move(occl^,occl2^,len*sizeof(Pclause)+sizeof(Toccurlist));
  if rm>0 then begin
    cl := PPclause(pointer(@occl[1]));
    cl := @cl[m -rm];
    cl2 := PPclause(pointer(@occl2[1]));
    cl2 := @cl2[m2 -rm];
    move(cl^,cl2^,rm*sizeof(Pclause));
  end;
  occl2^.mem := m2;
  freemem(occl,m*sizeof(Pclause)+sizeof(Toccurlist));
  occl := occl2;
end;

procedure Toccur_solver.initOccurList(var occl : Poccurlist; n : int64);
begin
  getmem(occl,n*sizeof(Pclause)+sizeof(Toccurlist));
  occl^.mem := n;
  occl^.len := 0;
  occl^.rm := 0;
  occl^.flags := 0;
end;

procedure Toccur_solver.destroyOccurList(var occl : Poccurlist);
var m : int64; 
begin
  m := occl^.mem;
  freemem(occl,m*sizeof(Pclause)+sizeof(Toccurlist));
  occl :=  nil;
end;

procedure Toccur_solver.initOccurLists(p : Pint64);
var k,m2 : int64; 
begin
  m2 := calc_nxt_occur_size(1);
  k := num_vars;
  k := 2*(k+1);
  setlength(occurlist,k);
  if (p=nil) then begin
    for k := high(occurlist) downto 0 do begin
      initOccurList(occurlist[k],m2);
    end;
  end else begin
    for k := high(occurlist) downto 0 do begin
      m2 := calc_nxt_occur_size(p[k]);
      initOccurList(occurlist[k],m2);
    end;
  end;
end;

procedure Toccur_solver.destroyOccurList();
var k : int64;
begin
  for k := high(occurlist) downto 0 do destroyOccurList(occurlist[k]);
  setlength(occurlist,0);
end;



procedure Toccur_solver.addToOccurList(cls : Pclause);
var lits : Pliteral; i : Tliteral;
begin
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do addToOccurList(lits[i],cls);
end;

procedure Toccur_solver.removeFromOccurList(cls : Pclause);
var lits : Pliteral; i : Tliteral;
begin
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do mark_occurlist_dirty(lits[i]);
end;


begin
end.

