{$mode objfpc}
unit trk_solver;

{$include config.pas}

{ About conflict reason analysis.


}

interface
uses bcp_solver,clause;


type

Ttrk_solver = class(Tbcp_solver)
  public
  constructor create();
  destructor destroy(); override;

  procedure analyse_conflict();

  procedure trk_analyse_cls_conflict();
  procedure trk_analyse_cls_conflict_dr();

  function trk_put_learn_candidate() : boolean; virtual; abstract;
  procedure trk_onBeforeTrk(); virtual; abstract;
  procedure trk_onAfterTrk(); virtual; abstract;

  procedure trk_fallback();

  protected
  procedure trk_doConflictCounts(); virtual; abstract;


  private

  function trk_expand(l : Tliteral; var allexp : boolean) : Tliteral;
  function trk_can_expand(l : Tliteral) : boolean;
  function trk_lit_redundant(l : Tliteral) : boolean;


  protected
  trk_learn_cls_len : Tliteral;
  trk_cur_conflcls_len : Tliteral; // current length of conflict clause
  trk_expanded_lits : Tliteral;

  trk_used_fallback : boolean;
  maxmeg : Tliteral;
  trk_litredchk_limit : Tliteral;
end;




implementation
uses bare_solve_struct;





constructor Ttrk_solver.create();
begin
  inherited create();
  clear_conflict_indicators();
  trk_litredchk_limit := 10;
  maxmeg := -1;
end;

destructor Ttrk_solver.destroy();
begin
  inherited destroy();
end;

{ redundant literal removal...
  
  remove a literal from confl cls, if this doesn't require additional literals to appear in cc
}
function Ttrk_solver.trk_expand(l : Tliteral; var allexp : boolean) : Tliteral;
var g,lit,alit,i : Tliteral; cls : Pclause; lits : Pliteral;
begin
//write('expand ',l,' @',vardecilevel[abs(l)],',',vardecipt[abs(l)],': ');
  allexp := false;
  g := -1;
  l := abs(l);
  cls := varreason[l];
  if (cls<>nil) then begin
    allexp := true;
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if (vardecilevel[alit]>0) and not trkvar_visited[alit] then begin
        trkvar_visited[alit] := true;
        trkvar_conflcls[trk_cur_conflcls_len] := lit;
        inc(trk_cur_conflcls_len);
        inc(g);
        allexp := allexp and trk_can_expand(lit);
//write(lit,' ');
      end else begin
//write('[\',lit,'@',vardecilevel[alit],',',vardecipt[alit],'] ');

      end;
    end;
  end else g := 1;
  trk_expand := g;
//writeln(allexp);
end;

function Ttrk_solver.trk_can_expand(l : Tliteral) : boolean;
begin
  if (varreason[abs(l)]<>nil) then begin
    trk_can_expand := true;
  end else trk_can_expand := false;
end;

function Ttrk_solver.trk_lit_redundant(l : Tliteral) : boolean;
var start_cclen,i,lit,alit,effgrow,meg : Tliteral; x : boolean;
begin
  alit := abs(l);
  trk_lit_redundant := false;
  if (vardecilevel[alit]=decilevel) then exit; // curdl will fail anyway
  start_cclen := trk_cur_conflcls_len;

  effgrow := trk_expand(l,x);
  meg := effgrow;
  i := start_cclen;
  while x and (i<trk_cur_conflcls_len) do begin
    effgrow += trk_expand(trkvar_conflcls[i],x);
    if (effgrow>trk_litredchk_limit) then x := false;
    inc(i);
    if (effgrow>meg) then meg := effgrow;
  end;


  if x and (effgrow<0) and (meg>maxmeg) then begin
    maxmeg := meg;
  end;

  { cleanup }
  if x and (effgrow<0) then begin
    { All literals added to conflcls disappeared by expanding their reasons.
      There'd be just a cleanup here, but for unsat-cert let's include the reason clauses. }
    while (trk_cur_conflcls_len>start_cclen) do begin
      dec(trk_cur_conflcls_len);
      lit := trkvar_conflcls[trk_cur_conflcls_len];
      trkvar_expstore[trk_expanded_lits] := lit;
      inc(trk_expanded_lits);
    end;
    trk_lit_redundant := true;
  end else begin
    { clean up }
    while (trk_cur_conflcls_len>start_cclen) do begin
      dec(trk_cur_conflcls_len);
      lit := trkvar_conflcls[trk_cur_conflcls_len];
      alit := abs(lit);
      trkvar_visited[alit] := false;
    end;
    trk_lit_redundant := false;
  end;
end;







{ For experimenting:
  Expand all reasons to decisions roots, or until there's only 1 var per decilevel left. 
}
procedure Ttrk_solver.trk_analyse_cls_conflict_dr();
var cls : Pclause; i,idx,lit,alit,last,dl,mindl : Tliteral; lits : Pliteral;
begin

  trk_cur_conflcls_len := 0;
  trk_expanded_lits := 0;

  cls := conflict_clause;
  idx := historylen-1;
  lit := 0;
  last := 0;

  for i := decilevel downto 0 do trkvar_dlstat[i] := 0;
  mindl := decilevel+1;

  repeat

    if (cls<>nil) then begin
      lits := getLiterals(cls);
      for i := cls^.len-1 downto 0 do begin
        lit := lits[i];
        if (lit<>last) then begin
          alit := abs(lit);
          dl := vardecilevel[alit];
          if not(trkvar_visited[alit]) and (dl>0) then begin
            trkvar_visited[alit] := true;
            inc(trkvar_dlstat[dl]);
            if dl<mindl then mindl := dl;
          end;
        end;
      end;
    end; // else raise Texc_trk.create('Trk: missing expansion clause');

    repeat
      if (idx<0) then raise Texc_trk.create('Trk: out of trail');
      lit := varhistory[idx];
      alit := abs(lit);
      dec(idx);
    until trkvar_visited[alit];
    dl := vardecilevel[alit];
    cls := varreason[alit];
    dec(trkvar_dlstat[dl]);
    if (trkvar_dlstat[dl]>0) then begin 
      trkvar_expstore[trk_expanded_lits] := lit;
      inc(trk_expanded_lits);
    end else begin
      trkvar_conflcls[trk_cur_conflcls_len] := -lit;
      inc(trk_cur_conflcls_len);
      cls := nil;
      if dl=mindl then break;
    end;
    last := lit;
  until false;

{
  // check the conclict clause
  last := 0;
  while last < trk_cur_conflcls_len do begin
    lit := trkvar_conflcls[last];
    alit := abs(lit);
    if not(trkvar_visited[alit]) then raise Texc_trk.create('found unseen var in ccls?');
    if (varstate[alit]=lit) then raise Texc_trk.create('found true lit in confcls?');
    if (varstate[alit]=0) then raise Texc_trk.create('found unassigned var in confcls?');
    inc(last);
  end;
}


  trk_used_fallback := false;
  trk_put_learn_candidate();

  for i := trk_cur_conflcls_len-1 downto 0 do begin
    lit := trkvar_conflcls[i];
    trkvar_visited[abs(lit)] := false;
  end;
  for i := trk_expanded_lits-1 downto 0 do begin
    lit := trkvar_expstore[i];
    trkvar_visited[abs(lit)] := false;
  end;

{
  for i := num_vars downto 1 do begin
    if trkvar_visited[i] then raise Texc_trk.create('Trk: visit-flag unclean');;
  end;
}
end;







procedure Ttrk_solver.trk_analyse_cls_conflict();
var cls : Pclause; i,idx,lit,alit,last,dl,curdlvars : Tliteral; lits : Pliteral;
begin
  trk_cur_conflcls_len := 0;
  trk_expanded_lits := 0;

  cls := conflict_clause;
  curdlvars := 0;
  idx := historylen-1;
  lit := 0;
  last := 0;

{ Generate a conflict clause that contains only one current-decilevel 
  var (this is always possible given there is always only one decision
  var per decilevel). This is called UIP (unique implication point).

  Relative to cycle-based propagation with multi-reasoning,
  this implementation (found in minisat, for example) of conflict clause generation 
  is extremely optimised (with little effect on overall performance, however).

  Instead of successively expanding reasons of the already added
  var with highest propagation depth (in order to minimize expansions, to not 
  miss an UIP), we scan the trail/history for "expansion-requests" generated 
  on expansions. The temporary conflict clause doesn't include current decilevel
  vars (because they generate the expansion requests instead). 
   
}
  repeat

    if (cls<>nil) then begin
      lits := getLiterals(cls);
      for i := cls^.len-1 downto 0 do begin
        lit := lits[i];

        if (lit<>last) then begin
          alit := abs(lit);
          dl := vardecilevel[alit];
{$ifdef dbg_use_decipt}
          if vardecipt[alit]>(idx+1) then raise Texc_trk.create('Trk: late literal');
{$endif}
          if not(trkvar_visited[alit]) and (dl>0) then begin
            trkvar_visited[alit] := true;
            if (dl=decilevel) 
            then begin
              inc(curdlvars);
            end else begin
              trkvar_conflcls[trk_cur_conflcls_len] := lit;
              inc(trk_cur_conflcls_len);
            end;
          end;
        end;
      end;
    end else raise Texc_trk.create('Trk: missing expansion clause');

    repeat
      if (idx<0) then raise Texc_trk.create('Trk: out of trail');
      lit := varhistory[idx];
      alit := abs(lit);
      dec(idx);
    until trkvar_visited[alit];


    cls := varreason[alit];
    if (vardecilevel[alit]=decilevel) 
    then dec(curdlvars)
    else raise Texc_trk.create('Trk: read a non-curdl-var from trail');
    if (curdlvars>0) then begin 
      trkvar_expstore[trk_expanded_lits] := lit;
      inc(trk_expanded_lits);
    end else break;
    last := lit;
  until false;
  trkvar_conflcls[trk_cur_conflcls_len] := -lit;
  inc(trk_cur_conflcls_len);
  trkvar_visited[abs(lit)] := true;


{
  // check the conclict clause
  last := 0;
  while last < trk_cur_conflcls_len do begin
    lit := trkvar_conflcls[last];
    alit := abs(lit);
    if not(trkvar_visited[alit]) then raise Texc_trk.create('found unseen var in ccls?');
    if (varstate[alit]=lit) then raise Texc_trk.create('found true lit in confcls?');
    if (varstate[alit]=0) then raise Texc_trk.create('found unassigned var in confcls?');
    inc(last);
  end;
}


  last := 0;
  while last < trk_cur_conflcls_len do begin
    lit := trkvar_conflcls[last];
    if trk_lit_redundant(lit) then begin
      trkvar_conflcls[last] := trkvar_conflcls[trk_cur_conflcls_len-1];
      dec(trk_cur_conflcls_len);
      trkvar_expstore[trk_expanded_lits] := lit;
      inc(trk_expanded_lits);
      last -= 1;
    end;
    last += 1;
  end;


{
  // check the conclict clause
  last := 0;
  while last < trk_cur_conflcls_len do begin
    lit := trkvar_conflcls[last];
    alit := abs(lit);
    if not(trkvar_visited[alit]) then raise Texc_trk.create('found unseen var in ccls?');
    if (varstate[alit]=lit) then raise Texc_trk.create('found true lit in confcls?');
    if (varstate[alit]=0) then raise Texc_trk.create('found unassigned var in confcls?');
    inc(last);
  end;

}
  trk_used_fallback := false;

  trk_put_learn_candidate();
  trk_doConflictCounts();
  trk_put_learn_candidate();

  for i := trk_cur_conflcls_len-1 downto 0 do begin
    lit := trkvar_conflcls[i];
    trkvar_visited[abs(lit)] := false;
  end;
  for i := trk_expanded_lits-1 downto 0 do begin
    lit := trkvar_expstore[i];
    trkvar_visited[abs(lit)] := false;
  end;

{ // check if cleaned up everything
  for i := num_vars downto 1 do begin
    if trkvar_visited[i] then raise Texc_trk.create('Trk: visit-flag unclean');;
  end;
}
end;




procedure Ttrk_solver.trk_fallback();
var dl : Tliteral;
begin
  for dl := decilevel downto 1 do begin
    trkvar_conflcls[dl-1] := -decivar[dl];
  end;
  trk_cur_conflcls_len := decilevel;
  trk_expanded_lits := 0;

  trk_used_fallback := true;
  trk_put_learn_candidate();
end;




procedure Ttrk_solver.analyse_conflict();
begin
  inc(confl_clsbased);

  trk_onBeforeTrk();
  

  trk_analyse_cls_conflict();


  trk_onAfterTrk();
end;


begin
end.
