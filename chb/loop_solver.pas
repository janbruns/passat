{$mode objfpc}
unit loop_solver;

{$include config.pas}


interface
uses pp_solver,clause;

type
Tloop_solver = class(Tpp_solver)
  public
  constructor create();
  procedure choose_to_unsat();
  function do_solvecycles() : longint;
  procedure onAfterLoad(); override;
  procedure onStartSolveCycling(); override;

  procedure decide_clause_deletion(cls : Pclause; d : qword);
  procedure decide_learnt_clause_deletion();
  procedure delete_some_clauses();



  protected
  procedure update_restart_intervall();

  function search(n : int64) : longint;

  function varwlen(lit : Tliteral) : Tuliteral;


  public
  restart_intervall : qword;
  loop_multitb : qword;
  loop_tb : qword;

  cls_del_probab : double;
  target_del_amount : double;
  delprobab_adj,delprobab_adj_adj,
  loop_randtakebackprobab : double;
  luby_u,luby_v : int64;
  freshvar_handlingcounter, freshvar_handlingintervall : longint;
end;




implementation
uses print;


constructor Tloop_solver.create();
begin
  inherited create();
  restart_intervall := 10;

  cls_del_probab := 0.15;
  target_del_amount := 0.90;
  delprobab_adj := 1.05;
  delprobab_adj_adj := 0.99;
  loop_multitb := 0;
  loop_tb := 0;
  loop_randtakebackprobab := 1.5;
  luby_u := 1; luby_v := 1;
  freshvar_handlingcounter := 0;
  freshvar_handlingintervall := 10;
end;

procedure Tloop_solver.onAfterLoad();
begin
  inherited onAfterLoad();
  check_for_satisfied_clauses();
  unlink_deleted_clauses();
  do_freshknownvars_cleanups(false);
  bcp();
end;




procedure lubynxt(var u,v : int64);
begin
  if (u and (-u))=v then begin
    u := u+1;
    v := 1;
  end else begin
    v := 2*v;
  end;
end;








procedure Tloop_solver.decide_learnt_clause_deletion();
var cls : Pclause; flg : Tuliteral; d : qword;
begin
  d := 0;
  cls := latestclause;
  while (cls<>nil)and((cls^.flags and clausetag_learnt)>0) do begin
    flg := cls^.flags;
    if (flg and clausetag_hasrefs)=0 then begin
      decide_clause_deletion(cls,d); 
    end;
    flg := cls^.flags;
    if (flg and clausetag_deleted)=0 then begin
      mark_cls_extra_refs(cls,clausetag_hasrefs);
    end;
    inc(d);
    cls := cls^.prv;
  end;
end;


procedure Tloop_solver.decide_clause_deletion(cls : Pclause; d : qword);
var dodel : boolean; n : Tliteral;
begin
  if (d<1.01*restart_intervall) then begin
    dodel := random()<(d/(1.01*restart_intervall));
  end else;
  dodel := true;
  n := cls^.len-3;
  if (n<1) then n := 1;
  dodel := dodel and (random()*avg_cls_len/n < (1/cls_del_probab));
  if dodel then begin
    cls^.flags := cls^.flags or clausetag_deleted or clausetag_linkless;
  end;
end;




procedure Tloop_solver.delete_some_clauses();
begin
  inc(freshvar_handlingcounter);
  if (freshvar_handlingcounter>=freshvar_handlingintervall) then begin
    freshvar_handlingcounter := 0;
    do_freshknownvars_cleanups(true);
  end;
  calc_avg_clauselen(clausetag_learnt);
  clear_clause_flag(clausetag_learnt, clausetag_hasrefs );

  mark_varreasons_as_clause_ref();
  decide_learnt_clause_deletion();
  unlink_deleted_clauses();
  release_deleted_clauses();
  old_clscnt_deleted := clscnt_deleted;
end;

procedure Tloop_solver.choose_to_unsat();
var c : Tliteral;
begin
  c := 0;
  while not inConflict() do begin
    c := choose();
    if (c<>0) then apply_decision(c) else break;
  end;
  choose_enterhint := false;
end;

procedure Tloop_solver.update_restart_intervall();
begin
  if (restart_intervall<1000) then restart_intervall += 20
  else if (restart_intervall<10000) then restart_intervall += 150
  else restart_intervall += 250;
end;

procedure Tloop_solver.onStartSolveCycling();
begin
  choose_enterhint := true;
  inherited onStartSolveCycling();
end;








function Tloop_solver.search(n : int64) : longint;
var mtb : int64;
begin
  onStartSolveCycling();
  mtb := 1;
  while (n>0) do begin
    trk_multitb := 0;
    choose_to_unsat();
    if inConflict() then begin
      if not resolve_conflicts() then begin
        search := -1;
        exit;
      end else begin
      end;
    end else begin
      search := 1;
      exit;
    end;
    loop_multitb += trk_multitb;
    loop_tb += 1;
    if (trk_multitb>mtb) then begin
      n += 33*(trk_multitb-mtb);
      mtb := trk_multitb;
    end;
    n := n-1;
  end;
  search := 0;
  takeback_to_decilevel(0);
end;


function chainlen(cls : Pclause; c : longint) : qword;
var q : qword;
begin
  q := 0;
  while (cls<>nil) do begin
    inc(q);
    cls := cls^.nxt[c];
  end;
  chainlen := q;
end;

function Tloop_solver.varwlen(lit : Tliteral) : Tuliteral;
var idx : qword;
begin
  idx := abs(lit);
  idx := 4*idx+ord(lit<0);
  varwlen := chainlen(varwatches[idx],0)+chainlen(varwatches[idx+2],1);
end;

function Tloop_solver.do_solvecycles() : longint;
var n : qword; q : double; i,kv : longint;
begin
  do_solvecycles := 0;

  for n := 1 to 20 do begin
    lubynxt(luby_u,luby_v);
    i := search(luby_v*20);
    if (i<>0) then begin
      do_solvecycles := i;
      exit;
    end;
  end;
  if notquiet then writeln;

  q := (clscnt_deleted)/(1+trk_learnt_clauses);
  if trk_learnt_clauses>(100+sqrt(num_initial_cls)) then begin
    for n := 1 to 1 do delete_some_clauses();
    if (q<target_del_amount) then cls_del_probab /= delprobab_adj;
    if (q>target_del_amount) then cls_del_probab *= delprobab_adj;
    delprobab_adj := delprobab_adj_adj*delprobab_adj + (1-delprobab_adj_adj)*1.0;
  end;
  update_restart_intervall();
  kv := 0;
  for i := num_vars downto 1 do begin
    if varstate[i]<>0 then inc(kv);
  end;
  if notquiet then write('c ',int4str(trk_learnt_clauses),' bt, ',int4str(clscnt_deleted),' : ',int4str(trk_learnt_clauses-clscnt_deleted),' (',round(100*q):2,'% rm) cllen=',flt5str(avg_cls_len),' ',kv,' knvr, ',flt5str(varccnt[top_ccnxt]/trk_cc_inc),' | ');


  do_solvecycles := 0;
end;

begin
end.