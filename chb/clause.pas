{$mode objfpc}
unit clause;

{$include config.pas}


interface

const
max_int64 : int64 = 9223372036854775807;

type
Tliteral = longint;
Tuliteral = dword;
Pliteral = ^Tliteral;
Puliteral = ^Tuliteral;

{ clauses have some temp-storage to ease up parallelization:
}
Tclsstate = packed record
  sat,oldsat,prp,oldprp : Tliteral;
end;

PPclause = ^Pclause;
Pclause = ^Tclause;
Tclsextra = Pclause;
Pclsextra = ^Tclsextra;
Tclause = packed record
  nxt : array[0..1] of Pclause; // var's 2-watch linked list
  rnxt : Pclause;               // var's reason linked list
  prv : Pclause;                // used in global all clauses list (starting at latestclause)
{$IFDEF cls_use_blocker}
  blocker : Tliteral;
{$ENDIF}
  len,    // number of literals in clause
  flags,  // partially used for propagate-count 
  rm,     // number of deleted literals in clause (also used for extra-alignment)
  extra : Tuliteral; // number of extra-elems (used for unsat-cert build info)
  { literal data }
  { extra data }
end;


{ some container for the "next bcp cycle assignments"-list
}
Tpropagatelist = class
  public
  constructor create();
  destructor destroy(); override;
  procedure push(lit : Tliteral; reason : Pclause);
  function pop(var lit : Tliteral; var reason : Pclause) : boolean;
  
  procedure concat(p : Tpropagatelist);
  procedure clear();

  function memusage() : qword;

  public
  lits : array of Tliteral;
  reasons : array of Pclause;
  len,mem : qword;
end;






function createClause(len,extra : Tuliteral) : Pclause;
procedure releaseClause(p : Pclause);

function getLiterals(p : Pclause) : Pliteral;

{ varstate[0..max_var] : 0=unassigned, signed var_num = literal otherwise
  varlinks : 2 link-slots * 2 literal sign values = 4 entries (chains, linked lists) per var. 
}
//function check_clause_chain( firstcls : Pclause; varstate : Pliteral; chainidx : longint ) : Pclause;

//procedure relink_clause_chain_with_propagate( firstcls : Pclause; varlinks : PPclause; chainidx : longint; pj : Tpropagatelist);

//procedure link_clause( cls : Pclause; varlinks : PPclause; chainidx, lit : Tliteral);

procedure remove_marked_clauses_from_chains(varlinks : PPclause; lit : Tliteral; tag : Tuliteral);

{ treat extra-data of a clause as a list of clause-pointers
  and mark all of such clauses. 
}
procedure mark_cls_extra_refs(cls : Pclause; tag : Tuliteral);


function getMemLen(p : Pclause) : qword;
function getExtra(p : Pclause) : Pclsextra;


function getPropCount(p : Pclause) : Tuliteral;
procedure incPropCount(p : Pclause); // saturated
procedure setPropCount(p : Pclause; n : Tuliteral); // saturated

procedure releaseClauses(p : Pclause);



const 
speciallit_unsat = -2147483648;
propcntmask  : Tuliteral = $007FFFFF;
npropcntmask : Tuliteral = $FF800000;

clausetag_prob      : Tuliteral = $80000000;
clausetag_learnt    : Tuliteral = $40000000;
clausetag_hasrefs   : Tuliteral = $20000000;
clausetag_deleted   : Tuliteral = $10000000;
clausetag_incert    : Tuliteral = $08000000;
clausetag_linkless  : Tuliteral = $04000000;
clausetag_visited   : Tuliteral = $02000000;
clausetag_pp_del    : Tuliteral = $04000000;
clausetag_pp_discard: Tuliteral = $04000000;
clausetag_pp_occur  : Tuliteral = $01000000;
clausetag_pp_occur_del: Tuliteral = $05000000;
clausetag_pp_bce   : Tuliteral  = $00800000;

var
clsteps : qword;
clchecks  : qword;
clcchecks : qword;
cllinks : qword;
clclinks : qword;
clchecksskp : qword;
propcnt,propcyc : qword;
confl_varbased, confl_clsbased, decicnt : qword;

implementation

const
sizeof_Tclause = sizeof(Tclause) div sizeof(Tliteral); // warn: remainder = 0 is assumed!
sizeof_Pclause = sizeof(Pclause) div sizeof(Tliteral); // warn: uint power of 2 assumed!



procedure remove_marked_clauses_from_chains(varlinks : PPclause; lit : Tliteral; tag : Tuliteral);
var idx : qword; chainidx,litsig : longint; cls,cls2 : Pclause;
begin
  lit := abs(lit);
  for litsig := 0 to 1 do for chainidx := 0 to 1 do begin
    idx := 4*lit +2*chainidx +litsig;
    while (true) do begin
      cls := varlinks[idx];
      if (cls=nil) then break;
      if (cls^.flags and tag)>0 
      then varlinks[idx] := cls^.nxt[chainidx]
      else break;
    end;
    cls := varlinks[idx];
    while (cls<>nil) do begin
      cls2 := cls^.nxt[chainidx];
      if (cls2=nil) then break;
      if (cls2^.flags and tag)>0 
      then cls^.nxt[chainidx] := cls2^.nxt[chainidx]
      else cls := cls2;
    end;
  end;
end;

{ treat extra-data of a clause as a list of clause-pointers
  and mark all of such clauses. 
}
procedure mark_cls_extra_refs(cls : Pclause; tag : Tuliteral);
var c : PPclause; cls2 : Pclause; len,i : Tliteral; 
begin
  len := cls^.extra;
  c := getExtra(cls);
  for i := len-1 downto 0 do begin
    cls2 := c[i];
    if (cls2<>nil) then begin
      cls2^.flags := cls2^.flags or tag;
    end;
  end;
end;



function getLiterals(p : Pclause) : Pliteral; 
begin
  getLiterals := Pliteral(pointer(@pbyte(pointer(p))[sizeof(Tclause)]));
end;

function getExtra(p : Pclause) : Pclsextra;
begin
  getExtra := pointer(@pbyte(pointer(p))[sizeof(Tclause)+(p^.len+p^.rm)*sizeof(Tliteral)]);
end;

function calcMemLen(len,extra : Tuliteral) : qword;
begin
  calcMemLen := sizeof(Tclause) +qword(len)*sizeof(Tliteral) +qword(extra)*sizeof(Tclsextra);
end;

function getMemLen(p : Pclause) : qword;
begin
  getMemLen := calcMemLen(p^.len+p^.rm,p^.extra);
end;

function createClause(len,extra : Tuliteral) : Pclause;
var p : Pclause; m : qword; len2 : Tuliteral;
begin
  len2 := (sizeof_Tclause+len) and (sizeof_Pclause-1);
  if (len2>0) then len2 := sizeof_Pclause -len2;
  m := calcMemLen(len+len2,extra);
  p := getmem(m);
  fillbyte(p^,m,0);
  p^.len := len;
  p^.rm := len2;
  p^.extra := extra;
  createClause := p;
end;

procedure releaseClause(p : Pclause);
begin
  freemem(p,getMemLen(p));
end;

procedure releaseClauses(p : Pclause);
var p2 : Pclause;
begin
  while (p<>nil) do begin
    p2 := p^.prv;
    releaseClause(p);
    p := p2;
  end;
end;



procedure incPropCount(p : Pclause);
var a,b : Tuliteral; 
begin
  a := p^.flags;
  b := (a and propcntmask)+1;
  if (b>propcntmask) then b := propcntmask;
  p^.flags := b or (a and npropcntmask);
end;

procedure setPropCount(p : Pclause; n : Tuliteral);
var a : Tuliteral; 
begin
  a := p^.flags;
  if (n>propcntmask) then n := propcntmask;
  p^.flags := n or (a and npropcntmask);
end;

function getPropCount(p : Pclause) : Tuliteral;
begin
  getPropCount := p^.flags and propcntmask;
end;







constructor Tpropagatelist.create();
begin
  mem := 64;
  len := 0;
  setlength(lits,mem);
  setlength(reasons,mem);
end;

destructor Tpropagatelist.destroy();
begin
  setlength(lits,0);
  setlength(reasons,0);
  mem := 0;
  len := 0;
end;

procedure Tpropagatelist.push(lit : Tliteral; reason : Pclause);
begin
  if len>=mem then begin
    mem *= 2;
    setlength(lits,mem);
    setlength(reasons,mem);
  end;
  lits[len] := lit;
  reasons[len] := reason;
  len += 1;
end;

function Tpropagatelist.pop(var lit : Tliteral; var reason : Pclause) : boolean;
begin
  if (len>0) then begin
    len -= 1;
    lit := lits[len];
    reason := reasons[len];
    pop := true;
  end else pop := false;
end;

procedure Tpropagatelist.concat(p : Tpropagatelist);
var n : qword;
begin
  if (p.len<=0) then exit;
  n := len+p.len;
  if n>=mem then begin
    mem := 2*n;
    setlength(lits,mem);
    setlength(reasons,mem);
  end;
  move(p.lits[0],lits[len],p.len*sizeof(lits[0]));
  move(p.reasons[0],reasons[len],p.len*sizeof(reasons[0]));
  len := n;
end;

procedure Tpropagatelist.clear();
begin
  len := 0;
end;

function Tpropagatelist.memusage() : qword;
begin
  memusage := InstanceSize +mem*sizeof(lits[0]) +mem*sizeof(reasons[0]);
end;

begin
  clsteps := 0;
  clchecks := 0;
  clcchecks := 0;
  cllinks := 0;
  clclinks := 0;
  clchecksskp := 0;
  propcnt := 0;
  propcyc := 0;
  confl_varbased := 0;
  confl_clsbased := 0;
  decicnt := 0;
end.


