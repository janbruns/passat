

type
Tliteral = longint;
Tclsidx = longint;
Pliteral = ^Tliteral;
Tclause = packed record
  len,rm : Tliteral;
end;

var
clauses : array of Pclause;
clscnt : Tclsidx;


function createClause(len : Tliteral) : Pclause;
var cls : Pclause;
begin
  getmem(cls,(len+2)*sizeof(Tliteral));
  cls^.len := len;
  cls^.rm := 0;
  createClause := cls;
end;

procedure releaseClause(cls : Pclause);
begin
  freemem(cls,(2+cls^.len)*sizeof(Tliteral));
end;

function getLiterals(cls : Pclause) : Pliteral;
begin
  getLiterals := @pbyte(cls)[sizeof(Tclause)];
end;

procedure takeclause(const cf : Tcnfload; idx : longint);
var clen,i : Tliteral; cls : Pclause; lits : Pliteral;
begin
  clen := idx;
  while not(cf.ia[clen]=0) do begin
    inc(clen);
  end;
  clen := clen-idx;
  if (clen<1) then exit;
  cls := createClause(clen);
  clauses[clscnt] := cls;
  inc(clscnt);
  lits := getLiterals(cls);
  for i := clen-1 downto 0 do begin
    lits[i] := cf.ia[idx+i];
  end;
end;

procedure read_cnf(probfn : ansistring);
var cf : Tcnfload; i,j : int64; 
begin
  clscnt := 0;
  filemode := READ_ONLY;
  if (copy(probfn,1,7)='file://') then delete(probfn,1,7);
  if probfn='' then begin
    print_options();
    halt(0);
  end;
  writeln('c loading file ',probfn);
  cf.force_maxsat := false;
  cf.quiet := false;
  readcnf(probfn,cf);
  setlength(clauses,high(cf.cidx)+1);
  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then takeclause(cf,j);
    end;
  end;
  writeln('c loaded ',clscnt,' clauses');
end;





begin
  read_cnf(paramstr(1));
end.