{$mode objfpc}
unit bcp_solver;

{$include config.pas}


interface
uses bare_solve_struct,clause;

type
Tcls_rlnk_cmp_lits = function(a,b : Tliteral) : boolean of object;


Tbcp_solver = class(Tbare_solve_struct)
  public
  constructor create();
  destructor destroy(); override;

  procedure do_var_CHB(a : Tliteral);
  procedure do_trail_CHB(a : Tliteral);

  function bcp() : boolean;

  function inConflict() : boolean;
  procedure clear_conflict_indicators();
  procedure takeback_to_decilevel(dl : Tliteral);
  procedure apply_decision(lit : Tliteral);

  procedure onAfterLoad(); virtual;
  function cls_sat(cls : Pclause) : boolean;
  function cls_unsat(cls : Pclause) : boolean;

  procedure relink_cls_to(cls : Pclause; chain,litpos : Tliteral);
  procedure search_link_targets(cls : Pclause);
  function cls_propagate(cls : Pclause) : boolean;

  private procedure consistency_check();
  procedure fast_bcp();
  procedure do_fastbcpcycle();
  procedure add_fastprop(lit : Tliteral; reason : Pclause);

  function is_cls_in_chain(cls : Pclause; chain, lit : Tliteral) : boolean;




  public
  bcp_subcycle_num : qword;
  
  last_problem_clause : Pclause;

  protected
  new_known_vars : Tliteral;
  fastprop_head : Tliteral;
  propagations : Tpropagatelist;
  chb_lastuipvar, chb_lastdecivar : Tliteral;
  chb_blend,chb_minblend,chb_blend_adj,chb_factor1,chb_factor0 : double;

end;





implementation
uses sysutils,rand;


constructor Tbcp_solver.create();
begin
  inherited create();
  propagations := Tpropagatelist.create();
  chb_blend := 0.4;
  chb_minblend := 0.06;
  chb_blend_adj := -1.0e-6;
end;

destructor Tbcp_solver.destroy();
begin
  propagations.destroy();
  inherited destroy();
end;

procedure Tbcp_solver.onAfterLoad();
begin
  last_problem_clause := latestclause;
  clear_conflict_indicators();
  bcp();
end;



function Tbcp_solver.inConflict() : boolean;
begin
  inConflict := (conflict_clause<>nil);
end;

procedure Tbcp_solver.clear_conflict_indicators();
begin
  conflict_var := 0;
  conflict_clause := nil;
end;

procedure Tbcp_solver.takeback_to_decilevel(dl : Tliteral);
var lit,alit : Tliteral;
begin
  while (historylen>0) do begin
    dec(historylen);
    lit := varhistory[historylen];
    alit := abs(lit);
    if vardecilevel[alit]>dl then begin
      varstate[alit] := 0;
      varstateb[lit+num_varsb] := false;
    end else begin
      inc(historylen);
      break;
    end;
  end;
  decilevel := dl;
end;

procedure Tbcp_solver.apply_decision(lit : Tliteral);
begin
  chb_lastdecivar := lit;
  inc(decicnt);
  inc(decilevel);
  decivar[decilevel] := lit;
  bcp_subcycle_num := 0;
  propagations.push(lit,nil);
  bcp();
end;







procedure Tbcp_solver.fast_bcp();
var lit,alit : Tliteral; reason : Pclause;
begin
  fastprop_head := historylen;
  while propagations.pop(lit, reason) do begin
    alit := abs(lit);
    if (varstate[alit]=0) then begin
      if (decilevel=0) then new_known_vars += 1;
      add_fastprop(lit,reason);
    end else if (varstate[alit]<>lit) then begin
      conflict_var := lit;
      conflict_var_reason := reason;
      exit;
    end;
  end;
  do_fastbcpcycle();
end;


{$GOTO ON}

procedure Tbcp_solver.do_fastbcpcycle();
var chain,i,j,lit,lit2,other,vs,sat,prpcnt : Tliteral; vw : PPclause;
  cls,cls2,clsnxt : Pclause; lits : Pliteral; idx,idx2 : qword; varstb : Pboolean;
begin
inc(propcyc);
  varstb := varstateb;
  vw := varwatches;
  while (fastprop_head<historylen) do begin
inc(propcnt);
    lit := varhistory[fastprop_head];
    idx := abs(lit);
    idx := 4*idx +ord(-lit<0);
    for chain := 0 to 1 do begin
      cls := vw[idx];
      cls2 := nil;
      while (cls<>nil) do begin
        lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
inc(clchecks);
        clsnxt := cls^.nxt[chain];
//        prefetch(clsnxt^);
        other := lits[1-chain];
{$IFDEF cls_use_blocker}

        if (varstb[cls^.blocker+num_varsb] or varstb[other+num_varsb]) then begin

{$ELSE}
        if (varstb[other+num_varsb]) then begin

{$ENDIF}
          cls^.nxt[chain] := cls2;
          cls2 := cls;
        end else begin
          //other := ord(sat=0);
          j := cls^.len;
          sat := 0;
          prpcnt := 0;
          i := 2;
          while (i<j) do begin
            lit2 := lits[i];

            vs := varstate[abs(lit2)];
            if (vs=0) then begin
              vartmp[prpcnt] := i;
              inc(prpcnt);
              if prpcnt>1 then break;
            end else if (vs=lit2) then begin
              sat := i;
              break;
            end;

            inc(i);
          end;
          if sat=0 then begin
            other := ord((varstb[-other+num_varsb]));
            other := 1-other + prpcnt;
            if (other>1) then begin
              sat := random_qword(prng) mod Tuliteral(prpcnt);
{$IFDEF cls_use_blocker}
              if (sat+1)<prpcnt then cls^.blocker := lits[vartmp[sat+1]]
              else if (sat>0) then cls^.blocker := lits[vartmp[sat-1]];
{$ENDIF}
              sat := vartmp[sat];
              lit2 := lits[sat];
//relink_cls_to(cls,chain,sat);

              lits[sat] := -lit;
              lits[chain] := lit2; 
              idx2 := abs(lit2);
              idx2 := idx2*4 +2*chain +ord(lit2<0);
              cls^.nxt[chain] := vw[idx2];
              vw[idx2] := cls;

            end else if (other=1) then begin
              vartmp[prpcnt] := 1-chain;
              add_fastprop(lits[vartmp[0]],cls);
              cls^.nxt[chain] := cls2;
              cls2 := cls;
            end else begin
              conflict_clause := cls;
              while (cls<>nil) do begin
                clsnxt := cls^.nxt[chain];
                cls^.nxt[chain] := cls2;
                cls2 := cls;
                cls := clsnxt;
              end;
              vw[idx] := cls2;
              exit;
            end;
          end else begin // cls is satisfied
            lit2 := lits[sat];
{$IFDEF cls_use_blocker}
            cls^.blocker := lit2;
            cls^.nxt[chain] := cls2;
            cls2 := cls;
{$ELSE}
//relink_cls_to(cls,chain,sat);

            lits[sat] := -lit;
            lits[chain] := lit2; 
            idx2 := abs(lit2);
            idx2 := idx2*4 +2*chain +ord(lit2<0);
            cls^.nxt[chain] := vw[idx2];
            vw[idx2] := cls;
{$ENDIF}

          end;
        end;
        cls := clsnxt;
      end;
      vw[idx] := cls2;
      idx += 2;      
    end;
    fastprop_head += 1;
  end;
end;


procedure Tbcp_solver.add_fastprop(lit : Tliteral; reason : Pclause);
var avs : Tliteral; 
begin
//  if (reason<>nil) then reason^.rnxt := nil;
  avs := abs(lit);
//  if varstate[avs]<>0 then raise Texc_trk.create('illegal var-state (re)set:'+inttostr(lit)+' old='+inttostr(varstate[avs]));
//  if lit=0 then raise Texc_trk.create('illegal var-state 0 (re)set:'+inttostr(lit)+' old='+inttostr(varstate[avs]));

  varstate[avs] := lit;
  varstateb[lit+num_varsb] := true;



  varoldstate[avs] := lit;
  varreason[avs] := reason;
  vardecilevel[avs] := decilevel;
{$ifdef dbg_use_decipt}
  vardecipt[avs] := historylen;
{$endif}

  varhistory[historylen] := lit;
//  if (historylen>=num_vars) then raise Texc_trk.create('history-overflow:'+inttostr(historylen)+' old='+inttostr(num_vars));
  inc(historylen);
{$ifdef use_varsigch}
  if (lit<0) then dec(varsigch[avs]) else inc(varsigch[avs]); 
{$endif}
end;






procedure Tbcp_solver.relink_cls_to(cls : Pclause; chain,litpos : Tliteral);
var idx : qword; lit : Tliteral; lits : Pliteral;
begin
  if (litpos=1-chain) then raise Texc_trk.create('relink_cls_to(litpos=1-chain)');
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  lit := lits[litpos];
  lits[litpos] := lits[chain];
  lits[chain] := lit;
  idx := abs(lit);
  idx := idx*4 +2*chain +ord(lit<0);
  cls^.nxt[chain] := varwatches[idx];
  varwatches[idx] := cls;
end;

procedure Tbcp_solver.search_link_targets(cls : Pclause);
var alit,i,lit : Tliteral; lits : Pliteral;
begin
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  lit := lits[0];
  alit := abs(lit);
  if varstate[alit]<>0 then begin
    for i := cls^.len-1 downto 2 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=0 then begin
        lits[i] := lits[0];
        lits[0] := lit;
        break;
      end;
    end;
  end;
  if cls^.len<3 then exit;
  lit := lits[1];
  alit := abs(lit);
  if varstate[alit]<>0 then begin
    for i := cls^.len-1 downto 2 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=0 then begin
        lits[i] := lits[1];
        lits[1] := lit;
        break;
      end;
    end;
  end;
end;


procedure Tbcp_solver.do_var_CHB(a : Tliteral);
begin
  varchb[a] := varchb[a]*chb_factor1 + chb_factor0/(1+trk_learnt_clauses-varlastconfl[a]);
end;

procedure Tbcp_solver.do_trail_CHB(a : Tliteral);
var i : Tliteral;
begin
  if (conflict_clause=nil) then begin
    chb_factor0 := 0.9 
  end else begin
    chb_factor0 := 1.0;
  end;
  chb_factor1 := 1-chb_blend;
  chb_factor0 *= chb_blend;
  for i := historylen-1 downto a do do_var_CHB(abs(varhistory[i]));
  if not(chb_lastuipvar=0) then begin
    do_var_CHB(abs(chb_lastuipvar));
    chb_lastuipvar := 0;
  end;
  if not(chb_lastdecivar=0) then begin
    do_var_CHB(abs(chb_lastdecivar));
    chb_lastdecivar := 0;
  end;
end;


function Tbcp_solver.bcp() : boolean;
var traillen : Tliteral;
begin
  traillen := historylen;
  if not inConflict() then fast_bcp() else propagations.clear();
  do_trail_CHB(traillen);
  bcp := (conflict_clause=nil);
end;



function Tbcp_solver.is_cls_in_chain(cls : Pclause; chain, lit : Tliteral) : boolean;
var idx : qword; c : Pclause;
begin
  idx := abs(lit);
  idx := 4*idx +2*chain +ord(lit<0);
  c := varwatches[idx];
  while (c<>nil) do begin
    if (c=cls) then begin
      is_cls_in_chain := true;
      exit;
    end;
    c := c^.nxt[chain];
  end;
  is_cls_in_chain := false;
end;






procedure Tbcp_solver.consistency_check();
var sat,unassig,falselit,i,lit,alit,x : Tliteral; lits : Pliteral; cls,cls2 : Pclause; q1,q2 : boolean;
begin
  x := 0;
  cls := latestclause;
  while (cls<>nil) do begin
    inc(x);
    sat := 0;
    unassig := 0;
    falselit := 0;
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=lit then inc(sat)
      else if varstate[alit]=0 then inc(unassig)
      else inc(falselit);
    end;

    if (sat=0) then begin
      if (unassig=1) then begin
        q1 := is_cls_in_chain(cls,0,lits[0]);
        q2 := is_cls_in_chain(cls,1,lits[1]);
        writeln();
        writeln('current decilevel=',decilevel);
        //writeln('cls-links: ',cls^.links[0],':',q1,' | ',cls^.links[1],':',q2);
        cls2 := cls^.nxt[0]; if (cls2=nil) then writeln('Link0-next: nil') else writeln('Link0-next: ',getLiterals(cls2)[0]);
        cls2 := cls^.nxt[1]; if (cls2=nil) then writeln('Link1-next: nil') else writeln('Link1-next: ',getLiterals(cls2)[1]);

        printcls(lits,cls^.len,true);
writeln('islatest?',cls=latestclause,x,q1);
        if not(q1) then raise Texc_trk.create('consistency_check: clause link0 not in chain');
        if not q1 then writeln('not q1');
        if q1 then writeln('doch q1');
        if not(q1) then writeln('not q1');

writeln;
repeat
  historylen -= 1;
  lit := varhistory[historylen];
  alit := abs(lit);
  if vardecilevel[alit]<decilevel then break;
  write(lit,' ');
until false;

readln;
        raise Texc_trk.create('consistency_check: clause didn''t propagate correctly');
      end;
      if (cls^.len>1) then begin
        q1 := is_cls_in_chain(cls,0,lits[0]);
        q2 := is_cls_in_chain(cls,1,lits[1]);
        if not(q1 and q2) then begin
          writeln();
          writeln('current decilevel=',decilevel);
          writeln('linkprob...');
          writeln('clsflgs=',hexstr(cls^.flags,16));
          printcls(lits,cls^.len,true);
          writeln('islatest?',cls=latestclause,x,q1);
          writeln('cls-links: ',lits[0],':',q1,' | ',lits[1],':',q2);
          cls2 := cls^.nxt[0]; if (cls2=nil) then writeln('Link0-next: nil') else writeln('Link0-next: ',getLiterals(cls2)[0]);
          cls2 := cls^.nxt[1]; if (cls2=nil) then writeln('Link1-next: nil') else writeln('Link1-next: ',getLiterals(cls2)[1]);
          writeln('cls appears in the following chains...');
          for i := 1 to num_vars do begin
            if is_cls_in_chain(cls,0,i) then write(i,'_0 ');
            if is_cls_in_chain(cls,1,i) then write(i,'_1 ');
            if is_cls_in_chain(cls,0,-i) then write(-i,'_0 ');
            if is_cls_in_chain(cls,1,-i) then write(-i,'_1 ');
          end;
          writeln;
          readln;
          raise Texc_trk.create('consistency_check: clause link0 not in chain');
        end;
        if (lits[0]=lits[1]) then Texc_trk.create('consistency_check: clause links a lit twice');
        if (varstate[abs(lits[0])]<>0) 
        or (varstate[abs(lits[1])]<>0) then begin
          writeln('Prob wth cls: ',hexstr(ptruint(cls),16),' curdl=',decilevel); 
          writeln('prob cls: ');
          printcls(lits,cls^.len,false);
          printcls(lits,cls^.len,true);
          writeln;
          raise Texc_trk.create('consistency_check: clause links assigned lit2');
        end;
      end;
    end;

    cls := cls^.prv;
  end;
write('+');
end;


function Tbcp_solver.cls_sat(cls : Pclause) : boolean;
var i,lit,alit : Tliteral; lits : Pliteral; 
begin
  if (cls<>nil) then begin
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=lit then begin
        cls_sat := true;
        exit;
      end;
    end;
  end;
  cls_sat := false;
end;


function Tbcp_solver.cls_unsat(cls : Pclause) : boolean;
var i,lit,alit : Tliteral; lits : Pliteral; 
begin
  if (cls<>nil) then begin
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]<>-lit then begin
        cls_unsat := false;
        exit;
      end;
    end;
  end;
  cls_unsat := true;
end;

function Tbcp_solver.cls_propagate(cls : Pclause) : boolean;
var i,lit,alit,sat,prp,prplit : Tliteral; lits : Pliteral; 
begin
  sat := 0; prp := 0; prplit := 0;
  if (cls<>nil) then begin
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=lit then inc(sat)
      else if varstate[alit]=0 then begin
        inc(prp);
        prplit := lit;
      end;
    end;
  end;
  if (sat=0)and(prp=1) then begin
    propagations.push(prplit,cls);
    cls_propagate := true;
  end else cls_propagate := false;
end;








begin
end.

