


procedure Tpp_solver.remove_clause_from_appearlist(cls : Pclause);
var lits : Pliteral;
begin
  cls^.flags := cls^.flags or clausetag_pp_discard;
  removeFromOccurList(cls);
  if (cls^.len=2) then begin
    lits := getLiterals(cls);
    unregister_new_binary_clause(lits[0],lits[1]);
  end;
end;


procedure Tpp_solver.insert_clause_into_appearlist(cls : Pclause);
var lits : Pliteral;
begin
  addToOccurList(cls);
  if (cls^.len=2) then begin
    lits := getLiterals(cls);
    register_new_binary_clause(lits[0],lits[1]);
  end;
end;



procedure Tpp_solver.handle_appearlist_effects_of_literal_drop(cls : Pclause; lit : Tliteral);
var lits : Pliteral; 
begin
  removeFromOccurList(lit,cls);
  if cls^.len=2 then begin
    lits := getliterals(cls);
    register_new_binary_clause(lits[0],lits[1]);
  end else if cls^.len=1 then begin
    lits := getliterals(cls);
    unregister_new_binary_clause(lits[0],lits[1]);
  end;
end;











procedure Tpp_solver.make_big(p : Pint64);
var alit : Tuliteral; m,n : int64; 
begin
  alit := num_vars;
  alit := 2*alit+2;
  setlength(bin_impl_graph,alit);
  if (p=nil) then begin
    n := 2 + 62;
    m := 2;
    while (m<n) do m *= 2;
    for alit := alit-1 downto 0 do begin
      getmem(bin_impl_graph[alit], m*sizeof(Tliteral));
      bin_impl_graph[alit][0] := m;
      bin_impl_graph[alit][1] := 0;
    end;
  end else begin
    for alit := alit-1 downto 0 do begin
      n := 2+p[alit];
      m := 2;
      while (m<n) do m *= 2;
      getmem(bin_impl_graph[alit], m*sizeof(Tliteral));
      bin_impl_graph[alit][0] := m;
      bin_impl_graph[alit][1] := 0;
    end;
  end;
end;

procedure Tpp_solver._update_big(lit1,lit2 : Tliteral);
var alit,m,n,n2,len : Tuliteral; lits,p : Pliteral;
begin
  if (lit2=0) then raise Texc_trk.create('upd_big: lit2=0');
  alit := abs(lit1);
  alit := 2*alit +ord(lit1<0);
  if (alit>high(bin_impl_graph))or(alit=0) then raise Texc_trk.create('big bounds check fail');
  lits := bin_impl_graph[alit];
  m   := lits[0];
  len := lits[1];
  if (len+3)>m then begin
    n := round(m*1.5)+62;
    n2 := 2;
    while (n2<n) do n2 *= 2;
    n := n2;
    getmem(p,n*sizeof(Tliteral));
    move(lits[2],p[2],len*sizeof(Tliteral));
    freemem(lits,m*sizeof(Tliteral));
    bin_impl_graph[alit] := p;
    lits := p;
    lits[1] := len;
    lits[0] := n;
  end;
  lits[2+len] := lit2;
  lits[1] := len+1;
end;

procedure Tpp_solver.update_big(lit1,lit2 : Tliteral);
begin
  _update_big(lit1,lit2);
  _update_big(lit2,lit1);
end;

procedure Tpp_solver._unupdate_big(lit1,lit2 : Tliteral);
var alit,n,len : Tuliteral; lits : Pliteral;
begin
  if (lit2=0) then raise Texc_trk.create('upd_big: lit2=0');
  alit := abs(lit1);
  alit := 2*alit +ord(lit1<0);
  if (alit>high(bin_impl_graph))or(alit=0) then raise Texc_trk.create('big bounds check fail');
  lits := bin_impl_graph[alit];
  len := lits[1];
  for n := len-1 downto 0 do begin
    if lits[2+n]=lit2 then begin
      lits[2+n] := lits[2+len-1];
      lits[1] -= 1;
      exit;
    end;
  end;
  raise Texc_trk.create('cl2 to delete not found in big');
end;

procedure Tpp_solver.unupdate_big(lit1,lit2 : Tliteral);
begin
  _unupdate_big(lit1,lit2);
  _unupdate_big(lit2,lit1);
end;

procedure Tpp_solver.destroy_big();
var alit : Tuliteral; m : Tliteral; 
begin
  alit := num_vars;
  alit := 2*alit+2;
  for alit := alit-1 downto 0 do begin
    m := bin_impl_graph[alit][0];
    freemem(bin_impl_graph[alit], m*sizeof(Tliteral));
  end;
  setlength(bin_impl_graph,0);
end;


procedure Tpp_solver.register_new_binary_clause(lit1,lit2 : Tliteral);
var alit : Tuliteral;
begin
  update_big(lit1,lit2);
  alit := abs(lit1);
  alit := 2*alit+ord(lit1<0);
  have_fresh_bincls_about[alit] := true;
  alit := abs(lit2);
  alit := 2*alit+ord(lit2<0);
  have_fresh_bincls_about[alit] := true;
  have_fresh_binary_cls := true;
end;


procedure Tpp_solver.unregister_new_binary_clause(lit1,lit2 : Tliteral);
begin
  unupdate_big(lit1,lit2);
end;





