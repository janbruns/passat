{$mode objfpc}
UNIT cnf_read;


INTERFACE

TYPE
Tia = array of int64;
Tcnfload = record
  ia,cidx : Tia; 
  hw,mv,nc,nv : int64;
  use_w,expl_w,pfound,pvalid : boolean;
  force_maxsat,quiet : boolean;
end;

PROCEDURE readcnf(fn : ansistring; var cf : Tcnfload);



IMPLEMENTATION
uses sysutils;


VAR
numchars : array[#0..#255] of boolean;
charval  : array[#0..#255] of longint;
lfchar   : array[#0..#255] of boolean;
lfchar2  : array[#0..#255] of boolean;
digit    : array[#0..#255] of boolean;


PROCEDURE readv(var s,s2 : ansistring);
BEGIN
  s2 := '';
  while (length(s)>0) and (s[1]=' ') do delete(s,1,1);
  while (length(s)>0) and not(s[1]=' ') do begin
    s2 += s[1]; 
    delete(s,1,1);
  end;
  while (length(s)>0) and (s[1]=' ') do delete(s,1,1);
END;


function remove_lines_and_get_p(p : Pchar; n : int64) : ansistring;
var i,j,k : int64; crlf : char; pline : ansistring;
begin
  pline := '';
  crlf := #10;
  i := 0;
  while (i<n) do begin
    if (p[i]=#13)or(p[i]=#10) then begin
      crlf := p[i];
      break;
    end; 
    inc(i);
  end;
  i := 0;
  while (i<n) do begin
    while (i<n)and(lfchar[p[i]]) do begin
      p[i] := #32;
      inc(i);
    end;
    j := IndexChar(p[i],n-i,crlf);
    if (j<0) then j := n-i-1;
    if (p[i]='c') then begin
      for k := j downto 0 do begin
        p[i] := #32;
        inc(i);
      end;
    end else if (p[i]='p') then begin
      for k := j downto 0 do begin
        if not(lfchar2[p[i]]) then pline += p[i];
        p[i] := #32;
        inc(i);
      end;
    end else begin
      for k := j downto 0 do begin
        if not numchars[p[i]] then p[i] := #32;
        inc(i);
      end;
    end;
  end;
  remove_lines_and_get_p := pline;
end;



FUNCTION readthefile(fn : ansistring; var cf : Tcnfload) : ansistring;
VAR s,ss,pt,nv,ncl,ntop : ansistring; f : file; 
BEGIN
  assign(f,fn);
  reset(f,1);
  setlength(ss,filesize(f));
  blockread(f,ss[1],filesize(f));
  close(f);
  ss := ss + ' 0 0 ';
  s := remove_lines_and_get_p(@ss[1],length(ss));
  if (length(s)>0) then begin
    cf.pfound := true;
    delete(s,1,1);
    readv(s,pt);
    readv(s,nv);
    readv(s,ncl);
    readv(s,ntop);
    if (lowercase(pt)='wcnf') then begin
      cf.pvalid := true;
      cf.expl_w := true;
      cf.use_w := true;
      if not(ntop='') then val(ntop,cf.hw);
      val(nv,cf.nv);
      val(ncl,cf.nc);
      if not(cf.quiet) then begin
        writeln('c cnf-head problem type=',pt);
        writeln('c cnf-head using weights : ',cf.use_w);
        writeln('c cnf-head explicit weights: ',cf.expl_w);
        writeln('c cnf-head top weight: ',cf.hw);
        writeln('c cnf-head clauses: ',cf.nc);
        writeln('c cnf-head vars: ',cf.nv);
      end;
    end else if (lowercase(pt)='cnf') then begin
      cf.pvalid := true;
      val(nv,cf.nv);
      val(ncl,cf.nc);
      if not(cf.quiet) then begin
        writeln('c cnf-head problem type=',pt);
        writeln('c cnf-head using weights : ',cf.use_w);
        writeln('c cnf-head clauses: ',cf.nc);
        writeln('c cnf-head vars: ',cf.nv);
      end;
    end;

  end;
  readthefile := ss;
END;




PROCEDURE doval(const s : ansistring; a,b : longint; var v : int64);
var i,sig : int64; c : char;
BEGIN
  v := 0;
  sig := 0;
  i := a;
  while (i<=b) do begin
    c := s[i];
    if (c='-') then begin
      inc(sig);
      inc(i);
    end else if (c='+') then begin
      inc(i);
    end else break;
  end;
  while (i<=b) do begin
    c := s[i];
    if digit[c] then begin
      v := v*10+charval[c];
      inc(i);
    end else break;
  end;
  if (sig and 1)>0 then v := -v;
  if (i<>b) then v := 0;
END;

FUNCTION nextnum(VAR i : int64; len : int64; s : ansistring) : int64;
VAR j,a : int64; pc : Pchar;
BEGIN
  pc := @pbyte(@s[1])[-1];
  while (i<len) do begin
    if numchars[pc[i]] then break;
    inc(i);
  end;
  j := i;
  while (i<len) do begin
    if not(numchars[pc[i]]) then break;
    inc(i);
  end;
  a := 0;
  doval(s,j,i,a);
  nextnum := a;
END;

PROCEDURE _nextnum(VAR i : int64; len : int64; const s : ansistring);
VAR pc : Pchar;
BEGIN
  pc := @pbyte(@s[1])[-1];
  while (i<len) do begin
    if numchars[pc[i]] then break;
    inc(i);
  end;
  while (i<len) do begin
    if not(numchars[pc[i]]) then break;
    inc(i);
  end;
END;


PROCEDURE str2intarr(const s : ansistring; var ia : Tia);
VAR i,j,len : int64; 
BEGIN
  len := length(s);
  i := 1;
  j := 0;
  while (i<len) do begin
    _nextnum(i,len,s);
    inc(j);
  end;
  setlength(ia,j);
  i := 1;
  j := 0;
  while (i<len) do begin
    ia[j] := nextnum(i,len,s);
    inc(j);
  end;
END;

PROCEDURE nextidx(var i : int64; const ia : Tia);
BEGIN
  inc(i);
  while (i<high(ia)) do begin
    if (ia[i]=0) then break;
    inc(i);
  end;
  inc(i);
END;

PROCEDURE ia2ca(ia : Tia; var cidx : Tia);
VAR i,j : int64;
BEGIN
  i := 0; j := 0;
  while (i<high(ia)) do begin
    nextidx(i,ia);
    inc(j);
  end;
  setlength(cidx,j);
  i := 0; j := 0;
  while (i<high(ia)) do begin
    cidx[j] := i;
    nextidx(i,ia);
    inc(j);
  end;
END;





PROCEDURE readcnf(fn : ansistring; var cf : Tcnfload);
var s : ansistring; i,j : int64;
BEGIN
  cf.use_w := cf.force_maxsat;
  s := readthefile(fn,cf);
  str2intarr(s,cf.ia);
  ia2ca(cf.ia,cf.cidx);
  cf.mv := 0;
  for i := high(cf.ia) downto 0 do begin
    j := abs(cf.ia[i]);
    if (j>cf.mv) then cf.mv := j;
  end;
END;



procedure init_nc(s : string);
var i : longint;
begin
  for i := 0 to 255 do numchars[chr(i)] := false;
  for i := 0 to 255 do lfchar[chr(i)] := false;
  for i := 0 to 255 do lfchar[chr(i)] := false;
  for i := 0 to 255 do digit[chr(i)] := false;
  for i := 0 to 255 do charval[chr(i)] := 0;
  for i := length(s) downto 1 do numchars[s[i]] := true;
  for i := 1 to 9 do begin
    charval[chr(ord('0')+i)] := i;
    digit[chr(ord('0')+i)] := true;
  end;
  digit['0'] := true;
  lfchar[#13] := true;
  lfchar[#10] := true;
  lfchar[#32] := true;
  lfchar2[#13] := true;
  lfchar2[#10] := true;
end;

BEGIN
  init_nc('0123456789+-');
END.
