# Passat, a combinatorial boolean constraint solver

Passat by design generates very short UNSAT proofs, relative to techniques used by many other CDCL SAT solvers.

My personal impression about performance is that it's relatively similar to minisat. 

100% pascal source code, in no way a remake/patch of anything. 

## Example problem
Given the web structure in the following image,

![](g35.svg "Graph Coloring")

find the minmum number of border colors, so that connected vertices don't have to share a color.
