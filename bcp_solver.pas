{$mode objfpc}
unit bcp_solver;

{$include config.pas}


interface
uses bare_solve_struct,clause;

type
Tcls_rlnk_cmp_lits = function(a,b : Tliteral) : boolean of object;




Tbcp_solver = class(Tbare_solve_struct)
  public
  constructor create();
  destructor destroy(); override;


  function bcp() : boolean;

  function inConflict() : boolean;
  procedure clear_conflict_indicators();
  procedure takeback_to_decilevel(dl : Tliteral);
  procedure apply_decision(lit : Tliteral);

  procedure onAfterLoad(); virtual;
  function cls_sat(cls : Pclause) : boolean;
  function cls_unsat(cls : Pclause) : boolean;

  procedure relink_cls_to(cls : Pclause; chain,litpos : Tliteral);
  procedure search_link_targets(cls : Pclause);
  function cls_propagate(cls : Pclause) : boolean;

  procedure check_cls_for_duplicate_lits(cls : Pclause);
  procedure link_cls_to_lit(cls : Pclause; chain,lit : Tliteral);
  procedure unlink_cls_from_lit(cls : Pclause; chain : Tliteral);


  procedure lazy_mark_varreasons_as_clause_ref();

  private
  procedure fast_bcp();
  procedure do_fastbcpcycle();
  procedure add_fastprop(lit : Tliteral; reason : Pclause);

  function is_cls_in_chain(cls : Pclause; chain, lit : Tliteral) : boolean;

  procedure repair_backlinks(cls : Pclause; chain : Tliteral);
  function get_good_linktarget(var targcnt : Tuliteral) : Tliteral;
  function earliest_satlit(var targcnt : Tuliteral) : Tliteral;

  function scan_cls(lits : Pliteral; len : Tuliteral; var targcnt : Tuliteral) : boolean;


  private
  procedure make_free_relinkjob();
  function get_free_relinkjob() : Prelinkjob;
  procedure release_relinkjob(p : Prelinkjob);
  procedure do_relinkjob(var p : Prelinkjob);
  procedure add_delayed_relinkjob(cls : Pclause; chain,lit,v : Tliteral);

  public
  bcp_subcycle_num : qword;
  
  last_problem_clause : Pclause;

  protected
  new_known_vars : Tliteral;
  fastprop_head : Tliteral;
  propagations : Tpropagatelist;

  public
  varflips : qword;
  bcp_clsitemsteps, bcp_learnt_clsitemsteps : qword;

  private
  lazy_dl0_reason_historypoint : Tliteral;

  free_relinkjobs : Prelinkjob;
end;





implementation
uses sysutils,rand,math;


constructor Tbcp_solver.create();
begin
  inherited create();
  propagations := Tpropagatelist.create();
  varflips := 0;
  lazy_dl0_reason_historypoint := 0;
end;

destructor Tbcp_solver.destroy();
var i : Tliteral; p : Prelinkjob;
begin
  if num_vars>0 then begin
    for i := 0 to num_vars do begin
      while varelinkjobs[i]<>nil do begin
        p := varelinkjobs[i];
        varelinkjobs[i] := p^.next;
        release_relinkjob(p);
      end;
    end;
  end;
  while free_relinkjobs<>nil do begin
    p := free_relinkjobs;
    free_relinkjobs := p^.next;
    dispose(p);
  end;

  propagations.destroy();
  inherited destroy();
end;


procedure Tbcp_solver.onAfterLoad();
begin
  last_problem_clause := latestclause;
  clear_conflict_indicators();
  bcp();
end;


procedure Tbcp_solver.make_free_relinkjob();
var p : Prelinkjob;
begin
  new(p);
  release_relinkjob(p);
end;

function Tbcp_solver.get_free_relinkjob() : Prelinkjob;
var p : Prelinkjob;
begin
  if free_relinkjobs=nil then make_free_relinkjob();
  p := free_relinkjobs;
  free_relinkjobs := p^.next;
  get_free_relinkjob := p;
end;

procedure Tbcp_solver.release_relinkjob(p : Prelinkjob);
begin
  p^.next := free_relinkjobs;
  free_relinkjobs := p;
end;

procedure Tbcp_solver.add_delayed_relinkjob(cls : Pclause; chain,lit,v : Tliteral);
var p : Prelinkjob;
begin
  p := get_free_relinkjob();
  p^.cls := cls;
  p^.lit := lit;
  p^.chain := chain;
  p^.next := varelinkjobs[v];
  varelinkjobs[v] := p;
end;

procedure Tbcp_solver.do_relinkjob(var p : Prelinkjob);
var p2 : Prelinkjob; lit : Tliteral; lits : Pliteral; cls : Pclause;
begin
  p2 := p^.next;
  cls := p^.cls;
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  lit := lits[p^.chain];
  if varstate[abs(lit)]=0 
  then link_cls_to_lit(cls,p^.chain,lit)
  else raise Texc_trk.create('relinkjob with still-assigned literal');
  release_relinkjob(p);
  p := p2;
end;



function Tbcp_solver.inConflict() : boolean;
begin
  inConflict := (conflict_var<>0)or(conflict_clause<>nil);
end;

procedure Tbcp_solver.clear_conflict_indicators();
begin
  conflict_var := 0;
  conflict_clause := nil;
end;

procedure Tbcp_solver.takeback_to_decilevel(dl : Tliteral);
var lit,alit : Tliteral;
begin
  while (historylen>0) do begin
    dec(historylen);
    lit := varhistory[historylen];
    alit := abs(lit);
    if vardecilevel[alit]>dl then begin
      varstate[alit] := 0;
      varstateb[lit+num_varsb] := false;
      while varelinkjobs[alit]<>nil do do_relinkjob(varelinkjobs[alit]);
    end else begin
      inc(historylen);
      break;
    end;
  end;
  decilevel := dl;
end;

procedure Tbcp_solver.apply_decision(lit : Tliteral);
begin
  inc(decicnt);
  inc(decilevel);
  decivar[decilevel] := lit;
  bcp_subcycle_num := 0;
  propagations.push(lit,nil);
  bcp();
end;







procedure Tbcp_solver.fast_bcp();
var lit,alit : Tliteral; reason : Pclause;
begin
  fastprop_head := historylen;
  while propagations.pop(lit, reason) do begin
    alit := abs(lit);
    if (varstate[alit]=0) then begin
      if (decilevel=0) then new_known_vars += 1;
      add_fastprop(lit,reason);
    end else if (varstate[alit]<>lit) then begin
      conflict_var := lit;
      conflict_var_reason := reason;
      exit;
    end;
  end;
  do_fastbcpcycle();
end;


{$GOTO ON}







procedure Tbcp_solver.do_fastbcpcycle();
var chain,lit,other,satlit : Tliteral;  n,m : Tuliteral;
  cls,cls2,clsnxt : Pclause; lits : Pliteral; idx : qword; 
begin
  inc(propcyc);
  while (fastprop_head<historylen) do begin
    inc(propcnt);
    lit := varhistory[fastprop_head];
    idx := abs(lit);
    idx := 4*idx +ord(-lit<0);
    { For any new lit in trail, we need to process the clauses that contain -lit,
      and that are linked to the var abs(lit). We have two linked lists of
      such clauses, chain=0 and chain=1 (representing the cases 
      "linked to clause 1st/2nd literal").
    }
    for chain := 0 to 1 do begin
      cls := varwatches[idx];
      cls2 := nil;
      while (cls<>nil) do begin
        inc(clchecks);

inc(bcp_clsitemsteps);
if (cls^.flags and clausetag_learnt)>0 then inc(bcp_learnt_clsitemsteps);
inc(cls^.touched);

        lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
        clsnxt := cls^.nxt[chain];

        { do an early-out check first, to probably avoid heavy scan }
        other := lits[1-chain];
        lit := abs(other); // alit would be prettier name

        if (other=varstate[lit]) then begin
          { cls-check early out because of cls is satisfied (by other) }
          satlit := vardecilevel[lit]; // other_decilevel would be prettier name
          if satlit>0 then begin
            { normal situation: other is true.
              depending on wether it was assigned on the current 
              decilevel (just like this currently processed watch)
              or some time earlier, either use the normal "reapair backlinks"
              style to keep the link here, where it was, or request
              a relink delayed until backtracks unassign other. 
            }
            if satlit<decilevel 
            then add_delayed_relinkjob(cls,chain,lits[chain],lit)
            else begin
              cls^.nxt[chain] := cls2;
              cls2 := cls;
            end;
          end else begin
            { if other=true without assumptions applied,
              do not relink, and prevent the clause from getting linked again
             }
            cls^.flags := cls^.flags or clausetag_linkless;
          end;
        end else begin

          { early-out check failed.

            the clause isn't obviously satisfied, and needs relink,
            so we have to do a heavy clause scan to see if it propagates,
            and where it could be linked to. 
          }
inc(bcp_clsitemsteps,cls^.len);
if (cls^.flags and clausetag_learnt)>0 then inc(bcp_learnt_clsitemsteps,cls^.len);
          n := 0;
          if scan_cls(@lits[2],cls^.len-2,n) then begin

            { clause is satisfied }
            satlit := earliest_satlit(n);
            lit := lits[0];
            if vardecilevel[abs(satlit)]>0 then begin
              link_cls_to_lit(cls,chain,satlit);
              { check if there is another true lit}
              if (n>0) then begin
                { yes, multiple true lits, also update the other link }
                unlink_cls_from_lit(cls,1-chain);
                link_cls_to_lit(cls,1-chain,earliest_satlit(n));
              end else begin
                { no additional true literal, but given this clause is currently
                  satisfied, let's attempt to relink other to the false literal
                  processed just now
                }
                unlink_cls_from_lit(cls,1-chain);
                link_cls_to_lit(cls,1-chain,lit);
              end;
            end else begin
              { if lit=true without assumptions applied,
                do not relink, unlink other and prevent the clause from getting linked again
               }
              unlink_cls_from_lit(cls,1-chain);
              cls^.flags := cls^.flags or clausetag_linkless;
            end;

          end else begin
            { clause isn't satisfied }
            m := n;
            { check if other link is on a valid link-target }
            if (varstate[lit]=0) then inc(m);
            if (m>1) then begin
              { Nothing special happend. No propagation. Cls not satisfied. More than one tagets. }
              link_cls_to_lit(cls,chain,get_good_linktarget(n));
            end else if (m=0) then begin
inc(cls^.propagated);
              { Clause is violated. }
              { Copy the remaining cls-links back to here, where they have been and leave. }
              conflict_clause := cls;
              while (cls<>nil) do begin
                clsnxt := cls^.nxt[chain];
                cls^.nxt[chain] := cls2;
                cls2 := cls;
                cls := clsnxt;
              end;
              varwatches[idx] := cls2;
              repair_backlinks(cls2,chain);
              exit;
            end else begin
inc(cls^.propagated);
              { m=1, Clause propagates.}
              vartmp[n] := other;
              add_fastprop(vartmp[0],cls);
              { let cls where it is}
              cls^.nxt[chain] := cls2;
              cls2 := cls;
            end;
          end;

        end;
        cls := clsnxt;
      end;
      varwatches[idx] := cls2;
      repair_backlinks(cls2,chain);
      { to 2nd chain, or back to outer trail-loop}
      idx += 2;      
    end;
    fastprop_head += 1;
  end;
end;


procedure Tbcp_solver.repair_backlinks(cls : Pclause; chain : Tliteral);
var cls2 : Pclause;
begin
  cls2 := nil;
  while (cls<>nil) do begin
    cls^.nxt[chain+2] := cls2;
    cls2 := cls;
    if (cls=cls^.nxt[chain]) then raise Texc_trk.create('cannot (cls=cls^.nxt[chain])');
    cls := cls^.nxt[chain];
  end;
end;






{ Heavy scan a clause during bcp.
  * find out, if the clause is satisfied
  * find out, if the clause is propagating
  * find out, if the clause is violated
  * identify possible watch-link targets

  Ret true, if cls satisfied.
  Ret link-targets/satisfied lits in vartmp.
  Always ret targcnt, the number of possible link-targets in vartmp.
}
function Tbcp_solver.scan_cls(lits : Pliteral; len : Tuliteral; var targcnt : Tuliteral) : boolean;
var lit,vs : Tliteral; i : Tuliteral; issat : boolean;
begin
  targcnt := 0; 
  i := 0;
  { find propagate/relink-targets, abort on issat }
  issat := false;
  while (i<len) do begin
    lit := lits[i];
    vs := varstate[abs(lit)];
    inc(i);
    if (vs=0) then begin
      vartmp[targcnt] := lit;
      inc(targcnt);
    end else if (vs=lit) then begin
      issat := true;
      break;
    end;
  end;
  { if issat, make a list of satisfied literals instead }
  if issat then begin
    dec(i);
    targcnt := 0; 
    while (i<len) do begin
      lit := lits[i];
      vs := varstate[abs(lit)];
      if (vs=lit) then begin
        vartmp[targcnt] := lit;
        inc(targcnt);
      end;
      inc(i);
    end;
  end;
  scan_cls := issat;
end;



{ Return the literal assigned earliest from a 
  list of satisfied literals as generated by scan_cls().

  Assumes that targcnt>=1.
  Prepare for repeated call.
}
function Tbcp_solver.earliest_satlit(var targcnt : Tuliteral) : Tliteral;
var lit,litdl,i,idl,ilit : Tliteral; 
begin
  dec(targcnt);
  i := targcnt;
  lit := vartmp[i];
  litdl := vardecilevel[abs(lit)];
  dec(i);
  while (i>=0) do begin
    ilit := vartmp[i];
    idl := vardecilevel[abs(ilit)];
    if (idl<litdl) then begin
      vartmp[i] := lit;
      vartmp[targcnt] := ilit;
      lit := ilit;
      litdl := idl;
    end;
    dec(i);
  end;
  earliest_satlit := lit;
end;




{ Return a good link-target from a 
  list of possible targets as generated by scan_cls().

  Assumes that targcnt>=1.
  Prepare for repeated call?
}


function Tbcp_solver.get_good_linktarget(var targcnt : Tuliteral) : Tliteral;
var lit,i : Tliteral; 
begin
  { try to use first var with oldstate=true }
  dec(targcnt);
  i := 0;
  while (i<=targcnt) do begin
    lit := vartmp[i];
    if varoldstate[abs(lit)]=lit then begin
      vartmp[i] := vartmp[targcnt];
      vartmp[targcnt] := lit;
      get_good_linktarget := lit;
      exit;
    end;
    inc(i);
  end;
  i := random_qword(prng) mod Tuliteral(targcnt+1);
  //i := 0;
  lit := vartmp[i];
  if (i<>targcnt) then begin
    vartmp[i] := vartmp[targcnt];
    vartmp[targcnt] := lit;
  end;
  get_good_linktarget := lit;
end;






procedure Tbcp_solver.check_cls_for_duplicate_lits(cls : Pclause);
var lits : Pliteral; i : Tliteral; 
begin
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  for i := cls^.len-1 downto 0 do begin
    varvisit[abs(lits[i])] := false;
  end;
  for i := cls^.len-1 downto 0 do begin
    if varvisit[abs(lits[i])] then begin
      writeln;
      printcls(lits,cls^.len,false);
      raise Texc_trk.create('cls with duplicate literals');
    end;
    varvisit[abs(lits[i])] := true;
  end;
  for i := cls^.len-1 downto 0 do begin
    varvisit[abs(lits[i])] := false;
  end;
end;


procedure Tbcp_solver.link_cls_to_lit(cls : Pclause; chain,lit : Tliteral);
var i : SizeInt; lits : Pliteral; lit2 : Tliteral; idx : qword;
begin
{$ifdef debug_checks}
  check_cls_for_duplicate_lits(cls);
{$endif}
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  i := IndexDWord(lits[0],cls^.len,lit);
  if (i<0) then raise Texc_trk.create('cannot link cls to missing literal');
  if (i=(1-chain)) then raise Texc_trk.create('attempt to link cls to existing link');
  if (i<>chain) then begin
    lit2 := lits[chain];
    lits[chain] := lits[i];
    lits[i] := lit2;
  end;
  idx := abs(lit);
  idx := idx*4 +2*chain +ord(lit<0);

  cls^.nxt[chain] := varwatches[idx];
  cls^.nxt[chain+2] := nil;
  if varwatches[idx]<>nil then varwatches[idx]^.nxt[chain+2] := cls;
  varwatches[idx] := cls;
end;


procedure Tbcp_solver.unlink_cls_from_lit(cls : Pclause; chain : Tliteral);
var lits : Pliteral; lit : Tliteral; idx : qword; prvcls,nxtcls : Pclause;
{$ifdef debug_checks}
var n,n2 : qword; c1,c2 : Pclause;
{$endif}
begin
  nxtcls := cls^.nxt[chain];
  prvcls := cls^.nxt[chain+2];

  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  lit := lits[chain];
  idx := abs(lit);
  idx := idx*4 +2*chain +ord(lit<0);

{$ifdef debug_checks}
  n := 0;
  c1 := varwatches[idx];
  c2 := nil;
  while c1<>nil do begin
    if (c2<>c1^.nxt[chain+2]) then raise Texc_trk.create('backlinks broken');
    inc(n);
    c2 := c1;
    c1 := c1^.nxt[chain];
  end;
{$endif}


  if varwatches[idx]=cls then varwatches[idx] := nxtcls;
  if (nxtcls<>nil) then begin
    nxtcls^.nxt[chain+2] := prvcls;
  end;
  if (prvcls<>nil) then begin
    prvcls^.nxt[chain] := nxtcls;
  end;

{$ifdef debug_checks}
  n2 := 0;
  c1 := varwatches[idx];
  c2 := nil;
  while c1<>nil do begin
    if (c2<>c1^.nxt[chain+2]) then raise Texc_trk.create('backlinks broken2');
    inc(n2);
    c2 := c1;
    c1 := c1^.nxt[chain];
  end;
  if ((n2+1)<>n) then raise Texc_trk.create('backlink-count broken');
{$endif}
end;











procedure Tbcp_solver.add_fastprop(lit : Tliteral; reason : Pclause);
var avs : Tliteral; 
begin
  avs := abs(lit);
{$ifdef debug_checks}
  if varstate[avs]<>0 then raise Texc_trk.create('illegal var-state (re)set:'+inttostr(lit)+' old='+inttostr(varstate[avs]));
  if lit=0 then raise Texc_trk.create('illegal var-state 0 (re)set:'+inttostr(lit)+' old='+inttostr(varstate[avs]));
  if (historylen>=num_vars) then raise Texc_trk.create('history-overflow:'+inttostr(historylen)+' old='+inttostr(num_vars));
{$endif}
  varstate[avs] := lit;
  varstateb[lit+num_varsb] := true;
  if varoldstate[avs]<>lit then inc(varflips);
  varoldstate[avs] := lit;
  varreason[avs] := reason;
  vardecilevel[avs] := decilevel;
{$ifdef dbg_use_decipt}
  vardecipt[avs] := historylen;
{$endif}
  varhistory[historylen] := lit;
  inc(historylen);
{$ifdef use_varsigch}
  if (lit<0) then dec(varsigch[avs]) else inc(varsigch[avs]); 
{$endif}
end;






procedure Tbcp_solver.relink_cls_to(cls : Pclause; chain,litpos : Tliteral);
var lits : Pliteral;
begin
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  link_cls_to_lit(cls,chain,lits[litpos]);
end;





procedure Tbcp_solver.search_link_targets(cls : Pclause);
var alit,i,lit : Tliteral; lits : Pliteral;
begin
  lits := Pliteral(pointer(@pbyte(pointer(cls))[sizeof(Tclause)]));
  lit := lits[0];
  alit := abs(lit);
  if varstate[alit]<>0 then begin
    for i := cls^.len-1 downto 2 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=0 then begin
        lits[i] := lits[0];
        lits[0] := lit;
        break;
      end;
    end;
  end;
  if cls^.len<3 then exit;
  lit := lits[1];
  alit := abs(lit);
  if varstate[alit]<>0 then begin
    for i := cls^.len-1 downto 2 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=0 then begin
        lits[i] := lits[1];
        lits[1] := lit;
        break;
      end;
    end;
  end;
end;



function Tbcp_solver.bcp() : boolean;
begin
  if not inConflict() then fast_bcp() else propagations.clear();
  bcp := (conflict_clause=nil)and(conflict_var=0);
end;



function Tbcp_solver.is_cls_in_chain(cls : Pclause; chain, lit : Tliteral) : boolean;
var idx : qword; c : Pclause;
begin
  idx := abs(lit);
  idx := 4*idx +2*chain +ord(lit<0);
  c := varwatches[idx];
  while (c<>nil) do begin
    if (c=cls) then begin
      is_cls_in_chain := true;
      exit;
    end;
    c := c^.nxt[chain];
  end;
  is_cls_in_chain := false;
end;






function Tbcp_solver.cls_sat(cls : Pclause) : boolean;
var i,lit,alit : Tliteral; lits : Pliteral; 
begin
  if (cls<>nil) then begin
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=lit then begin
        cls_sat := true;
        exit;
      end;
    end;
  end;
  cls_sat := false;
end;


function Tbcp_solver.cls_unsat(cls : Pclause) : boolean;
var i,lit,alit : Tliteral; lits : Pliteral; 
begin
  if (cls<>nil) then begin
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]<>-lit then begin
        cls_unsat := false;
        exit;
      end;
    end;
  end;
  cls_unsat := true;
end;

function Tbcp_solver.cls_propagate(cls : Pclause) : boolean;
var i,lit,alit,sat,prp,prplit : Tliteral; lits : Pliteral; 
begin
  sat := 0; prp := 0; prplit := 0;
  if (cls<>nil) then begin
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      if varstate[alit]=lit then inc(sat)
      else if varstate[alit]=0 then begin
        inc(prp);
        prplit := lit;
      end;
    end;
  end;
  if (sat=0)and(prp=1) then begin
    propagations.push(prplit,cls);
    cls_propagate := true;
  end else cls_propagate := false;
end;


procedure Tbcp_solver.lazy_mark_varreasons_as_clause_ref();
var cls : Pclause; lit : Tliteral;
begin
  while (lazy_dl0_reason_historypoint<historylen) do begin
    lit := varhistory[lazy_dl0_reason_historypoint];
    inc(lazy_dl0_reason_historypoint);
    cls := varreason[abs(lit)];
    if cls<>nil then begin
      inc(cls^.refcnt);
    end;
  end;
end;





begin
end.

