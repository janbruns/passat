{$mode objfpc}
unit trkpst_solver;

{$include config.pas}


interface
uses trk_solver,clause;

type
T_learncomparefunc = function() : int64 of object;
Tlearncompare = record f : T_learncomparefunc; s : int64; end;
Tqsortcmp = function(a,b : Tliteral) : boolean of object;

Ttrkpst_solver = class(Ttrk_solver)
  public
  constructor create();
  function resolve_conflict() : boolean;
  function resolve_conflicts() : boolean; // recursive resolve_conflict()
  procedure trk_sort_learn_clause_by_lits();
  procedure trk_sort_learn_clause_by_cc();
  procedure maybe_decay_cc();
  procedure maybe_repair_ccnxt_order();
  function trk_put_learn_candidate() : boolean; override;
  procedure trk_onBeforeTrk(); override;
  procedure trk_onAfterTrk(); override;

  protected

  procedure onAfterTackeBack(); virtual; abstract;

  function trk_alloc_learn_clause(rand : boolean) : Pclause;
  procedure trk_learn_clause();

  procedure trk_doConflictCounts(); override;

  procedure decay_cc_fact();

  private

  procedure trk_qsort_learn_clause_swp(a,b : Tliteral);  
  function check_cc_sorting(verb : Tliteral) : Tliteral;



  function trk_qsort_learn_clause_cmp_by_lits(a,b : Tliteral)  : boolean;
  function trk_qsort_learn_clause_cmp_by_cc(a,b : Tliteral)  : boolean;
  procedure trk_qsort_learn_clause(a,b : Tliteral; cmp : Tqsortcmp);
  procedure trk_qsort_learn_clause_cc(a,b : Tliteral);



  public
  trk_learnt_clauses : qword;
  slowccnt_fresh, slowccnt_putback : double;
  trk_slow_decay_counter : Tliteral;
  trk_slow_decay_intervall : Tliteral;
  keep_cc_order : boolean;
  scale_experiment : double;

  protected
  trk_learn_cls_extra : Tliteral;
  trk_learn_2nddl : Tliteral;
  trk_learn_1st, trk_learn_2nd : Tliteral; 
  trk_learn_dlsum,
  trk_learn_ccsum : int64; // both lit cc sum
  trk_checked_learn_candidates,
  trk_max_checked_learn_candidates : Tliteral;

  trk_decay_intervall : qword;


  trk_cc_inc,
  trk_cc_incgrow,
  trk_cc_targ_incgrow,
  trk_cc_inc_rndfrac : double;

  trk_multitb : qword;

  trk_stat_dl : double;
  trk_stat_cclen, trk_stat_expcc : qword;
  trk_stat_cclen_nlogn, trk_stat_ccexp_nlogn : double;


  private
  trk_learn_cls_expanded_lits : Tliteral;
  trk_decay_counter : qword;

end;


implementation
uses bare_solve_struct,rand;

constructor Ttrkpst_solver.create();
begin
  inherited create();

  trk_learnt_clauses := 0;
  
  trk_slow_decay_counter := 0;
  trk_slow_decay_intervall := 35;
  slowccnt_fresh := 0.05;
  slowccnt_putback := 0.4;

  trk_decay_intervall := 100;
  trk_cc_incgrow := 1.05;
  trk_cc_targ_incgrow := trk_cc_incgrow;

  trk_cc_inc_rndfrac := 0.0005;

  // dynamic:
  trk_cc_inc := 1;
  trk_decay_counter := 0;

  trk_multitb := 0;
  cccxt_order_clean := false;

  trk_stat_dl := 0;
  trk_stat_cclen := 0; 
  trk_stat_expcc := 0;
  trk_stat_cclen_nlogn := 0;
  trk_stat_ccexp_nlogn := 0;

  scale_experiment := 0;
  keep_cc_order := true;
end;




procedure Ttrkpst_solver.trk_onBeforeTrk();
begin
end;

procedure Ttrkpst_solver.trk_onAfterTrk();
begin
end;



procedure Ttrkpst_solver.decay_cc_fact();
var i : Tliteral; var q : double;
begin
  q := 1/trk_cc_inc;
  for i := 1 to num_vars do begin
    varccnt[i] *= q;
  end;
  trk_cc_inc := 1;
  trk_cc_incgrow := trk_cc_incgrow*0.999 + 0.001*trk_cc_targ_incgrow;
end;




procedure Ttrkpst_solver.maybe_decay_cc();
begin
  inc(trk_decay_counter);
  if (trk_decay_counter>=trk_decay_intervall) then begin
    trk_decay_counter := 0;
    decay_cc_fact();
    if cccxt_order_clean 
    then cccxt_order_clean := (0=check_cc_sorting(0));
    maybe_repair_ccnxt_order();
  end;
end;



procedure Ttrkpst_solver.trk_doConflictCounts();
var i,lit,alit : Tliteral; pts : double; maxexpcyc : dword;
begin
  maxexpcyc := 0;
  i := trk_learn_cls_expanded_lits + trk_learn_cls_len;
  for i := i-1 downto 0 do begin
    lit := trkvar_learncls[i];
    alit := abs(lit);
    if varexpcyc[alit]>maxexpcyc then maxexpcyc := varexpcyc[alit];
  end;


//  pts := 0.1+ 10*(historylen/num_vars)/decilevel; 
  pts := 1;
//  pts := 1/decilevel; 
//  pts := 10+sqrt(1+decilevel-trk_learn_2nddl);///sqrt(1+trk_learn_2nddl);

  for i := 0 to trk_learn_cls_len-1 do begin
    lit := trkvar_learncls[i];
    alit := abs(lit);
pts := (1+varexpcyc[alit])/(maxexpcyc+1);
pts := 1 + scale_experiment*pts;
    varccnt[alit] += pts*trk_cc_inc*(1+trk_cc_inc_rndfrac*(random_float(prng)-0.5));
  end;

  { also add some of the expanded literals  }
//pts *= 0.0;
  for i := trk_learn_cls_expanded_lits-1 downto 0 do begin
    lit := trkvar_learncls[trk_learn_cls_len];
    trk_learn_cls_len += 1;
    alit := abs(lit);
pts := (1+varexpcyc[alit])/(maxexpcyc+1);
pts := 1 + scale_experiment*pts;
    varccnt[alit] += pts*trk_cc_inc*(1+trk_cc_inc_rndfrac*(random_float(prng)-0.5));
  end;


  { in order to minimize position change delta within ccnxt chain,
    we sort here the learnts. Worst case (apart from using qsort) would be
    an update on all vars,  and runtime would then probably be O(n*log(n)) +O(n),
    much better than O(n*n) we'd see without sorting.
  }
  if keep_cc_order then begin
    trk_sort_learn_clause_by_cc();
    handle_sorted_learnt_cc_change(trk_learn_cls_len);
  end else cccxt_order_clean := false;
  trk_cc_inc *= trk_cc_incgrow;
  maybe_decay_cc();
end;


function Ttrkpst_solver.check_cc_sorting(verb : Tliteral) : Tliteral;
var a,b,c,e : Tliteral;
begin
  c := 0; e := 0;
  a := top_ccnxt;
  while (true) do begin
    b := varccnxt[a];
    if (a>0) then begin
      inc(c);
      if (b>0) then begin
        if (varccnt[b]>varccnt[a]) then begin
          e := b;
        end;
      end;
    end else break;
    a := b;
  end;
  if (c=num_Vars) then check_cc_sorting := e else check_cc_sorting := -1;
end;





procedure Ttrkpst_solver.maybe_repair_ccnxt_order();
var i : Tliteral;
begin
  if keep_cc_order and not cccxt_order_clean then begin
    if notquiet then write('c repair ccnxt for ',num_vars,' vars.');
    trk_learn_cls_len := num_vars;
    for i := 0 to num_vars-1 do trkvar_learncls[i] := i+1;
    trk_sort_learn_clause_by_cc();
    apply_ccnxt_order_from_lrncls();
    cccxt_order_clean := true;
    trk_learn_cls_len := 0;
  end;
end;







function Ttrkpst_solver.trk_put_learn_candidate() : boolean;
var i,lit,alit,dl : Tliteral;
begin
  trk_learn_cls_len := 0;
  trk_learn_cls_extra := 0;
  trk_learn_dlsum := 0;
  trk_learn_ccsum := 0;
  trk_learn_2nddl := 0;
  trk_learn_2nd := 0;
  trk_learn_1st := 0;

  move(trkvar_conflcls[0], trkvar_learncls[0], trk_cur_conflcls_len*sizeof(Tliteral));
  move(trkvar_expstore[0], trkvar_learncls[trk_cur_conflcls_len], trk_expanded_lits*sizeof(Tliteral));
  
  trk_learn_cls_len := trk_cur_conflcls_len;
  trk_learn_cls_expanded_lits := trk_expanded_lits;
  trk_learn_cls_extra := trk_expanded_lits;
  for i := trk_expanded_lits-1 downto 0 do begin
    trkvar_learnextra[i] := varreason[abs(trkvar_expstore[i])];
  end;
  trkvar_learnextra[trk_learn_cls_extra] := conflict_clause;
  trk_learn_cls_extra += 1;

  for i := trk_learn_cls_len-1 downto 0 do begin
    lit := trkvar_learncls[i];
    alit := abs(lit);
    dl := vardecilevel[alit];
    if (dl>trk_learn_2nddl)and(dl<decilevel) then begin
      trk_learn_2nddl := dl;
      trk_learn_2nd := lit;
    end;
    if (dl=decilevel) then begin
      if (trk_learn_1st<>0) then raise Texc_trk.create('Trk: not UIP');
      trk_learn_1st := lit;
    end;
  end;
//  writeln('learncls:');
//  printcls(trkvar_learncls,trk_learn_cls_len,true);
  trk_put_learn_candidate := false;
end;





function Ttrkpst_solver.trk_alloc_learn_clause(rand : boolean) : Pclause;
var cls,cls2 : Pclause; i : int64;
begin
  cls := createClause(trk_learn_cls_len, trk_learn_cls_extra );
  move(trkvar_learncls[0],  getLiterals(cls)^, trk_learn_cls_len  *sizeof(Tliteral) );
//  move(trkvar_learnextra[0],getExtra(cls)^,    trk_learn_cls_extra*sizeof(Pclause)  );
  if (trk_learn_cls_extra>0) then begin
    cl_spool.set_cls_extra(cls,trk_learn_cls_extra,trkvar_learnextra);
    i := trk_learn_cls_extra;
    while (i>0) do begin
      dec(i);
      cls2 := trkvar_learnextra[i];
      inc(cls2^.refcnt);
    end;
  end;
  if rand then begin
    randomize_cls(cls);
  end;
  trk_alloc_learn_clause := cls;
end;





function Ttrkpst_solver.trk_qsort_learn_clause_cmp_by_lits(a,b : Tliteral)  : boolean;
begin
  trk_qsort_learn_clause_cmp_by_lits := abs(trkvar_learncls[b])<abs(trkvar_learncls[a]);
end;

function Ttrkpst_solver.trk_qsort_learn_clause_cmp_by_cc(a,b : Tliteral)  : boolean;
begin
  trk_qsort_learn_clause_cmp_by_cc := (varccnt[abs(trkvar_learncls[b])]>varccnt[abs(trkvar_learncls[a])]);
end;

procedure Ttrkpst_solver.trk_qsort_learn_clause_swp(a,b : Tliteral);
var c : Tliteral;
begin
  c := trkvar_learncls[a];
  trkvar_learncls[a] := trkvar_learncls[b];
  trkvar_learncls[b] := c;
end;



procedure Ttrkpst_solver.trk_qsort_learn_clause(a,b : Tliteral; cmp : Tqsortcmp);
var ref,c,d : Tliteral;
begin
  repeat
    c := a;
    d := b;
    ref := (a+b) div 2;
    repeat
      while ( cmp(ref,c) ) do inc(c);
      while ( cmp(d,ref) ) do dec(d);
      if (c<=d) then begin
        trk_qsort_learn_clause_swp(c,d);
        if (ref=c) then ref := d 
        else if (ref=d) then ref := c;
        inc(c);
        dec(d);
      end;
    until (c>d);
    if (a<d) then trk_qsort_learn_clause(a,d,cmp);
    a := c;
  until (c>=b);
end;




procedure Ttrkpst_solver.trk_qsort_learn_clause_cc(a,b : Tliteral);
var ref,c,d : Tliteral; refcc : double;
begin
  repeat
    c := a;
    d := b;
    ref := (a+b) div 2;
    repeat
      refcc := varccnt[abs(trkvar_learncls[ref])];
      while ( varccnt[abs(trkvar_learncls[c])] > refcc ) do inc(c);
      while ( refcc > varccnt[abs(trkvar_learncls[d])] ) do dec(d);
      if (c<=d) then begin
        trk_qsort_learn_clause_swp(c,d);
        if (ref=c) then ref := d 
        else if (ref=d) then ref := c;
        inc(c);
        dec(d);
      end;
    until (c>d);
    if (a<d) then trk_qsort_learn_clause_cc(a,d);
    a := c;
  until (c>=b);
end;




procedure Ttrkpst_solver.trk_sort_learn_clause_by_lits();
begin
  if (trk_learn_cls_len>1) then trk_qsort_learn_clause(0,trk_learn_cls_len-1,@trk_qsort_learn_clause_cmp_by_lits);
end;

procedure Ttrkpst_solver.trk_sort_learn_clause_by_cc();
begin
  if (trk_learn_cls_len>1) then trk_qsort_learn_clause_cc(0,trk_learn_cls_len-1);
end;




procedure Ttrkpst_solver.trk_learn_clause();
var cls : Pclause; lits : Pliteral; i,a,b : Tliteral;
begin
  trk_stat_dl += decilevel;
  trk_stat_cclen += trk_learn_cls_len; 
  trk_stat_expcc += trk_learn_cls_expanded_lits;
  trk_stat_cclen_nlogn += trk_learn_cls_len*ln(trk_learn_cls_len)/ln(2);
  trk_stat_ccexp_nlogn += (trk_learn_cls_len+trk_learn_cls_expanded_lits)*ln(trk_learn_cls_len+trk_learn_cls_expanded_lits)/ln(2);

// printcls(trkvar_learncls,trk_learn_cls_len,false);


  for i := trk_learn_cls_len-1 downto 0 do begin
    a := trkvar_learncls[i];
    inc(litccoccurcnt[Tuliteral(abs(a))*2+ord(a<0)]);
  end;

  cls := trk_alloc_learn_clause(true);
  cls^.prv := latestclause;
  latestclause := cls;
  cls^.flags := cls^.flags or clausetag_learnt;
  inc(trk_learnt_clauses);

//  if not trk_used_fallback then trk_doConflictCounts();
// writeln('learnclslen=',cls^.len,' extra=',cls^.extra);

  if (cls^.len>1) then begin
    lits := getLiterals(cls);
    //printcls(lits,cls^.len,false);
    a := -1; b := -1;
    for i := cls^.len-1 downto 0 do begin
      if lits[i]=trk_learn_1st then a := i;
      if lits[i]=trk_learn_2nd then b := i;
    end;
    i := lits[0];
    lits[0] := lits[a];
    lits[a] := i; 
    //printcls(lits,cls^.len,false);
    a := -1; b := -1;
    for i := cls^.len-1 downto 0 do begin
      if lits[i]=trk_learn_1st then a := i;
      if lits[i]=trk_learn_2nd then b := i;
    end;
    i := lits[1];
    lits[1] := lits[b];
    lits[b] := i;
    //printcls(lits,cls^.len,false);
    relink_cls_to(cls,0, 0);
   // printcls(lits,cls^.len,false);
    relink_cls_to(cls,1, 1);
//   printcls(lits,cls^.len,false);
  end else begin
  end;
  takeback_to_decilevel(trk_learn_2nddl);
  if (varstate[abs(trk_learn_1st)]<>0) then raise Texc_trk.create('Unexpected still-assigned 1st learn literal');
  if (trk_learn_2nd<>0)and(varstate[abs(trk_learn_2nd)]=0) then raise Texc_trk.create('Unexpected non-assigned 2nd learn literal '+hexstr(trk_learn_2nd,8)+' dl='+hexstr(decilevel,8));
  propagations.push(trk_learn_1st,cls);

  a := 0;
  lits := getLiterals(cls);
  for i := cls^.len-1 downto 0 do begin
    if varstate[abs(lits[i])]=0 then a += 1;
  end;
  if (a<>1)and(cls^.len>1) then raise Texc_trk.create('Learnt cls wouldn''t really propagate');

  bcp();

  if (varstate[abs(trk_learn_1st)]=0) then raise Texc_trk.create('Learnt cls didn''t propagate');
end;

function Ttrkpst_solver.resolve_conflict() : boolean;
begin
  if (decilevel>0) then begin
    analyse_conflict();
    clear_conflict_indicators();
    trk_learn_clause();

    onAfterTackeBack();
    resolve_conflict := true;
  end else begin
    resolve_conflict := false;
  end;
end;

function Ttrkpst_solver.resolve_conflicts() : boolean;
begin
  trk_multitb := 0;
  resolve_conflicts := false;
  while inConflict() do begin
    inc(trk_multitb);
    if not(resolve_conflict()) then exit;
  end;
  resolve_conflicts := true;
end;










begin
end.

