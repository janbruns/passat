

{ About the clause signature, a 64/128 bit value 
  about literal occurence. Used as a filter for subsumption search.
}


procedure Tpp_solver.make_lits_signature(lits : Pliteral; len : Tliteral; var s1,s2 : qword);
var i,j,lit : Tliteral;
begin
  s1 := 0;
  s2 := 0;
  j := (8*sizeof(Pclause)-1);
  for i := len-1 downto 0 do begin
    lit := lits[i] xor (lits[i] shr 13) xor (lits[i] shr 11) xor (lits[i] shr 24);
    s1 := s1 or (1 shl (lit and j));
    lit := lit xor (lits[i] shr 1) xor (lits[i] shr 26) xor (lits[i] shr 7);
    s2 := s2 or (1 shl (lit and j));
  end;
end;

procedure Tpp_solver.make_cls_signature(cls : Pclause);
var s1,s2 : qword;
begin
  make_lits_signature(getliterals(cls),cls^.len,s1,s2);
  cls^.nxt[0] := pointer(ptruint(s1));
  cls^.nxt[1] := pointer(ptruint(s2));
end;

procedure Tpp_solver.make_allcls_signature();
var cls : Pclause;
begin
  cls := latestclause;
  while (cls<>nil) do begin
    make_cls_signature(cls);
    cls := cls^.prv;
  end;
end;

function cls_matches_sig(cls : Pclause; s1,s2 : qword) : boolean;
begin
  cls_matches_sig :=    (((qword(ptruint(cls^.nxt[0]))) and s1)=s1)
                     and(((qword(ptruint(cls^.nxt[1]))) and s2)=s2);
end;




{ Subsumption test.

  A clause C is subsumed by another clause C0,
  if C has all the literals of C0.

  cls_is_subsumed() expects then n=len literals of C0
  to be already marked as visited, and tests wether
  C has n=len matching literals.

  Duplicate literals in C or C0 would give wrong results. 
}

function Tpp_solver.cls_is_subsumed(cls : Pclause; len,s1,s2 : qword) : boolean;
var lit,k,i : Tliteral; alit : Tuliteral; lits : Pliteral;
begin
  cls_is_subsumed := false;
  if cls_matches_sig(cls,s1,s2) then begin
    k := cls^.len;
sss_work += k;
    if k<len then exit;
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      lit := lits[i];
      alit := abs(lit);
      alit := 2*alit +ord(lit<0);
      if not trkvar_visited[alit] then begin
        dec(k);
        if k<len then exit;
      end;
    end; 
    cls_is_subsumed := true;  
  end;
end;








{ Find clauses subsumed by cls0.
}

function Tpp_solver.check_for_subsumed(cls0 : Pclause) : qword;
var lit,bestlit,i : Tliteral; alit : Tuliteral; bestclc,occlcnt,n : int64; lits : Pliteral; cls2 : Pclause; ppc : PPclause;
begin
  if (cls0=nil) 
  or (cls0^.len<1) 
  or ((cls0^.flags and clausetag_pp_discard)>0) 
  then begin 
    check_for_subsumed := 0;
    exit;
  end;
  ppstrengthen.clear();

  { mark all literals of cls0 as visited. }
  lits := getLiterals(cls0);
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := true;
  end;
  { find the literal with shortest occur-list }
  bestlit := 0; 
  bestclc := 0;
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    occlcnt := lazylenOccurList(lit);
    if (bestlit=0) or (occlcnt<bestclc) then begin
      bestlit := lit;
      bestclc := occlcnt;
    end;
  end;
  sss_work += cls0^.len; 

  { search occurlist for matches }
  bestclc := lockOccurList(bestlit,ppc);
  for occlcnt := bestclc-1 downto 0 do begin
    cls2 := ppc[occlcnt];
    if (cls2<>cls0)and(cls2^.len>=cls0^.len) then begin
      if cls_is_subsumed(cls2,cls0^.len,ptruint(cls0^.nxt[0]),ptruint(cls0^.nxt[1]))
      then ppstrengthen.push(0,cls2);
    end;
  end;
  unlockOccurList(bestlit);

  { cleanup }
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;

  n := 0;
  while ppstrengthen.pop(lit,cls2) do begin
    remove_clause_from_appearlist(cls2);
    inc(n);
  end;
  check_for_subsumed := n;
end;


{ Self Subsumption (strengthening):

  Given a clause C0 and a literal choice of C0 of form

  C0 = lit | common
 
  then if there is a clause C of the form

  C = -lit | common | rest

  the Clause C can be simplified to

  C = common | rest.

  Reason:
  If common is true under some assigment, C0 and C are both satisfied.
  If common is false under some assignment, clause C0 forces lit=true, so -lit is as false as common.

  check_for_selfsubsumed() takes a clause C0, 
  and looks for clauses that would be subsumed by C0 with a single literal lit negated.  
}
function Tpp_solver.check_for_selfsubsumed(cls0 : Pclause) : qword;
var n,s1,s2 : qword; 
cnt : int64; i,lit : Tliteral; 
alit : Tuliteral; lits : Pliteral; 
cls2 : Pclause; ppc : PPclause;
begin
  if (cls0=nil) 
  or (cls0^.len<1) 
  or (cls0^.len>50)
  or ((cls0^.flags and clausetag_pp_discard)>0) 
  then begin 
    check_for_selfsubsumed := 0;
    exit;
  end;
  ppstrengthen.clear();

  { mark all literals of cls0 as visited. }
  lits := getLiterals(cls0);
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := true;
    { ... and copy all the literals to some tmp storage }
    trkvar_conflcls[i] := lit;
  end;
  sss_work += cls0^.len;

  { for any literal of cls0, check inverse lit for subsume }
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);

    { take back the literal mark, mark -literal instead }
    trkvar_visited[alit] := false;
    trkvar_visited[alit xor 1] := true;
    { update tmp storage to get a correct signature }
    trkvar_conflcls[i] := -lit;
    make_lits_signature( trkvar_conflcls, cls0^.len, s1, s2 );

    { check the clauses in -lit occur list for subsumption }
    for cnt := lockOccurList(-lit,ppc)-1 downto 0 do begin
      cls2 := ppc[cnt];
      if (cls2<>cls0)and(cls2^.len>=cls0^.len) then begin
        if cls_is_subsumed( cls2, cls0^.len, s1, s2 )
        then ppstrengthen.push(-lit,cls2);
      end;
    end;
    unlockOccurList(-lit);

    { roll back the modifications }
    trkvar_conflcls[i] := lit;
    trkvar_visited[alit xor 1] := false;
    trkvar_visited[alit] := true;
  end;

  { cleanup }
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;

  n := 0;
  while ppstrengthen.pop(lit,cls2) do begin
    strengthen(cls2,lit,cls0^.len>1);
    inc(n);
  end;
  check_for_selfsubsumed := n;
end;






function Tpp_solver.check_for_selfsubsumed_v2(cls0 : Pclause) : qword;
var n,bestclc : qword; 
cnt : int64; i,j,k,lit,bestlit,f : Tliteral; 
alit : Tuliteral; lits : Pliteral; 
cls2 : Pclause; ppc : PPclause;
begin
  if (cls0=nil) 
  or (cls0^.len<1) 
  or (cls0^.len>250)
  or ((cls0^.flags and clausetag_pp_discard)>0) 
  then begin 
    check_for_selfsubsumed_v2 := 0;
    exit;
  end;
  ppstrengthen.clear();

  { mark all literals of cls0 as visited. }
  lits := getLiterals(cls0);
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := true;
  end;

  { find the literal with shortest dbl-sided occur-list }
  bestlit := 0; 
  bestclc := 0;
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    cnt := lazylenOccurList(lit)+lazylenOccurList(-lit);
    if (bestlit=0) or (cnt<bestclc) then begin
      bestlit := lit;
      bestclc := cnt;
    end;
  end;
  sss_work += cls0^.len;


  for cnt := lockOccurList(bestlit,ppc)-1 downto 0 do begin
    cls2 := ppc[cnt];
    sss_work += cls2^.len;
    k := cls0^.len;
    j := 0;
    f := cls2^.len -k;
    if (cls0<>cls2)and(f>=0) then begin
      lits := getLiterals(cls2);
      for i := cls2^.len-1 downto 0 do begin
        lit := lits[i];
        alit := abs(lit);
        alit := 2*alit+ord(lit<0);
        if trkvar_visited[alit] then begin
          dec(k);
          if k=0 then break;
        end else if trkvar_visited[alit xor 1] then begin
          if j=0 then begin
            j := lit;
            dec(k);
            if k=0 then break;
          end else begin
            k := 1;
            break;
          end;
        end else begin
          dec(f);
          if f<0 then break;
        end;
      end;
      if k=0 then begin
        ppstrengthen.push(j,cls2);
      end;
    end;
  end;
  unlockOccurList(bestlit);
  bestlit := -bestlit;
  for cnt := lockOccurList(bestlit,ppc)-1 downto 0 do begin
    cls2 := ppc[cnt];
    sss_work += cls2^.len;
    k := cls0^.len;
    j := 0;
    f := cls2^.len -k;
    if (cls0<>cls2)and(f>=0) then begin
      lits := getLiterals(cls2);
      for i := cls2^.len-1 downto 0 do begin
        lit := lits[i];
        alit := abs(lit);
        alit := 2*alit+ord(lit<0);
        if trkvar_visited[alit] then begin
          dec(k);
          if k=0 then break;
        end else if trkvar_visited[alit xor 1] then begin
          if j=0 then begin
            j := lit;
            dec(k);
            if k=0 then break;
          end else begin
            k := 1;
            break;
          end;
        end else begin
          dec(f);
          if f<0 then break;
        end;
      end;
      if k=0 then begin
        ppstrengthen.push(j,cls2);
      end;
    end;
  end;
  unlockOccurList(bestlit);

  { cleanup }
  lits := getLiterals(cls0);
  for i := cls0^.len-1 downto 0 do begin
    lit := lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    trkvar_visited[alit] := false;
  end;

  n := 0;
  while ppstrengthen.pop(lit,cls2) do begin
    if ((cls2^.flags and clausetag_pp_discard)=0) then begin 
      if (lit=0) then begin
        remove_clause_from_appearlist(cls2);
        inc(subs_workaround);
      end else begin
        strengthen(cls2,lit,cls0^.len>1);
        inc(n);
      end;
    end;
  end;
  check_for_selfsubsumed_v2 := n;
end;








procedure Tpp_solver.check_marked_clauses_for_selfsubsumes();
var n,nc,x : qword; cls : Pclause;
begin
  x := 1;
  while (rclauselist<>nil) and keepsolving do begin
    n := 0; nc := 0; subs_workaround :=  0;
    repeat
      cls := get_cls_from_processinglist();
      if (cls<>nil) then begin
        //subs_workaround += check_for_subsumed(cls);
        n += check_for_selfsubsumed_v2(cls);
        inc(nc);
        inc(x);
      end else break;
    until not(keepsolving);
    repeat
      cls := get_cls_from_processinglist();
    until cls=nil;
    if notquiet then writeln('c subsume cycled ',nc,' clauses, found ',subs_workaround,' subsumed, ',n,' clauses strengthend.');
  end;
  rclauselist := nil;
end;




function Tpp_solver.get_cls_from_processinglist() : Pclause;
var cls : Pclause;
begin
  cls := rclauselist;
  if (cls<>nil) then begin
    rclauselist := cls^.rnxt;
    cls^.flags := cls^.flags and not(clausetag_visited);
  end;
  get_cls_from_processinglist := cls;
end;


procedure Tpp_solver.add_cls_to_processinglist(cls : Pclause);
begin
  if (cls^.flags and clausetag_visited)=0 then begin
    cls^.flags := cls^.flags or clausetag_visited;
    cls^.rnxt := rclauselist;
    rclauselist := cls;
  end;
end;

procedure Tpp_solver.add_all_cls_to_processinglist();
var cls : Pclause;
begin
  cls := latestclause;
  while (cls<>nil) do begin
    add_cls_to_processinglist(cls);
    cls := cls^.prv;
  end;
end;




