{$mode objfpc}
unit loop_solver;

{$include config.pas}


interface
uses pp_solver,clause;

type
Tloop_solver = class(Tpp_solver)
  public
  constructor create();
  procedure choose_to_unsat();
  function do_solvecycles() : longint;
  procedure onAfterLoad(); override;
  procedure onStartSolveCycling(); override;







  protected
  procedure update_restart_intervall();

  function search(n : int64) : longint;

  function varwlen(lit : Tliteral) : Tuliteral;

  private
  procedure push_deletable(cls : Pclause);

  function get_deletable_clauses() : int64;
  function get_subsequent_deletable_clauses() : int64;

  procedure compute_deletable_clause_scores(n : int64);
  procedure qsort_deletable_clauses(a,b : int64);
  procedure do_delete_clause(cls : Pclause; push_linkless_only : boolean);
  procedure fwd_prpcnt(cls : Pclause; push_linkless_only : boolean);

  function do_clause_deletion(todel : int64; subseq : boolean) : int64;
  function do_ll_clause_deletion() : int64;

  function unlink_some_learnts(x : double) : int64;



  public
  restart_intervall : qword;
  loop_multitb : qword;
  loop_tb : qword;

  cls_del_probab : double;
  target_del_amount : double;
  delprobab_adj,delprobab_adj_adj,
  max_cls_del_cycles : double;
  luby_u,luby_v : int64;
  freshvar_handlingcounter, freshvar_handlingintervall : longint;

  rst_lubyscale, rst_internal : longint;


  cls_ignore_activity,
  cls_ignore_what : double;

  rmclarr : array of Pclause;
  deletables : int64;
  total_deleted_learnts : qword;

  min_cls_del_success : longint;
  cls_del_keep_fract : double;

  print_cls_mng : boolean;
end;




implementation
uses print,bare_solve_struct,sysutils;


const min_cls_del_score = -10000;



constructor Tloop_solver.create();
begin
  inherited create();
  restart_intervall := 10;

  cls_del_probab := 0.15;
  target_del_amount := 0.90;
  delprobab_adj := 1.05;
  delprobab_adj_adj := 0.99;
  loop_multitb := 0;
  loop_tb := 0;
  luby_u := 1; luby_v := 1;
  freshvar_handlingcounter := 0;
  freshvar_handlingintervall := 10;
  max_cls_del_cycles := 2;

  cls_ignore_activity := 0.51;
  cls_ignore_what := 0.01;

  print_cls_mng := false;
  rst_lubyscale := 157;
  rst_internal := 10;

  min_cls_del_success := 33;
  cls_del_keep_fract := 0.05;
end;

procedure Tloop_solver.onAfterLoad();
begin
  inherited onAfterLoad();
  check_for_satisfied_clauses();
  unlink_deleted_clauses();
  do_freshknownvars_cleanups(false);
  bcp();
end;




procedure lubynxt(var u,v : int64);
begin
  if (u and (-u))=v then begin
    u := u+1;
    v := 1;
  end else begin
    v := 2*v;
  end;
end;


procedure Tloop_solver.push_deletable(cls : Pclause);
var l : int64;
begin
  while deletables>high(rmclarr) do begin
    l := high(rmclarr)+1;
    if l=0 then l := 1;
    l := l*2;
    setlength(rmclarr,l);
  end;
  rmclarr[deletables] := cls;
  inc(deletables);
end;


{ collect a list of clauses that could be deleted }

function Tloop_solver.get_deletable_clauses() : int64;
var cls : Pclause; flg,tag : dword;
begin
  lazy_mark_varreasons_as_clause_ref();
  deletables := 0;
  tag := clausetag_learnt or clausetag_deleted; 
  cls := latestclause;
  while (cls<>nil) do begin
    flg := cls^.flags;
    if ((flg and tag)=clausetag_learnt) and (cls^.refcnt=0) then begin
      push_deletable(cls);
    end;
    if (flg and clausetag_learnt)=0 then break;
    cls := cls^.prv;
  end;
  get_deletable_clauses := deletables;
end;

function Tloop_solver.get_subsequent_deletable_clauses() : int64;
var cls : Pclause; i,j : int64;
begin
  i := 0; 
  j := 0;
  while i<deletables do begin
    cls := rmclarr[i];
    if (cls<>nil) then begin
      rmclarr[j] := cls;
      inc(j);
    end;
    inc(i);
  end;
  deletables := j;
  get_subsequent_deletable_clauses := j;
end;



{ calc some score for later decision about clause deletion }
procedure Tloop_solver.compute_deletable_clause_scores(n : int64);
var cls : Pclause; q : int64; x : Tuliteral; score : single; tag : dword;
begin
  tag := clausetag_linkless;
  q := 0;
  while q<n do begin
    cls := rmclarr[q];
    x := getPropCount(cls);
    score := cls^.len -0.1*1.443*ln(x+1);
    score := (score+10)*((cls^.touched+1.0) / (cls^.propagated+1.0)); 

    { linkless learnts don't add sense anymore... }
    if (cls^.flags and tag)=tag then score += 1.0e9;
    Psingle(@cls^.rnxt)^ := score;
    inc(q);
  end;
end;


procedure Tloop_solver.qsort_deletable_clauses(a,b : int64);
var ref,c,d : int64; cls : Pclause; rv : single; 
begin
  repeat
    c := a;
    d := b;
    ref := (a+b) div 2;
    repeat
      rv := Psingle(@rmclarr[ref]^.rnxt)^;
      while ( rv < Psingle(@rmclarr[c]^.rnxt)^ ) do inc(c);
      while ( Psingle(@rmclarr[d]^.rnxt)^ < rv ) do dec(d);
      if (c<=d) then begin
        cls := rmclarr[c];
        rmclarr[c] := rmclarr[d];
        rmclarr[d] := cls;
        if (ref=c) then ref := d 
        else if (ref=d) then ref := c;
        inc(c);
        dec(d);
      end;
    until (c>d);
    if (a<d) then qsort_deletable_clauses(a,d);
    a := c;
  until (c>=b);
end;

procedure Tloop_solver.fwd_prpcnt(cls : Pclause; push_linkless_only : boolean);
var cl : PPclause; i,x,len : Tuliteral; cls2 : Pclause; tag : dword;
begin
  tag := clausetag_deleted or clausetag_learnt;
  cl := cl_spool.get_cls_extra(cls,len);
  x := getPropCount(cls)+1;
  i := 0;
  while (i<len) do begin
    cls2 := cl[i];
    setPropCount(cls2,x+getPropCount(cls2));
    if (cls2^.refcnt=0) then raise Texc_trk.create('zero_refcnt_error') else dec(cls2^.refcnt);
    if (cls2^.refcnt=0) then begin
      if (cls2^.flags and tag)=clausetag_learnt then begin
        if push_linkless_only then begin
          if (cls2^.flags and clausetag_linkless)>0 then begin
            push_deletable(cls2);
          end;
        end else push_deletable(cls2);
      end;
    end;
    inc(i);
  end;
end;



procedure Tloop_solver.do_delete_clause(cls : Pclause; push_linkless_only : boolean);
var flg : Tuliteral;
begin
  flg := cls^.flags;
  if (flg and clausetag_deleted)=0 then begin
    flg := cls^.flags or clausetag_deleted;
    cls^.flags := flg;
    inc(total_deleted_learnts);
    fwd_prpcnt(cls,push_linkless_only);
  end;
end;


function Tloop_solver.do_clause_deletion(todel : int64; subseq : boolean) : int64;
var n,i : int64; cls : Pclause;
begin
  do_clause_deletion := 0;
  if (todel<=0) and subseq then begin
    if print_cls_mng then writeln();
    exit;
  end;
  if print_cls_mng then write('learnts=',trk_learnt_clauses,' ');
  if print_cls_mng then write('gone=',total_deleted_learnts,' ');

  if print_cls_mng then write('todel=',todel,' ');
  if subseq 
  then n := get_subsequent_deletable_clauses()
  else n := get_deletable_clauses();
  if print_cls_mng then write('deletable=',n,' ');

  i := (n+1) div 2;
  if (todel>i) then todel := i;
  if todel<=0 then todel := 1;

  compute_deletable_clause_scores(n);
  if (n>1) then qsort_deletable_clauses(0,n-1);
  i := 0; 
  while (i<n)and(todel>0) do begin
    cls := rmclarr[i];
    rmclarr[i] := nil;
    do_delete_clause(cls,false);
    { cls still a valid pointer here.
      do not count linkless clauses as fulfilled deletion here,
    }
    if not((cls^.flags and clausetag_linkless)>0) 
    then dec(todel);
    inc(i);
  end;
  if print_cls_mng then write('deleted=',i,' ');
  if print_cls_mng then writeln('remain=',trk_learnt_clauses-total_deleted_learnts,' ');
  do_clause_deletion := i;
end;

{ after do_clause_deletion(),
  do_ll_clause_deletion() is used to delete all deletebale linkless clauses. 
}
function Tloop_solver.do_ll_clause_deletion() : int64;
var n,i,j : int64; cls : Pclause;
begin
  do_ll_clause_deletion := 0;
  n := get_subsequent_deletable_clauses();
  i := 0;
  j := 0;
  while (i<n) do begin
    cls := rmclarr[i];
    rmclarr[i] := nil;
    if ((cls^.flags and clausetag_linkless)>0) then begin
      do_delete_clause(cls,true);
      inc(j);
    end;
    inc(i);
  end;
  do_ll_clause_deletion := j;
end;








{ Decide about intentionally making learnts ignored.

  After some time of solving, many learnts may get superseeded
  by other clauses in a way, that they can't contribute
  much propagation anymore, but still often get touched during bcp.
 
  I expect better solving times at some expense of memory usage
  and unsat-cert size. 
}
function Tloop_solver.unlink_some_learnts(x : double) : int64;
var cls : Pclause; n : int64; flg,tag : dword; q : double;
begin
  tag := clausetag_learnt or clausetag_pp_del or clausetag_linkless;
  n := 0;
  cls := latestclause;
  while (cls<>nil) do begin
    flg := cls^.flags;
    if ((flg and tag)=clausetag_learnt) then begin
      q := (cls^.propagated+cls_ignore_activity) / (cls^.touched+cls_ignore_activity); 
      if (q<cls_ignore_what*x) then begin
        cls^.flags := cls^.flags or tag;
        inc(n);
      end else begin
        cls^.propagated := cls^.propagated shr 1;
        cls^.touched    := cls^.touched    shr 1;
      end;
    end else if ((flg and tag)=tag) then begin
      inc(n);
    end;
    if (flg and clausetag_learnt)=0 then break;
    cls := cls^.prv;
  end;
  unlink_some_learnts := n;
end;













procedure Tloop_solver.choose_to_unsat();
var c : Tliteral;
begin
  c := 0;
  while not inConflict() do begin
    c := choose();
    if (c<>0) then apply_decision(c) else break;
  end;
  choose_enterhint := false;
end;

procedure Tloop_solver.update_restart_intervall();
begin
  if (restart_intervall<1000) then restart_intervall += 20
  else if (restart_intervall<10000) then restart_intervall += 150
  else restart_intervall += 250;
end;

procedure Tloop_solver.onStartSolveCycling();
begin
  choose_enterhint := true;
  inherited onStartSolveCycling();
end;








function Tloop_solver.search(n : int64) : longint;
var mtb : int64;
begin
  onStartSolveCycling();
  mtb := 1;
  while (n>0) and keepsolving do begin
    trk_multitb := 0;
    choose_to_unsat();
    if inConflict() then begin
      if not resolve_conflicts() then begin
        search := -1;
        exit;
      end else begin
      end;
    end else begin
      search := 1;
      exit;
    end;
    loop_multitb += trk_multitb;
    loop_tb += 1;
    if (trk_multitb>mtb) then begin
      n += 33*(trk_multitb-mtb);
      mtb := trk_multitb;
    end;
    n := n-1;
  end;
  search := 0;
  takeback_to_decilevel(0);
end;


function chainlen(cls : Pclause; c : longint) : qword;
var q : qword;
begin
  q := 0;
  while (cls<>nil) do begin
    inc(q);
    cls := cls^.nxt[c];
  end;
  chainlen := q;
end;

function Tloop_solver.varwlen(lit : Tliteral) : Tuliteral;
var idx : qword;
begin
  idx := abs(lit);
  idx := 4*idx+ord(lit<0);
  varwlen := chainlen(varwatches[idx],0)+chainlen(varwatches[idx+2],1);
end;





function Tloop_solver.do_solvecycles() : longint;
var n : qword; q,q2,fract_learntwork,tim : double; i,kv : longint; linkless : int64;
begin
  do_solvecycles := 0;

  for n := 1 to rst_internal do begin
    lubynxt(luby_u,luby_v);
    i := search(luby_v*rst_lubyscale);
    if (i<>0) or not(keepsolving) then begin
      do_solvecycles := i;
      exit;
    end;
  end;

  if bcp_clsitemsteps>0 then q := bcp_learnt_clsitemsteps/bcp_clsitemsteps else q := 0;

  fract_learntwork := q;

  if notquiet then writeln;


  unlink_all_cls();
  i := 0;
  repeat
    q2 := q*cls_del_keep_fract*trk_learnt_clauses + (1-q)*trk_learnt_clauses;
    q2 := (trk_learnt_clauses-total_deleted_learnts)-q2;
    inc(i);
    if print_cls_mng then write('c ');
  until (do_clause_deletion(round(q2),i>1)<=min_cls_del_success) or (i>max_cls_del_cycles);
  while do_ll_clause_deletion()>0 do;
  release_deleted_clauses();

  linkless := unlink_some_learnts(fract_learntwork);
  if print_cls_mng then writeln('c ',linkless,' linkless');
  link_all_cls(false);




  bcp_learnt_clsitemsteps := bcp_learnt_clsitemsteps shr 1;
  bcp_clsitemsteps := bcp_clsitemsteps shr 1;

  do_solvecycles := 0;

  q := (total_deleted_learnts)/(1+trk_learnt_clauses);

  update_restart_intervall();
  calc_avg_clauselen(clausetag_learnt);
  kv := 0;
  for i := num_vars downto 1 do begin
    if varstate[i]<>0 then inc(kv);
  end;

  q2 := (trk_learnt_clauses-total_deleted_learnts);
  if (q2>1) then q2 := linkless/q2 else q2 := 0;
  tim := (now()-starttime)*3600*24;
  if notquiet then write('c ',int4str(trk_learnt_clauses),' bt, ',int4str(total_deleted_learnts),' rmvd (',round(100*q):2,'%), ',int4str(trk_learnt_clauses-total_deleted_learnts),' in db (',round(100*q2):2,'% ll) l=',flt5str(avg_cls_len),', ',kv,' var,',round(fract_learntwork*100):3,'%lwrk, ',round(tim),'s');
end;

begin
end.