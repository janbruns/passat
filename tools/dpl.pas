{ Simple bare DPL-based SAT solver.

  It cannot solve many problems.

  From the historic dimacs files, it can solve most of the
  graph coloring instances (like flat200), not taking the larger gcp instances into account, 
  some instances from bejing, pret, some Latin square (qg)...
  
  Pigeon hole instances: hole10.cnf in 30s. 

  Things it can do better than many CDCL-based solver:
  - some sgen1 instances from SAT09

}

uses cnf_read,sysutils;

type
Ttotlit = int64;
Tliteral = longint;
Tclsidx = longint;
Pliteral = ^Tliteral;
Pclause = ^Tclause;
Tclause = packed record
  len,rm : Tliteral;
end;
Ttbinfo = record
  modinfocnt : Ttotlit;
  clscnt : Tclsidx;
  choice : Tliteral;
  assigncnt : Tliteral;
  times : Tliteral;
end;


var
clauses,modinfo : array of Pclause;
clscnt, initialclscnt : Tclsidx;
modinfocnt : Ttotlit;
num_vars : Tliteral;
litoccur,nlitoccur : array of Ttotlit;
tbinfo : array of Ttbinfo;
tbcnt : Ttotlit;

assignments : array of Tliteral; // used for solution printing
assigncnt : Tliteral;
decisionsmade, bcktracksmade : int64;
starttime : double;

function createClause(len : Tliteral) : Pclause;
var cls : Pclause;
begin
  getmem(cls,(len+2)*sizeof(Tliteral));
  cls^.len := len;
  cls^.rm := 0;
  createClause := cls;
end;

procedure releaseClause(cls : Pclause);
begin
  freemem(cls,(2+cls^.len+cls^.rm)*sizeof(Tliteral));
end;

function getLiterals(cls : Pclause) : Pliteral;
begin
  getLiterals := @pbyte(cls)[sizeof(Tclause)];
end;

procedure takeclause(const cf : Tcnfload; idx : longint);
var clen,i : Tliteral; cls : Pclause; lits : Pliteral;
begin
  clen := idx;
  while not(cf.ia[clen]=0) do begin
    inc(clen);
  end;
  clen := clen-idx;
  if (clen<1) then exit;
  cls := createClause(clen);
  clauses[clscnt] := cls;
  inc(clscnt);
  lits := getLiterals(cls);
  for i := clen-1 downto 0 do begin
    lits[i] := cf.ia[idx+i];
  end;
end;

const
READ_ONLY = 0;
WRITE_ONLY = 1;
READ_WRITE = 2;

procedure read_cnf(probfn : ansistring);
var cf : Tcnfload; i,j : Ttotlit; 
begin
  clscnt := 0;
  filemode := READ_ONLY;
  if (copy(probfn,1,7)='file://') then delete(probfn,1,7);
  if probfn='' then begin
    writeln('c missing filename');
    halt(0);
  end;
  writeln('c loading file ',probfn);
  cf.force_maxsat := false;
  cf.quiet := false;
  readcnf(probfn,cf);
  setlength(clauses,high(cf.cidx)+1);
  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then takeclause(cf,j);
    end;
  end;
  initialclscnt := clscnt;
  writeln('c loaded ',clscnt,' clauses');
  { count the total amount of literals }
  j := 0;
  for i := clscnt-1 downto 0 do begin
    j := j+clauses[i]^.len;
  end;
  writeln('c total literal count: ',j);
  setlength(modinfo,j);
  num_vars := cf.mv;
  setlength(litoccur,num_vars+1);
  setlength(nlitoccur,num_vars+1);
  setlength(tbinfo,num_vars+1);
  setlength(assignments,num_vars+1);
end;

procedure unloadCnf();
var i : Tclsidx;
begin
  for i := initialclscnt-1 downto 0 do begin
    releaseClause(clauses[i]);
  end;
  clscnt := 0;
  initialclscnt := 0;
  setlength(clauses,0);
end;

procedure disable_satisfied_clause(clsidx : Tclsidx);
var cls : Pclause;
begin
  cls := clauses[clsidx];
  clauses[clsidx] := clauses[clscnt-1];
  clauses[clscnt-1] := cls;
  dec(clscnt);
end;

procedure printcls(cls : Pclause);
var i : Tliteral; lits : Pliteral;
begin
  lits := getLiterals(cls);
  for i := 0 to (cls^.len+cls^.rm)-1 do begin
    write(lits[i]);
    if i<cls^.len then write(' ') else write('* ');
  end;
  writeln;
end;

procedure store_modinfo(cls : Pclause);
begin
  modinfo[modinfocnt] := cls;
  inc(modinfocnt);
end;

procedure add_assignment(v : Tliteral); // for sat cert printing
begin
  if (assigncnt>high(assignments)) then begin
    writeln('assignments array out of range: ',assigncnt);
    halt(0);
  end;
  assignments[assigncnt] := v; 
  inc(assigncnt);
end;

function apply_unit_to_cls(v : Tliteral; clsidx : Tclsidx) : boolean;
var i : Tliteral; lits : Pliteral; cls : Pclause; sat : boolean;
begin
  cls := clauses[clsidx];
  lits := getLiterals(cls);
  sat := false;
  for i := cls^.len-1 downto 0 do begin
    if (lits[i]=v) then sat := true;
  end;
  if sat then begin
    disable_satisfied_clause(clsidx);
  end else begin
    for i := cls^.len-1 downto 0 do begin
      if (lits[i]=-v) then begin
        lits[i] := lits[cls^.len-1];
        lits[cls^.len-1] := -v;
        inc(cls^.rm);
        dec(cls^.len);
        store_modinfo(cls);
      end;
    end;
  end;
  apply_unit_to_cls := (cls^.len>0);
end;

procedure undo_unitapply_to_cls(cls : Pclause);
begin
  inc(cls^.len);
  dec(cls^.rm);
end;

procedure undo_clsmod();
begin
  dec(modinfocnt);
  undo_unitapply_to_cls(modinfo[modinfocnt]);
end;

procedure undo_clsmods(n : Ttotlit);
begin
  while (modinfocnt>n) do undo_clsmod();
end;

function apply_unit_to_clauses(v : Tliteral) : boolean;
var i : Tclsidx;
begin
  add_assignment(v);
  i := 0;
  for i := clscnt-1 downto 0 do begin
    if not apply_unit_to_cls(v,i) then begin
      apply_unit_to_clauses := false;
      exit;
    end;
  end;
  apply_unit_to_clauses := true;
end;

function get_unit_clause(var cls : Pclause) : boolean;
var i : Tclsidx;
begin
  cls := nil;
  for i := clscnt-1 downto 0 do begin
    if clauses[i]^.len=1 then begin
      cls := clauses[i];
      get_unit_clause := true;
      exit;
    end;
  end;
  get_unit_clause := false;
end;

function do_bcp() : boolean;
var cls : PClause; lits : Pliteral; 
begin
  while get_unit_clause(cls) do begin
    lits := getLiterals(cls);
    if not apply_unit_to_clauses(lits[0]) then begin
      do_bcp := false;
      exit;
    end;
  end;
  do_bcp := true;
end;

function decide() : Tliteral;
var i : Tliteral; j : Tclsidx; m,x : Ttotlit; lits : Pliteral; cls : Pclause;
begin
  inc(decisionsmade);
  for i := num_vars downto 1 do begin
    litoccur[i] := 0;
    nlitoccur[i] := 0;
  end;
  for j := clscnt-1 downto 0 do begin
    cls := clauses[j];
    lits := getLiterals(cls);
    for i := cls^.len-1 downto 0 do begin
      if lits[i]>=0 
      then inc( litoccur[ lits[i]])
      else inc(nlitoccur[-lits[i]]);
    end;
  end;
  m := 0; j := 0;
  for i := num_vars downto 1 do begin
    x := litoccur[i] + nlitoccur[i];
    if (x>m) then begin
      m := x;
      j := i;
      if litoccur[i]<nlitoccur[i] then j := -i;
    end;
  end;
  decide := j;
end;

function takeback() : boolean;
begin
  repeat
    inc(bcktracksmade);
    dec(tbcnt);
    if (tbcnt>=0) then begin
      if tbinfo[tbcnt].times=0 then begin
        clscnt := tbinfo[tbcnt].clscnt;
        assigncnt := tbinfo[tbcnt].assigncnt;
        undo_clsmods(tbinfo[tbcnt].modinfocnt);
        inc(tbinfo[tbcnt].times); //clscnt := -tbinfo[tbcnt].clscnt;
        tbinfo[tbcnt].choice := -tbinfo[tbcnt].choice;
        if apply_unit_to_clauses(tbinfo[tbcnt].choice) then begin
          if do_bcp() then begin
            inc(tbcnt);
            takeback := true;
            exit;
          end;
        end;
      end;
    end else begin
      takeback := false;
      exit;
    end;
  until false;
end;

function deeper() : boolean;
begin
  if clscnt>0 then begin
    tbinfo[tbcnt].modinfocnt := modinfocnt;
    tbinfo[tbcnt].clscnt := clscnt;
    tbinfo[tbcnt].choice := decide();
    tbinfo[tbcnt].times := 0;
    tbinfo[tbcnt].assigncnt := assigncnt; // for sat-cert printing
    inc(tbcnt);
    deeper := true;
  end else deeper := false;
end;

procedure print_solution();
var i : Tliteral;
begin
  for i := assigncnt-1 downto 0 do begin
    write('v ',assignments[i]);
    writeln;
  end;
  writeln('v 0');
end;

procedure solve();
var noconflict : boolean;
begin
  repeat
    if deeper() then begin
      noconflict := apply_unit_to_clauses(tbinfo[tbcnt-1].choice);
      if noconflict then noconflict := do_bcp();
      if not noconflict then begin
        if not takeback() then begin
          writeln('s UNSATISFIABLE');
          exit;
        end;
      end;
    end else begin
      writeln('s SATISFIABLE');
      //print_solution();
      exit;
    end;
  until false;
end;


begin
  modinfocnt := 0;
  tbcnt := 0;
  assigncnt := 0;
  decisionsmade := 0;
  bcktracksmade := 0;
  starttime := now();
  read_cnf(paramstr(1));
  solve();
  writeln('c decisions : ',decisionsmade);
  writeln('c backtracks: ',bcktracksmade);
  writeln('c time: ',(now()-starttime)*3600*24:0:2,' s');
  unloadCnf();
end.