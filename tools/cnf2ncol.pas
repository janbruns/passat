uses cnf_read;

type
Tliteral = longint;

const
READ_ONLY = 0;
WRITE_ONLY = 1;
READ_WRITE = 2;

var 
varnames : array of Tliteral;
curcls,nxtcls : array of Tliteral;
maxclslen,curclslen : longint;
basenode,truenode,falsenode,nodecnt : int64;


procedure make_edge(a,b : int64);
begin
  writeln(a,' ',b);
end;

procedure make_tri(a,b,c : int64);
begin
  make_edge(a,b);
  make_edge(a,c);
  make_edge(b,c);
end;


procedure countclause(const cf : Tcnfload; idx : longint);
var clen,i,j,k : Tliteral; 
begin
  clen := idx;
  while not(cf.ia[clen]=0) do begin
    inc(clen);
  end;
  clen := clen-idx;
  if clen>maxclslen then maxclslen := clen;
  if (clen<1) then exit;
  for i := 0 to clen-1 do begin
    j := cf.ia[idx+i];
    inc(varnames[abs(j)]);
  end;
end;


function work_cls_cycle() : boolean;
var i,j,n1 : int64;
begin
  j := 0;
  if curclslen=1 then begin
    make_tri(basenode,falsenode,curcls[0]);
    work_cls_cycle := false;
  end else begin
    work_cls_cycle := true;
    i := 0;
    while (i<curclslen) do begin
      if (i+1=curclslen) then begin
        nxtcls[j] := curcls[i];
        inc(j);
        inc(i);
      end else begin
        n1 := nodecnt;
        inc(nodecnt,3);
        make_tri(n1,n1+1,n1+2);
        make_edge(curcls[i],n1);
        make_edge(curcls[i+1],n1+1);
        nxtcls[j] := n1+2;
        inc(i,2);
        inc(j);
      end;
    end;
    move(nxtcls[0],curcls[0],j*sizeof(curcls[0]));
    curclslen := j;
  end;
end;


procedure makeclause(const cf : Tcnfload; idx : longint);
var clen,i,j,k : Tliteral; 
begin
  clen := idx;
  while not(cf.ia[clen]=0) do begin
    inc(clen);
  end;
  clen := clen-idx;
  if (clen<1) then exit;
  write('c ');
  for i := 0 to clen-1 do begin
    j := cf.ia[idx+i];
    write(j,' ');
    curcls[i] := varnames[abs(j)];
    if (j<0) then inc(curcls[i]);
  end;
  writeln;
  curclslen := clen;
  while work_cls_cycle() do;
end;

var
cf : Tcnfload;
probfn : ansistring;
i,j,cc : int64;



begin
  maxclslen := 0;

  filemode := READ_ONLY;
  probfn := paramstr(1);
  if (copy(probfn,1,7)='file://') then delete(probfn,1,7);
  fillbyte(cf,sizeof(cf),0);
  cf.force_maxsat := false;
  cf.quiet := true;
  readcnf(probfn,cf);

  setlength(varnames,cf.mv+1);
  for i := 0 to cf.mv do varnames[i] := 0;

  cc := 0;
  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then countclause(cf,j);
    end;
    inc(cc);
  end;

  setlength(curcls,maxclslen);
  setlength(nxtcls,maxclslen);

  nodecnt := 4;
  basenode := 1;
  truenode := 2;
  falsenode := 3;

  make_tri(basenode,truenode,falsenode);

  for i := 0 to cf.mv do begin
    if varnames[i]>0 then begin
      varnames[i] := nodecnt;
      make_tri(basenode,nodecnt,nodecnt+1);
      inc(nodecnt,2);
    end;
  end;

  cc := 0;
  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then makeclause(cf,j);
    end;
    inc(cc);
  end;


end.