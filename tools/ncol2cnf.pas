program ncol2cnf;
{$mode objfpc}


type
Tnodeidx = dword;
Tedge = record
  a,b : Tnodeidx;
end;
Tnode = class

  constructor create(x : int64);
  procedure incec();
  procedure setec();
  procedure takeedge(i : int64);
  function assign_cnfvar(n,c : int64) : int64; 
  function do_cnfrule(c : longint; prnt : boolean) : int64;

  public
  idx,edgecnt,firstvar : int64;
  edg : array of Tedge;
  used : boolean;
  
end;



function is_wschar(c : char) : boolean;
begin
  is_wschar := false;
  case c of
    ' ',#13,#10,#08 : is_wschar := true;
  end;
end;

procedure trimstr(var s : ansistring);
begin
  while (length(s)>0) and is_wschar(s[1]) do delete(s,1,1);
  while (length(s)>0) and is_wschar(s[length(s)]) do delete(s,length(s),1);
end;


function parse_line(s : ansistring; var e : Tedge) : boolean;
var s1,s2 : ansistring; w : word; i : int64;
begin
  parse_line := false;
  trimstr(s);
  i := 1;
  while (i<=length(s)) and (not( is_wschar(s[i]))) do inc(i);
  s1 := copy(s,1,i-1);
  delete(s,1,i);
  trimstr(s);
  i := 1;
  while (i<=length(s)) and (not( is_wschar(s[i]))) do inc(i);
  s2 := copy(s,1,i-1);
  delete(s,1,i);
  trimstr(s);
  if s='' then begin
    val(s1,e.a,w);
    if w=0 then begin
      val(s2,e.b,w);
      if w=0 then begin
        parse_line := true;
      end;
    end;
  end;
end;




var
ec,nsat : int64;
edges : array of Tedge;
nodes : array of Tnode;
maxnode : Tnodeidx;




constructor TNode.create(x : int64);
begin
  inherited create();
  idx := x;
  edgecnt := 0;
  setlength(edg,0);
end;

procedure TNode.incec();
begin
  inc(edgecnt);
end;

procedure TNode.setec();
begin
  setlength(edg,edgecnt);
  edgecnt := 0;
end;

procedure TNode.takeedge(i : int64);
begin
  edg[edgecnt] := edges[i];
  inc(edgecnt);
end;

function TNode.assign_cnfvar(n,c : int64) : int64; 
begin
  firstvar := n;
  if edgecnt>0 then begin
    used := true;
    assign_cnfvar := n+c;
  end else begin
    assign_cnfvar := n;
    used := false;
  end;
end;

function TNode.do_cnfrule(c : longint; prnt : boolean) : int64;
var n,i,j : int64;
begin
  n := 0;
  if used then begin
    dec(c);

    // at least one of the colors rule
    inc(n);
    if prnt then begin
      for i := 0 to c do begin
        write(firstvar+i,' '); 
      end;
      writeln('0');
    end;

    // at most one of the colors rules
    for i := 0 to c do begin
      for j := i+1 to c do begin
        inc(n);
        if prnt then writeln(-(firstvar+i),' ',-(firstvar+j),' 0');
      end;
    end;

    // not the same color on edge-endpoints rules
    for i := 0 to edgecnt-1 do begin
      for j := 0 to c do begin
        if (idx<edg[i].a) then begin
          inc(n);
          if prnt then writeln(-(firstvar+j),' ',-(nodes[edg[i].a].firstvar+j),' 0');
        end;
        if (idx<edg[i].b) then begin
          inc(n);
          if prnt then writeln(-(firstvar+j),' ',-(nodes[edg[i].b].firstvar+j),' 0');
        end;
      end;
    end;

  end;
  do_cnfrule := n;
end; 






procedure readfile(fn : ansistring);
var e : Tedge; f : TEXT; s : ansistring; lnum : int64;
begin
  assign(f,fn);
  reset(f);
  lnum := 1;
  ec := 0;
  setlength(edges,16);
  while not(eof(f)) do begin
    readln(f,s);
    if length(s)>0 then begin
      case s[1] of
        'a'..'z','A'..'Z','#' : s := '';
      else
        if parse_line(s,e) then begin
          if ec>high(edges) then begin
            setlength(edges,2*(ec+1));
          end;
          if ec>high(edges) then begin
            writeln('internal error');
          end;
          edges[ec] := e;
          inc(ec);
        end else begin
          writeln('error in line ',lnum,':');
          writeln(s);
          halt(1);
        end;
      end;
    end;
  end;
end;

procedure get_maxnode();
var i : int64; 
begin
  i := ec;
  maxnode := 0;
  for i := i-1 downto 0 do begin
    if edges[i].a>maxnode then maxnode := edges[i].a;
    if edges[i].b>maxnode then maxnode := edges[i].b;
  end;
end;

var w : word;
i,curcnfvar,curcnfrule : int64;

begin
  nsat := 0;
  if paramcount=2 then val(paramstr(1),nsat,w)
  else begin
    writeln('convert edgelist to nsat. usage: ncol2cnf ncol filename');
    halt(1);
  end;
  if (w=0)and(nsat>1) then begin
    readfile(paramstr(2));
    if (ec>0) then begin
      get_maxnode();
      setlength(nodes,maxnode+1);
      i := maxnode;
      for i := i downto 0 do nodes[i] := Tnode.create(i);
      i := ec;
      for i := i-1 downto 0 do begin
        nodes[edges[i].a].incec();
        nodes[edges[i].b].incec();
      end;
      i := maxnode;
      for i := i downto 0 do begin
        nodes[i].setec();
      end;
      i := ec;
      for i := i-1 downto 0 do begin
        nodes[edges[i].a].takeedge(i);
        nodes[edges[i].b].takeedge(i);
      end;
      curcnfvar := 1;
      i := 0;
      while (i<=maxnode) do begin
        curcnfvar := nodes[i].assign_cnfvar(curcnfvar,nsat); 
        inc(i);
      end;
      curcnfrule := 0;

      for i := 0 to ec-1 do begin
writeln('c edge ',i,': ',edges[i].a,'@',nodes[edges[i].a].firstvar,' <-> ',edges[i].b,'@',nodes[edges[i].b].firstvar);
      end;


      i := 0;
      while (i<=maxnode) do begin
        curcnfrule += nodes[i].do_cnfrule(nsat,false); 
        inc(i);
      end;

      writeln('p cnf ',curcnfvar-1,' ',curcnfrule);

      i := 0;
      curcnfrule := 0;
      while (i<=maxnode) do begin
        curcnfrule += nodes[i].do_cnfrule(nsat,true); 
        inc(i);
      end;

    end else  writeln('no edges in file');
  end else writeln('error with parseint arg 1');
end.


