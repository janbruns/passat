uses cnf_read;

type
Tliteral = longint;

const
READ_ONLY = 0;
WRITE_ONLY = 1;
READ_WRITE = 2;

var varnames : array of Tliteral;

procedure takeclause(const cf : Tcnfload; idx : longint);
var clen,i,j,k : Tliteral; 
begin
  clen := idx;
  while not(cf.ia[clen]=0) do begin
    inc(clen);
  end;
  clen := clen-idx;
  if (clen<1) then exit;
  for i := 0 to clen-1 do begin
    j := cf.ia[idx+i];
    k := varnames[abs(j)];
    if (j<0) then k := -k;
    write(k,' ');
  end;
  writeln('0');
end;


var
cf : Tcnfload;
probfn : ansistring;
i,j,cc : int64;




begin
  randomize();
  filemode := READ_ONLY;
  probfn := paramstr(1);
  if (copy(probfn,1,7)='file://') then delete(probfn,1,7);
  fillbyte(cf,sizeof(cf),0);
  cf.force_maxsat := false;
  cf.quiet := true;
  readcnf(probfn,cf);

  setlength(varnames,cf.mv+1);
  for i := 0 to cf.mv do varnames[i] := i;
  for i := 1 to cf.mv do begin
    j := random(cf.mv)+1;
    cc := varnames[i];
    varnames[i] := varnames[j];
    varnames[j] := cc;
  end;
  for i := 1 to cf.mv do begin
    if random()<0.5 then varnames[i] := -varnames[i];
  end;

  // shuffle clauses
  for i := 0 to high(cf.cidx) do begin
    j := random(high(cf.cidx)+1);
    cc := cf.cidx[i];
    cf.cidx[i] := cf.cidx[j];
    cf.cidx[j] := cc;
  end;

  cc := 0;
  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then inc(cc);
    end;
  end;

  writeln('p cnf ',cf.mv,' ',cc);
  cc := 0;
  for i := 0 to high(cf.cidx) do begin
    j := cf.cidx[i];
    if (j<=high(cf.ia)) then begin
      if not(cf.ia[j]=0) then takeclause(cf,j);
    end;
    inc(cc);
  end;
end.