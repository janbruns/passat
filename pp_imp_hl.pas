



procedure Tpp_solver.pp_hle_add(elit : Tliteral);
var j,k,lit : Tliteral; ealit,alit : Tuliteral; lits : Pliteral;
begin
  j := pp_hllen;
  ealit := abs(elit);
  if (ealit>num_vars)or(ealit=0) then raise Texc_trk.create('hle_get bounds check 1 fail');
  ealit := 2*ealit +ord(elit<0);
  lits := bin_impl_graph[ealit];
  if (lits[1]+2>lits[0]) then raise Texc_trk.create('hle_get bounds check 3 fail');
  for k := lits[1]-1 downto 0 do begin
    lit := -lits[k+2];
    alit := abs(lit);
    if (alit>num_vars)or(alit=0) then raise Texc_trk.create('hle_get bounds check 2 fail');
    alit := 2*alit +ord(lit<0);
    if not hle_visit[alit] then begin
      hle_visit[alit] := true;
      hle_lits[j] := lit;
      inc(j);
    end;
  end;
  pp_hllen := j;
end;


procedure Tpp_solver.pp_get_hl(lit0 : Tliteral);
var i,elit : Tliteral; alit : Tuliteral; 
begin
  hle_lits[0] := lit0;
  pp_hllen := 1;
  alit := abs(lit0);
  alit := 2*alit +ord(lit0<0);
  hle_visit[alit] := true;
  i := 0; 
  while (i<pp_hllen) do begin
    elit := hle_lits[i];
    pp_hle_add(elit);
    inc(i);
  end;
end;

procedure Tpp_solver.pp_unget_hl(lit0 : Tliteral);
var i,lit : Tliteral; alit : Tuliteral;
begin
  for i := pp_hllen-1 downto 0 do begin
    lit := hle_lits[i];
    alit := abs(lit);
    alit := 2*alit +ord(lit<0);
    hle_visit[alit] := false;
  end;
  hl_work += pp_hllen;
end;

procedure Tpp_solver.pp_hle_lit(lit : Tliteral);
var alit : Tuliteral; cnt : int64; k : Tliteral; ppc : PPclause; cls : Pclause;
begin
  ppstrengthen.clear();
  k := 0;
  pp_get_hl(lit);
  if (pp_hllen>1) then begin
    alit := abs(lit);
    alit := alit*2 +ord(lit<0);
    if hle_visit[alit xor 1]  then begin
      k := 1;
    end else begin
      for cnt := lockOccurList(lit,ppc)-1 downto 0 do begin
        pp_hle_lit_apply(ppc[cnt],lit);
      end;
      unlockOccurList(lit);
      k := 0;
    end;
  end;
  pp_unget_hl(lit);
  while ppstrengthen.pop(k,cls) do begin
    strengthen(cls,k,true);
    pp_used_hle := true;
  end;
end;

procedure Tpp_solver.pp_hle_lit_apply(cls : Pclause; lit0 : Tliteral);
var alit : Tuliteral; i,lit : Tliteral; lits : Pliteral; seen : boolean;
begin
  if ((cls^.flags and clausetag_pp_discard)>0) then exit;
  seen := false;
  lits := getLiterals(cls);
  { well, don't apply to clauses that have lit already discarded }
  for i := cls^.len-1 downto 0 do begin
    lit := lits[i];
    if lit=lit0 then seen := true;
  end;
  hl_work += cls^.len;
  if seen then for i := cls^.len-1 downto 0 do begin
    lit := lits[i];
    if lit<>lit0 then begin 
      alit := abs(lit);
      alit := 2*alit +ord(lit<0);
      if hle_visit[alit] then begin
        ppstrengthen.push(lit,cls);
        inc(pp_hle_v_rm);
      end;
    end;
  end;
end;

procedure Tpp_solver.pp_hle_v();
var i : Tliteral; cnt,k : Tliteral;
begin
  pp_hle_v_rm := 0;
  if have_fresh_binary_cls then begin
    have_fresh_binary_cls := false;
    if notquiet then write('c hle');
    k := num_vars div 50;
    cnt := k;
    for i := num_vars downto 1 do begin
      pp_hle_lit(i);
      pp_hle_lit(-i);
      dec(cnt);
      if (cnt=0) then begin
        write('.');
        cnt := k;
      end;
    end;
    occurlist_valid := false;
    if notquiet then writeln(' ',pp_hle_v_rm);
  end;
end;





procedure Tpp_solver.pp_hle_v2();
var i,j : Tliteral; cnt,k,lit : Tliteral; alit : Tuliteral; c1,c0 : qword;
begin
  if hl_deactivated then exit;

  pp_hle_v_rm := 0;
  if not have_fresh_binary_cls then exit;
  have_fresh_binary_cls := false;

  if notquiet then write('c hle');

  c1 := 0; c0 := 0;
  for i := -num_Vars to num_vars do begin
    alit := abs(i);
    alit := 2*alit +ord(i<0);
    if have_fresh_bincls_about[alit] then inc(c1) else inc(c0);
  end;
  if (c1*5>c0) then begin
    c0 := 0;
    if notquiet then write('{ many vars, check all }');
    for i := -num_Vars to num_vars do begin
      alit := abs(i);
      alit := 2*alit +ord(i<0);
      varvisit[alit] := true;
      inc(c0);
    end;
  end else begin
    if notquiet then write('{ not so many, try to compute a minmal update }');
    c0 := 0;
    for i := -num_Vars to num_vars do begin
      alit := abs(i);
      alit := 2*alit +ord(i<0);
      if have_fresh_bincls_about[alit] then begin
        pp_get_hl(i);
        for j := pp_hllen-1 downto 0 do begin
          lit := hle_lits[j];
          alit := abs(lit);
          alit := 2*alit +ord(lit<0);
          if not varvisit[alit] then inc(c0);
          varvisit[alit] := true;
        end;
        pp_unget_hl(i);
      end;
    end;
  end;
  for i := -num_Vars to num_vars do begin
    alit := abs(i);
    alit := 2*alit +ord(i<0);
    have_fresh_bincls_about[alit] := false;
  end;
  if notquiet then write(' use ',c0,' ');
  k := c0 div 50;
  cnt := k;
  varvisit[0] := false;
  for i := -num_Vars to num_vars do begin
    if (hl_autolimit and (hl_work*hl_workscale>sss_work)) or not(keepsolving) then begin
      writeln;
      if notquiet then write('c deactivating expensive hl-checks... ');
      hl_deactivated := true;
    end;
    if hl_deactivated then break;

    alit := abs(i);
    alit := 2*alit +ord(i<0);
    if varvisit[alit] then begin
      pp_hle_lit(i);
      varvisit[alit] := false;
      dec(cnt);
    end;
    if (cnt=0) then begin
      cnt := k;
    end;
  end;
  if notquiet then writeln(' => ',pp_hle_v_rm);
end;




