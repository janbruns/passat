{$mode objfpc}
unit rand;

{$include config.pas}

interface


type
Txorshift128plusstate = array[0..1] of qword;

function random_qword() : qword;
function random_qword(var s : Txorshift128plusstate) : qword;
function random_float() : double;
function random_float(var s : Txorshift128plusstate) : double;


implementation


function xorshift128plus_23_17_26(var s : Txorshift128plusstate) : qword;
var x,y : qword;
begin
  x := s[0];
  y := s[1];
  s[0] := y;
  x := x xor (x shl 23);
  s[1] := x xor y xor (x shr 17) xor (y shr 26);
  xorshift128plus_23_17_26 := s[1] + y;
end;

var
glob_xorshift128plus_23_17_26_state : Txorshift128plusstate;
scale : double;

function random_qword() : qword;
begin
  random_qword := xorshift128plus_23_17_26(glob_xorshift128plus_23_17_26_state);
end;

function random_qword(var s : Txorshift128plusstate) : qword;
begin
  random_qword := xorshift128plus_23_17_26(s);
end;

function random_float() : double;
begin
  random_float := scale*xorshift128plus_23_17_26(glob_xorshift128plus_23_17_26_state);
end;

function random_float(var s : Txorshift128plusstate) : double;
begin
  random_float := scale*xorshift128plus_23_17_26(s);
end;


procedure doinit();
var i : qword;
begin
  glob_xorshift128plus_23_17_26_state[0] := $29FC00599B39BA43;
  glob_xorshift128plus_23_17_26_state[1] := $7BB47FDF54A89104;
  i := 1;
  repeat
    scale := 1/(18446744073709551615.0+i);
    i*= 2;
  until scale*qword(18446744073709551615)<1.0;
end;




begin
  doinit();
end.